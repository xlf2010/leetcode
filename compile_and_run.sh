#/bin/bash
set -e
[ "$#" -lt 1 ] && echo "please input filename" && exit 1;

out_folder="bin"

[ ! -d "${out_folder}" ] && mkdir ${out_folder}

function compile_run_c () {
	f_dst=`echo "$1" | awk -F'/' '{print $NF}'`
	f_dst="${f_dst/\.c/\.e}"
	f_dst="${out_folder}/${f_dst}"
	[ -f ${f_dst} ] && echo "clean old executable file:${f_dst}" && rm -f ${f_dst}
	echo " compiling file $f_src to $f_dst"
	#Linux add sanitize adress
	case `uname` in
		"Linux")
			gcc -Wall -fno-omit-frame-pointer -fsanitize=address -o ${f_dst} -g ${f_src}
		;;
		*)
		gcc -Wall -o ${f_dst} -g ${f_src}
		;;
	esac
	echo "compile finished,running file ${f_dst}"
	[ -x ${f_dst} ] && ./${f_dst}
}

function compile_run_java () {
	[ -z "$JAVA_HOME" ] && echo "JAVA_HOME not set " && exit 1;
	f_dst=`echo "$1" | awk -F'/' '{print $NF}'`
	f_dst="${f_dst/\.java/}"
	
	echo " compiling file $f_src to ${out_folder}/${f_dst}.class"
	javac -g -encoding UTF-8 $1 -d ${out_folder}
	
	echo "compile finished,running file ${out_folder}/${f_dst}.class"
	java -Dfile.encoding=UTF-8 -cp ${out_folder} ${f_dst}
}

f_src="$1"

case "${f_src##*.}" in
	"java")
		compile_run_java ${f_src}
	;;
	"c")
		compile_run_c ${f_src}
	;;
	*)
		echo "unknow file type!!"
	;;
esac
