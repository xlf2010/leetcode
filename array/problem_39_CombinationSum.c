/**
39. Combination Sum		https://leetcode.com/problems/combination-sum/
Medium

Given a set of candidate numbers (candidates) (without duplicates) and a target number (target), 
find all unique combinations in candidates where the candidate numbers sums to target.

The same repeated number may be chosen from candidates unlimited number of times.

Note:

    All numbers (including target) will be positive integers.
    The solution set must not contain duplicate combinations.

Example 1:

Input: candidates = [2,3,6,7], target = 7,
A solution set is:
[
  [7],
  [2,2,3]
]

Example 2:

Input: candidates = [2,3,5], target = 8,
A solution set is:
[
  [2,2,2,2],
  [2,3,3],
  [3,5]
]

Accepted
342,511
Submissions
705,934


*/

#include <stdio.h>
#include <stdlib.h>

struct Stack{
	int val;
	struct Stack *pre;
};

void push(struct Stack **stack,int *stackSize,int val){
	struct Stack *s = (struct Stack *)malloc(sizeof(struct Stack));
	s->val=val;
	s->pre=*stack;
	*stack=s;
	(*stackSize)++;
}

int pop(struct Stack **stack,int *stackSize){
	if(!(*stack)) return 0;
	int val=(*stack)->val;
	struct Stack *s=*stack;
	*stack=s->pre;
	free(s);
	(*stackSize)--;
	return val;
}


//构建结果
void generalPath(struct Stack *stack,int stackSize, int*** ret,int** returnColumnSizes,int returnSize){
	*ret = (int **)realloc(*ret,sizeof(int *) * returnSize);
	*returnColumnSizes = (int *)realloc(*returnColumnSizes,sizeof(int) * returnSize);
	
	int i=stackSize-1,*re=(int *)malloc(sizeof(int) * stackSize);
	while(stack){
		re[i--] = stack->val;
		stack=stack->pre;
	}
	(*ret)[returnSize-1] = re;
	(*returnColumnSizes)[returnSize-1] = stackSize;
}

int comp(const void *a,const void *b){
	return *((int *)a) - *((int *)a);
}

void freeStack(struct Stack *stack){
	struct Stack *s;
	while(stack){
		s=stack;
		stack=stack->pre;
		free(s);
	}
}

//给定一个正整数数组，任选其中的元素 选中的元素之和=target，每个元素
int** combinationSum(int* candidates, int candidatesSize, int target, int* returnSize, int** returnColumnSizes){
	//init
	*returnSize=0;
	*returnColumnSizes = (int *)malloc(sizeof(int));
	(*returnColumnSizes)[0] = 0;
	if(!candidates || candidatesSize <=0) return NULL;
	//升序排
	qsort(candidates,candidatesSize,sizeof(int),comp);
	if(candidates[0] > target) return NULL;
	
	struct Stack *stack = NULL;
	int j,i,stackSize=0,stackSum=0;
	int **ret = (int **)malloc(sizeof(int *));
	for(i=0;i<candidatesSize;i++){
		j=i;
		stackSum=0;
		push(&stack,&stackSize,candidates[j]);
		
		while(stackSize > 0 && j >= 0 && j < candidatesSize){
			stackSum += candidates[j];			
			if(stackSum == target){				
				(*returnSize)++;				
				generalPath(stack,stackSize,&ret,returnColumnSizes,*returnSize);
				stackSum -= pop(&stack,&stackSize);
				j++;
			} else if(stackSum > target){
				stackSum -= pop(&stack,&stackSize);
			}else{
				push(&stack,&stackSize,candidates[j]);
			}
		}
		
		freeStack(stack);
		stack=NULL;
	}
	return ret;
}

int main(int argc,char** argv){
	
	int arr[] = {1,3,5,16};
	int i=7,size=sizeof(arr)/sizeof(*arr);
	
	int *returnColumnSizes,returnSize,j;
	int **res = combinationSum(arr,size,i,&returnSize,&returnColumnSizes);
	for(i=0;i<returnSize;i++){
		for(j=0;j<returnColumnSizes[i];j++){
			printf("%d,",res[i][j]);
		}
		printf("\n");
	}
	//printf("res=%d\n",res);
	return 0;
}

