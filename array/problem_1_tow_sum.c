/*
Easy
  tow sum leet code https://leetcode.com/problems/two-sum/
  
  Given an array of integers, return indices of the two numbers such that they add up to a specific target.

	You may assume that each input would have exactly one solution, and you may not use the same element twice.

	Example:

	Given nums = [2, 7, 11, 15], target = 9,

	Because nums[0] + nums[1] = 2 + 7 = 9,
	return [0, 1].
*/

#include<stdio.h>
#include<stdlib.h>
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target) {
    int* ret = (int *)malloc(sizeof(int) * 2);
	ret[0]=ret[1]=0;
	int i,j;
	for(i=0;i<numsSize-1;i++){
		for(j=i+1;j<numsSize;j++){
			if(nums[i]+nums[j]==target){
				ret[0]=i;
				ret[1]=j;
				return ret;
			}
		}
	}
	return ret;
}

int main(int argc,char **argv){
	int num[7]={3,4,1,5,6,2,9};
	int *ret=twoSum(num,7,14);
	printf("%d,%d",*ret,*(ret+1));
	return 0;
}
