/*
 * 11. Container With Most Water
https://leetcode.com/problems/container-with-most-water/solution/
Medium
Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

Example:

Given array nums = [-1, 2, 1, -4], and target = 1.

The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).


*/

#include<stdio.h>
#include<stdlib.h>
#define ARRAY_LEH 9

int min(int a,int b){
	return a>b?b:a;
}


//两头接近,先计算宽度最大的面积，随着宽度变小，寻找最高的值
int maxArea(int* height, int heightSize) {
	int l=0,r=heightSize-1,result=-1,area;
	while (l<r){
		area = min(height[l],height[r]) * (r-l);
		if(area>result) result= area;
		if(height[l]<height[r]) l++;
		else r--;
	}
	return result;
}

//遍历所有情况，选取最大的面积，O(n^2)
/*int maxArea(int* height, int heightSize) {
    int i,j,area,max=-1;
    for(i=0;i<heightSize-1;i++){
		for(j=i+1;j<heightSize;j++){
			area = min(height[i],height[j])*(j-i);
			if(area>max){
				max=area;
			}
		}
	}
	return max;
}*/


int main(int argc,char **argv){
//	int num[ARRAY_LEH]={3,4,1,5,6,3,-9};
//	int num[]={500,10,10,5000,501};
	int num[]={1,8,6,2,5,4,8,3,7};
//	int i,target=80;
	int res = maxArea(num,ARRAY_LEH);
	printf("%d",res);
	printf("\n");

	return 0;
}
