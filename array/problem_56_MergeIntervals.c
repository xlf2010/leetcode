/*
56. Merge Intervals		https://leetcode.com/problems/merge-intervals/
Medium

2250

174

Favorite

Share
Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
NOTE: input types have been changed on April 15, 2019. Please reset to default code definition to get new method signature.

Accepted
370,073
Submissions
1,027,269
*/

#include <stdio.h>
#include <stdlib.h>

//快速排序
void quick_sort(int** intervals,int start,int end,int comp(const int*,const int *)){
	if(start > end) return;
	int *tmp=intervals[start];
	int i=start,j=end;
	while(i<j){
		while(i<j && comp(intervals[j],tmp)>=0){
			j--;
		}
		intervals[i]=intervals[j];
		while(i<j && comp(intervals[i],tmp)<=0){
			i++;
		}
		intervals[j]=intervals[i];
	}
	intervals[i]=tmp;
	
	quick_sort(intervals,start,i-1,comp);
	quick_sort(intervals,i+1,end,comp);
}

int comp(const int* a,const int* b){
	return *a-*b;
}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
//区间合并,将多个区间按起始位置排序，遇到在区间内的值合并到一起，不同的则另建新区间
//Runtime: 12 ms, faster than 97.52% of C online submissions for Merge Intervals.
//Memory Usage: 9.2 MB, less than 100.00% of C online submissions for Merge Intervals.
int** merge(int** intervals, int intervalsSize, int* intervalsColSize, int* returnSize, int** returnColumnSizes){
	*returnSize =0;
	if(intervalsSize <=0) return NULL;
	
	quick_sort(intervals,0,intervalsSize-1,comp);

	//init first interval
	int i,**ret = (int **)malloc(sizeof(int *));
	int *re = (int *)malloc(sizeof(int) * 2);
	re[0] = intervals[0][0];
	re[1] = intervals[0][intervalsColSize[0]-1];
	ret[0] = re;
	(*returnSize)++;
	*returnColumnSizes = (int *)malloc(sizeof(int));
	**returnColumnSizes = 2;
	//init first interval end
	int ret_curr_index=0;
	
	for(i=1;i<intervalsSize;i++){
		//当前区间超过上一个区间的最大值，新建一个区间
		if(intervals[i][0] > ret[ret_curr_index][1]){
			re = (int *)malloc(sizeof(int) * 2);
			re[0] = intervals[i][0];
			re[1] = intervals[i][intervalsColSize[i]-1];
						
			ret = realloc(ret, sizeof(int *) * (*returnSize+1));
			ret[*returnSize] = re;
			
			*returnColumnSizes = realloc(*returnColumnSizes,sizeof(int) * (*returnSize+1));
			(*returnColumnSizes)[*returnSize] = 2;
			(*returnSize)++;
			ret_curr_index++;
			
			continue;
		}
		
		//起始位置在结果区间内,且终点超过当前结果集的，扩大结果集
		if(intervals[i][intervalsColSize[i]-1] > ret[ret_curr_index][1]){
			ret[ret_curr_index][1] = intervals[i][intervalsColSize[i]-1];
		}
	}
	return ret;
}

int main(int argc,char** argv){
	int size=8,i=0,j;
	int **intervals = (int **)malloc(sizeof(int *) * size);
	int a[2]={4,5};
	int b[2]={2,4};
	int c[2]={4,6};
	int d[2]={3,4};
	int e[2]={0,0};
	int f[2]={1,1};
	int g[2]={3,5};
	int h[2]={2,2};
	intervals[i++] =a;
	intervals[i++] =b;
	intervals[i++] =c;
	intervals[i++] =d;
	intervals[i++] =e;
	intervals[i++] =f;
	intervals[i++] =g;
	intervals[i++] =h;
	
	for(i=0;i<size;i++){
		printf("%d,%d\n",intervals[i][0],intervals[i][1]);
	}
	printf("--------------after sort-------------\n");
	

	//for(i=0;i<intervalsSize;i++){
		//for(j=0;j<2;j++){
			//printf("%d,",intervals[i][j]);
		//}
		//printf("\n");
	//}
	
	int returnSize,*returnColumnSizes=NULL,intervalsColSize[8] = {2,2,2,2,2,2,2,2};
	int** ret = merge(intervals,size,intervalsColSize,&returnSize,&returnColumnSizes);
	printf("--------------after merge-------------\n");
	for(i=0;i<returnSize;i++){
		for(j=0;j<returnColumnSizes[i];j++){
			printf("%d,",ret[i][j]);
		}
		printf("\n");
	}
	
	free(intervals);
	if(ret != NULL){
		for(i=0;i<returnSize;i++){
			free(ret[i]);
		}
		free(ret);
	}
	if(returnColumnSizes!=NULL){
		free(returnColumnSizes);
	}
	
	return 0;
}
