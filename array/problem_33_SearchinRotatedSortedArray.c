/*
33. Search in Rotated Sorted Array	https://leetcode.com/problems/search-in-rotated-sorted-array/
Medium

Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.

(i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Your algorithm's runtime complexity must be in the order of O(log n).

Example 1:

Input: nums = [4,5,6,7,0,1,2], target = 0
Output: 4
Example 2:

Input: nums = [4,5,6,7,0,1,2], target = 3
Output: -1
*/

#include<stdio.h>

int search(int* nums, int numsSize, int target) {
	if(numsSize <=0 ) return -1;	
	//如果第一个数大于target且最后一个数小于target，则target不在数组内
	if(nums[0] > target && nums[numsSize-1] < target) return -1;
    int i,mid,l=0,r=numsSize-1;

    //二分查找
    while (l<=r){
		//判断边界值
		if(nums[l] == target) return l;
		if(nums[r] == target) return r;

		mid=((r-l)>>1)+l;
		if(nums[mid] == target) return mid;
		//中间点在转置点之前，说明当前区间[l..mid]是递增的，直接二分查找
		if(nums[mid] > nums[l]){
			//在左边的递增区间
			if(target > nums[l] && target < nums[mid]){
				r=mid-1;
			}else{
				l=mid+1;
			}
		}
		//中间点在转置点之后，说明当前区间[mid..r]是递增的，直接二分查找
		else{
			if(target > nums[mid] && target < nums[r]){
				l=mid+1;
			}else{
				r=mid-1;
			}
		}
	}
	return -1;
}

int main(int args,char** argv){
	int num[]={1};
	int len=sizeof(num)/sizeof(*num),target=1;
	int res = search(num,len,target);
	printf("%d\n",res);
}
