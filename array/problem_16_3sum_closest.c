/*
 * 16. 3Sum Closest
Medium
Given an array nums of n integers and an integer target, find three integers in nums such that the sum is closest to target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

Example:

Given array nums = [-1, 2, 1, -4], and target = 1.

The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).


*/

#include<stdio.h>
#include<stdlib.h>
#define ARRAY_LEH 6

int comp(const void* o1,const void* o2){
	return *((int *)o1)-*((int *)o2);
}

inline int abs(int val){
	return val<0?-val:val;
}

int threeSumClosest(int* nums, int numsSize, int target) {
	if(numsSize<3){
		return 0;
	}
	//排序
	qsort(nums,numsSize,sizeof(int),comp);
    int i,l,r,result,diff=1<<31-1,sum;
    for(i=0;i<numsSize-2;i++){
		l=i+1;
		r=numsSize-1;
		while(l<r){
			sum=nums[i]+nums[l]+nums[r];
			if(sum==target) return sum;
			if(abs(sum-target) < diff){
				result=sum;
				diff=abs(sum-target);
			}
			if(sum>target) {
				r--;
			}else{
				l++;
			}
		}
	 }
	 return result;
}

int main(int argc,char **argv){
//	int num[ARRAY_LEH]={3,4,1,5,6,3,-9};
	int num[]={1,6,9,14,16,70};
	int i,target=80;
	int res = threeSumClosest(num,ARRAY_LEH,target);
	printf("%d",res);
	printf("\n");

	return 0;
}
