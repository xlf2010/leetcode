/*
34. Find First and Last Position of Element in Sorted Array 
* https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/
Medium

1459

81

Favorite

Share

Given an array of integers nums sorted in ascending order,
find the starting and ending position of a given target value.

Your algorithm's runtime complexity must be in the order of O(log n).

If the target is not found in the array, return [-1, -1].


Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1] 

*/

#include<stdio.h>
#include<stdlib.h>

/**
 * Return an array of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* searchRange(int* nums, int numsSize, int target, int* returnSize) {
	*returnSize=2;
    int* ret=(int *)malloc(sizeof(int) * (*returnSize));
    ret[0]=-1;
    ret[1]=-1;
    if(numsSize <=0 ) return ret;	

	int l=0,r=numsSize-1,mid,pos=-1;
	while(l<=r){
		mid=((r-l)>>1)+l;
		if(nums[mid]==target) {
			pos=mid;
			break;
		}
		if(nums[mid]>target){
			r=mid-1;
		}else{
			l=mid+1;			
		}
	}
	//查找不到数据
	if(pos==-1) return ret;
	
	//从中间向两边查找
	l=r=mid;
	while(l>0 && nums[--l] == target);
	while(r<numsSize-1 && nums[++r] == target);
	
	if(nums[l] == target){
		ret[0]=l;
	}else{
		ret[0]=l+1;
	}
	if(nums[r]==target){
		ret[1]=r;
	}else{
		ret[1]=r-1;
	}
	return ret;
}

int main(int args,char** argv){
	int num[]={5,7,7,8,8,10,10};
	int i,returnSize,len=sizeof(num)/sizeof(*num),target=10;
	int* res = searchRange(num,len,target,&returnSize);
	printf("returnSize=%d\n",returnSize);
	for(i=0;i<returnSize;i++){
		printf("%d,",res[i]);
	}
	printf("\n");
}
