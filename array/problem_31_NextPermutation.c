/**
31. Next Permutation	https://leetcode.com/problems/next-permutation/
Medium

Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.

If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).

The replacement must be in-place and use only constant extra memory.

Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.

1,2,3 → 1,3,2
3,2,1 → 1,2,3
1,1,5 → 1,5,1

Accepted
237,496
Submissions
779,337
*/

#include <stdio.h>
#include <stdlib.h>

void reverse(int* nums,int start,int end){
	int tmp;
	while(start<end){
		tmp = nums[start];
		nums[start] = nums[end];
		nums[end]=tmp;
		start++;
		end--;
	}
}
//Runtime: 8 ms, faster than 95.13% of C online submissions for Next Permutation.
//Memory Usage: 7.5 MB, less than 53.84% of C online submissions for Next Permutation.
//求下一组合
void nextPermutation(int* nums, int numsSize){
	if(!nums || numsSize <= 1) return;
	int i=numsSize-2;
	//反向查找前一个元素小于后一个的位置，从第i位置之后的都是降序排列
	for(;i>=0;i--){
		if(nums[i] < nums[i+1]){
			break;
		}
	}
	//整个数组降序排列，将整个数组翻转变成升序
	if(i<0) {
		reverse(nums,0,numsSize-1);
		return;
	}
	//反向查找大于位置i的位置j的值
	int j=numsSize-1,tmp;
	for(;j>=i;j--){
		if(nums[j] > nums[i]){
			break;
		}
	}
	//交换i，j位置的值，从第i位置之后的都是降序排列不变
	tmp=nums[j];
	nums[j]=nums[i];
	nums[i]=tmp;
	//i+1到结束位置翻转变成升序
	reverse(nums,i+1,numsSize-1);
}

int main(int argc,char** argv){
	
	int arr[] = {1,5,1};
	int i,size=sizeof(arr)/sizeof(*arr);
	for(i=0;i<size;i++){
		printf("%d,",arr[i]);
	}
	printf("\n");
	nextPermutation(arr,size);
	for(i=0;i<size;i++){
		printf("%d,",arr[i]);
	}
	printf("\n");
	return 0;
}
