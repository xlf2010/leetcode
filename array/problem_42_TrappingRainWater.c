/*
 * 42. Trapping Rain Water
	Hard
 * https://leetcode.com/problems/trapping-rain-water/
 * Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.


The above elevation map is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped. Thanks Marcos for contributing this image!

Example:
 */

#include<stdio.h>
#include<stdlib.h>

//计算在(start,end)能装多少水
int handle_trap(int* height,int start,int end){
	int i,res=0,min=height[start]>height[end]?height[end]:height[start];
	for(i=start+1;i<end;i++){
		if(height[i]<min){
			res=res+min-height[i];
		}
	}
	return res;
}

int trap(int* height, int heightSize) {
	if(heightSize < 2) return 0;
    int i,before_idx=-1,after_idx=-1,result=0;
    //寻找初始值
    for(i=0;i<heightSize-1;i++){
		//第一个元素大于0并且是大于后一个才可能装水
		if(height[i]>height[i+1]){
			before_idx = i;
			i++;
			break;
		}
	}
	//有序递增的数组，不可能装水。
	if(before_idx<0) return 0;
	
	after_idx=i;
	while (i<heightSize){
		//往后寻找最更高的栏
		if(height[after_idx] < height[i]){
			after_idx = i;
		}
		//如果右栏比左栏高，则计算能装多少水
		if(height[before_idx] <= height[after_idx] || i==heightSize-1){
			result+=handle_trap(height,before_idx,after_idx);
			before_idx=after_idx;
			i=after_idx++;
		}
		i++;
	}
	return result;
}


inline int max(int a,int b){
	return a>b?a:b;
}

inline int min(int a,int b){
	return a<b?a:b;
}
//参考solution 2,dynamic program
// 建立两个数组max_left,max_right，分别保存左边最高的值与右边最高值，
//则这区间能装的水为左右最短板的值-中间的高度 min(max_left[i],max_right[i])-height[i]
int trap_1(int* height, int heightSize) {
	int res=0,i,max_left[heightSize],max_right[heightSize];
	max_left[0]=height[0];
	for(i=1;i<heightSize;i++){
		max_left[i]=max(height[i],max_left[i-1]);
	}
	max_right[heightSize-1]=height[heightSize-1];
	for(i=heightSize-2;i>=0;i--){
		max_right[i]=max(height[i],max_right[i+1]);
	}
	for(i=0;i<heightSize;i++){
		res+=min(max_left[i],max_right[i])-height[i];
	}
	return res;
}


int main(int argc,char **argv){
	int num[]={1,7,8};
//	int num[]={5,2,1,2,1,5};
//	int num[]={0,1,0,2,1,0,1,3,2,1,2,1};
	int len=3;
	int res = trap_1(num,len);
	printf("%d",res);
	printf("\n");

	return 0;
}

