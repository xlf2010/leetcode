/*
48. Rotate Image	https://leetcode.com/problems/rotate-image/
48. Rotate Image
Medium

You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

Note:

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly.
DO NOT allocate another 2D matrix and do the rotation.

Example 1:

Given input matrix = 
[
  [1,2,3],
  [4,5,6],
  [7,8,9]
],

rotate the input matrix in-place such that it becomes:
[
  [7,4,1],
  [8,5,2],
  [9,6,3]
]

Example 2:

Given input matrix =
[
  [ 5, 1, 9,11],
  [ 2, 4, 8,10],
  [13, 3, 6, 7],
  [15,14,12,16]
], 

rotate the input matrix in-place such that it becomes:
[
  [15,13, 2, 5],
  [14, 3, 4, 1],
  [12, 6, 8, 9],
  [16, 7,10,11]
]


*/

#include <stdio.h>
#include <stdlib.h>

//n*n矩阵，将矩阵顺时针旋转90度，在原来的矩阵上操作
//Runtime: 4 ms, faster than 87.71% of C online submissions for Rotate Image.
//Memory Usage: 7.4 MB, less than 59.09% of C online submissions for Rotate Image.
void rotate(int** matrix, int matrixSize, int* matrixColSize){
	if(matrixSize <= 0 || !matrix) return;
	*matrixColSize = matrixSize;
	int i,j,tmp;
	for(i=0;i<matrixSize/2;i++){
		for(j=i;j<matrixSize-i-1;j++){
			//保存左上角值tmp，用左下角覆盖左上角值，用右下角覆盖左下角值，右上角覆盖右下角值，最后tmp覆盖右上角值
			tmp=matrix[i][j];
			matrix[i][j] = matrix[matrixSize-1-j][i];
			matrix[matrixSize-1-j][i] = matrix[matrixSize-1-i][matrixSize-1-j];
			matrix[matrixSize-1-i][matrixSize-1-j] = matrix[j][matrixSize-1-i];
			matrix[j][matrixSize-1-i] = tmp;
		}
	}
}

int main(int argc,char** argv){
	int i,j,size=5;
	int** matrix = (int **)malloc(sizeof(int *) * size);
	int *row;
	for(i=0;i<size;i++){
		row = (int *)malloc(sizeof(int) * size);
		matrix[i]=row;
		for(j=0;j<size;j++){
			matrix[i][j] = i*size+j;
		}
	} 
	
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			printf("%d,",matrix[i][j]);
		}
		printf("\n");
	}
	printf("----------------------------\n");
	
	rotate(matrix,size,&size);
	
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			printf("%d,",matrix[i][j]);
		}
		printf("\n");
	}
	
	for(i=0;i<size;i++){
		free(matrix[i]);
	}
	free(matrix);
	return 0;
}



