/*

18. 4Sum https://leetcode.com/problems/4sum/

Medium

Given an array nums of n integers and an integer target, are there elements a, b, c, and d in nums such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

Note:

The solution set must not contain duplicate quadruplets.

Example:

Given array nums = [1, 0, -1, 0, -2, 2], and target = 0.

A solution set is:
[
  [-1,  0, 0, 1],
  [-2, -1, 1, 2],
  [-2,  0, 0, 2]
]
*/

#include<stdio.h>
#include<stdlib.h>

int comp(const void* o1,const void* o2){
	return *((int *)o1)-*((int *)o2);
}

//添加到返回数组,数组不够大的话要扩大
int **add_res(int **ret_arr,int* a_size,int *m_size,int* ans){
	int i,max_size=*m_size,arr_size=*a_size;

	if(arr_size>=max_size){
		max_size+=10;
		ret_arr = (int **)realloc(ret_arr, sizeof(int *) * max_size);
		*m_size=max_size;
	}
	ret_arr[arr_size] = ans;
	(*a_size)++;
	return ret_arr;
}

/**
 * Return an array of arrays of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
int** fourSum(int* nums, int numsSize, int target, int* returnSize) {
    *returnSize=0;
	int max_size=10;
	int **ret_arr = (int **)malloc(sizeof(int *)*max_size);
	if(numsSize < 4){
		return ret_arr;
	}
	//排序
	qsort(nums,numsSize,sizeof(int),comp);
	
	int i,j,l,r;
	for(i=0;i<numsSize-3;i++){
		//跳过重复的值
		if(i>0&&nums[i]==nums[i-1]) continue;
		//参考4ms答案，如果从当前值nums[i]开始往后4位数和大于target，再往后数组之和肯定会大于targe，此处可以break
		if(nums[i] + nums[i+1] + nums[i+2]+nums[i+3] > target) break;
		//参考4ms答案，如果当前值nums[i]与有序数组最后三位数之和小于target，则当前索引i与numsSize-3之间的任3位数之和肯定小于target，此处可以continue
		if(nums[i] + nums[numsSize-1] + nums[numsSize-2]+nums[numsSize-3] < target) continue;
		for(j=i+1;j<numsSize-2;j++){
			if(j>i+1 && nums[j]==nums[j-1]) continue;
			//与上类似
			if(nums[i] + nums[j] + nums[j+1]+nums[j+2] > target) break;
			if(nums[i] + nums[j] + nums[numsSize-1]+nums[numsSize-2] < target) continue;
			l=j+1;
			r=numsSize-1;
			while(l<r){
				int sum = nums[i]+nums[j]+nums[l]+nums[r]-target;
				if(sum==0){
					int *res = (int *)malloc(sizeof(int)*4);
					res[0]=nums[i];
					res[1]=nums[j];
					res[2]=nums[l++];
					res[3]=nums[r--];
					ret_arr = add_res(ret_arr,returnSize,&max_size,res);
					//跳过重复的值
					while(l<numsSize && nums[l]==nums[l-1]) l++;
					while(r>=l && nums[r]==nums[r+1]) r--;
				}else if(sum<0){
					l++;
				}else{
					r--;
				}
			}
		}
	}
	return ret_arr;
}

int main(int argc,char **argv){
//	int num[ARRAY_LEH]={3,4,1,5,6,3,-9};
	int num[]={0,4,-5,2,-2,4,2,-1,4};
	int i,len=sizeof(num)/sizeof(*num),returnSize,target=12;
	int **res = fourSum(num,len,target,&returnSize);
	printf("ret_size=%d\n",returnSize);
	for(i=0;i<returnSize;i++){
		printf("%d,%d,%d,%d",res[i][0],res[i][1],res[i][2],res[i][3]);
		printf("\n");
	}
	return 0;
}
