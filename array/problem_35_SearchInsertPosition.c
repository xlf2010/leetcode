/**
35. Search Insert Position	https://leetcode.com/problems/search-insert-position/
Easy

Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

You may assume no duplicates in the array.

Example 1:

Input: [1,3,5,6], 5
Output: 2

Example 2:

Input: [1,3,5,6], 2
Output: 1

Example 3:

Input: [1,3,5,6], 7
Output: 4

Example 4:

Input: [1,3,5,6], 0
Output: 0

Accepted
401,211
Submissions
982,447

*/

#include <stdio.h>
#include <stdlib.h>

//Runtime: 0 ms, faster than 100.00% of C online submissions for Search Insert Position.
//Memory Usage: 7.2 MB, less than 57.61% of C online submissions for Search Insert Position.
//在一个有序数组查找，存在则返回，否则返回插入的位置
int searchInsert(int* nums, int numsSize, int target){
	if(!nums || numsSize <= 0 || *nums >= target) return 0;
	int i=0,j=numsSize-1,mid;
	while(i<=j){
		mid = i + ((j-i)>>1);
		if(nums[mid] == target){
			return mid;
		}
		if(nums[mid] > target){
			j = mid-1;
		}else{
			i = mid+1;
		}
	}
	//如果target大于mid值，说明要在mid右边插入，即mid+1
	return nums[mid] > target ? mid : mid + 1;
}


int main(int argc,char** argv){
	
	int arr[] = {1,3,5,16};
	int i,size=sizeof(arr)/sizeof(*arr);
	for(i=0;i<size;i++){
		printf("%d,",arr[i]);
	}
	printf("\n");
	int res=searchInsert(arr,size,19);
	printf("res=%d\n",res);
	return 0;
}
