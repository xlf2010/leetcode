/*
Medium
  3 sum leet code https://leetcode.com/problems/3sum/
  
Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0? Find all unique triplets in the array which gives the sum of zero.

Note:

The solution set must not contain duplicate triplets.

Example:

Given array nums = [-1, 0, 1, 2, -1, -4],

A solution set is:
[
  [-1, 0, 1],
  [-1, -1, 2]
]
*/

#include<stdio.h>
#include<stdlib.h>
#define ARRAY_LEH 15


int comp(const void* o1,const void* o2){
	return *((int *)o1)-*((int *)o2);
}
//添加到返回数组,数组不够大的话要扩大
int **add_res(int **ret_arr,int* a_size,int *m_size,int* ans){
	int i,max_size=*m_size,arr_size=*a_size;

	if(arr_size>=max_size){
		max_size+=10;
		ret_arr = (int **)realloc(ret_arr, sizeof(int *) * max_size);
		*m_size=max_size;
	}
	ret_arr[arr_size] = ans;
	(*a_size)++;
	return ret_arr;
}
/**
 * Return an array of arrays of size *returnSize.
 * Note: The returned array must be malloced, assume caller calls free().
 */
 int** threeSum(int* nums, int numsSize, int* returnSize) {
	*returnSize=0;
	int max_size=10;
	int **ret_arr = (int **)malloc(sizeof(int *)*max_size);
	if(numsSize < 3){
		return ret_arr;
	}
	//排序
	qsort(nums,numsSize,sizeof(int),comp);
	if(nums[0]>0){
		return ret_arr;
	}
	int i,l,r;
	for(i=0;i<numsSize-2;i++){
		//第一个数大于0，后面和肯定大于0
		if(nums[i]>0) break;
		//跳过重复的值
		if(i>0&&nums[i]==nums[i-1]) continue;
		l=i+1;
		r=numsSize-1;
		while(l<r){
			int sum = nums[i]+nums[l]+nums[r];
			if(sum==0){
				int *res = (int *)malloc(sizeof(int)*3);
				res[0]=nums[i];
				res[1]=nums[l++];
				res[2]=nums[r--];
				ret_arr = add_res(ret_arr,returnSize,&max_size,res);
				//跳过重复的值
				while(l<numsSize && nums[l]==nums[l-1]) l++;
				while(r>=0 && nums[r]==nums[r+1]) r--;
			}else if(sum<0){
				l++;
			}else{
				r--;
			}
		}
	}
	return ret_arr;
 }

int main(int argc,char **argv){
//	int num[ARRAY_LEH]={3,4,1,5,6,3,-9};
	int num[ARRAY_LEH]={-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6};
	int i,returnSize;
	int **res = threeSum(num,ARRAY_LEH,&returnSize);
	for(i=0;i<returnSize;i++){
		printf("%d,%d,%d",res[i][0],res[i][1],res[i][2]);
		printf("\n");
	}
	return 0;
}
