/*
 * 26. Remove Duplicates from Sorted Array
 * https://leetcode.com/problems/remove-duplicates-from-sorted-array/
Easy

Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

Example 1:

Given nums = [1,1,2],

Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.

It doesn't matter what you leave beyond the returned length.

Example 2:

Given nums = [0,0,1,1,1,2,2,3,3,4],

Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.

It doesn't matter what values are set beyond the returned length.

Clarification:

Confused why the returned value is an integer but your answer is an array?

Note that the input array is passed in by reference, which means modification to the input array will be known to the caller as well.

Internally you can think of this:

// nums is passed in by reference. (i.e., without making a copy)
int len = removeDuplicates(nums);

// any modification to nums in your function would be known by the caller.
// using the length returned by your function, it prints the first len elements.
for (int i = 0; i < len; i++) {
    print(nums[i]);
}
 */

#include<stdio.h>

int removeDuplicates(int* nums, int numsSize) {
	if(numsSize<=0) return 0;
    int i,j=0,size=0;
	nums[size++]=nums[0];
    for(i=1;i<numsSize;i++){
		if(nums[i] != nums[j]){
			nums[size++]=nums[i];
			j=i;
		}
	}
	return size;
}

int main(int argc,char** argv){
	int nums[]={0,0,1,1,1,2,2,3,3,4};
	int i,size=10;
	size=removeDuplicates(nums,size);
	printf("return size=%d\n",size);
	for(i=0;i<size;i++){
		printf("%d,",nums[i]);
	}
	printf("\n");
	return 0;
}
