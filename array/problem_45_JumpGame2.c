/*

45. Jump Game II  https://leetcode.com/problems/jump-game-ii/
Hard

1070

53

Favorite

Share
Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

Example:

Input: [2,3,1,1,4]
Output: 2
Explanation: The minimum number of jumps to reach the last index is 2.
    Jump 1 step from index 0 to 1, then 3 steps to the last index.
Note:

You can assume that you can always reach the last index.
 */


#include <stdio.h>

int min(int a,int b){
	return a<b?a:b;
}

// 从右往左找，max_jum保存每个达到终点的最小跳跃次数，结果Time Limited Execute了
int jump_1(int* nums, int numsSize) {
    int i,j,jump_cnt;
    int max_jum[numsSize]; // 保存每个位置最小跳的次数 
    max_jum[numsSize-1]=0;
    for(i=numsSize-2;i>=0;i--){
		if(nums[i] <= 0){
			max_jum[i]=-1;
			continue;
		}
		jump_cnt=numsSize;
		for(j=1;j<=nums[i];j++){
//			cnt++;
			if(i+j >= numsSize)break;
			if(max_jum[i+j]==-1)continue;
			jump_cnt = min(jump_cnt,max_jum[i+j]+1);
		}
		max_jum[i]=jump_cnt;
	}
//	printf("cnt=%d\n",cnt);
	return max_jum[0];
}

int max(int a,int b){
	return a>b?a:b;
}

//参考discuss,从左往右遍历，查找每个位置能跳到最远的位置，
int jump(int* nums, int numsSize) {
	if(numsSize<2) return 0;
	int i,res=0;
	int curr=0;//for循环保存 当前位置能跳到的最大位置，在[i..last]之间包括这个位置之前的能跳到最大的值
	int last=0;//上一个位置能达到的最位置
	for(i=0;i<numsSize;i++){
		//计算在[i..last]之间能达到的最远距离
		curr=max(curr,nums[i]+i);
		//如果i达到上一次最大值，则从当前位置开始跳多一步
		if(i==last){
			last=curr;
			res++;
			if(curr>=numsSize-1) break;	//超过数组最大值，退出循环
		}
	}
	return res;
}


int main(int argc,char **argv){
	int num[]={25000,24999,24998,24997,24996,24995,24994,24993,24992,24991,24990,24989,24988,24987,24986,24985,24984,24983,24982,24981,24980,24979,24978,24977,24976,24975,24974,24973,24972,24971,24970,24969,24968,24967,24966,24965,24964,24963,24962,24961,24960,24959,24958,24957,24956,24955,24954,24953,24952,24951,24950,24949,24948,24947,24946,24945,24944,24943,24942,24941,24940,24939,24938,24937,24936,24935,24934,24933,24932,24931,24930,24929,24928,24927,24926,24925,24924,24923,24922,24921,24920,24919,24918,24917,24916,24915,24914,24913,24912,24911,24910,24909,24908,24907,24906,24905,24904,24903,24902,24901,24900,24899,24898,24897,24896,24895,24894,24893,24892,24891,24890,24889,24888,24887,24886,24885,24884,24883,24882,24881,24880,24879,24878,24877,24876,24875,24874,24873,24872,24871,24870,24869,24868,24867,24866,24865,24864,24863,24862,24861,24860,24859,24858,24857,24856,24855,24854,24853,24852,24851,24850,24849,24848,24847,24846,24845,24844,24843,24842,24841,24840,24839,24838,24837,24836,24835,248};
	int i,len=sizeof(num)/sizeof(*num);
	int res = jump(num,len);
	printf("ret_size=%d\n",res);
//	for(i=0;i<returnSize;i++){
//		printf("%d,%d,%d,%d",res[i][0],res[i][1],res[i][2],res[i][3]);
//		printf("\n");
//	}
	return 0;
}
