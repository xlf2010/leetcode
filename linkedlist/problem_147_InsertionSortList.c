/**
147. Insertion Sort List	https://leetcode.com/problems/insertion-sort-list/
Medium

348

407

Favorite

Share
Sort a linked list using insertion sort.


A graphical example of insertion sort. The partial sorted list (black) initially contains only the first element in the list.
With each iteration one element (red) is removed from the input data and inserted in-place into the sorted list
 

Algorithm of Insertion Sort:

Insertion sort iterates, consuming one input element each repetition, and growing a sorted output list.
At each iteration, insertion sort removes one element from the input data, finds the location it belongs within the sorted list, and inserts it there.
It repeats until no input elements remain.

Example 1:

Input: 4->2->1->3
Output: 1->2->3->4
Example 2:

Input: -1->5->3->4->0
Output: -1->0->3->4->5
Accepted
148,346
Submissions
399,253

*/



#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//Runtime: 8 ms, faster than 100.00% of C online submissions for Insertion Sort List.
//Memory Usage: 7.9 MB, less than 86.36% of C online submissions for Insertion Sort List.
//插入排序
struct ListNode* insertionSortList(struct ListNode* head){
	if(!head || !head->next) return head;
	struct ListNode *pre=head,*curr=head->next;	//pre保存上一节点指针，curr保存下一节点
	struct ListNode *p,*pp;	//保存遍历插入位置的当前节点与上一节点
	while(curr){
		// 1. 当前节点小于上一节点，不需处理
		if(pre->val <= curr->val){
			pre=curr;
			curr=curr->next;
			continue;
		}
		// 2. 插入位置为头节点
		p=head;
		if(curr->val <= p->val){
			pre->next=curr->next;
			curr->next=p;
			head=curr;
			curr=pre->next;			
			continue;
		}
		
		// 3. 寻找插入位置
		while(p->val < curr->val){
			pp=p;
			p=p->next;
		}
		
		pre->next=curr->next;
		curr->next=pp->next;
		pp->next=curr;
		
		curr=pre->next;
	}
	return head;
}



int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 32;
	l2[i++].next = l2+(i+1);
	l2[i].val = 15;
	l2[i++].next = l2+(i+1);
	l2[i].val = 23;
	l2[i++].next = l2+(i+1);
	l2[i].val = 14;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 22;
	l2[i++].next = l2+(i+1);
	l2[i].val = 12;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res =
	insertionSortList(l2);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}