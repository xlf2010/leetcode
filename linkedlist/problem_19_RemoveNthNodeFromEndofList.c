/**
19. Remove Nth Node From End of List  https://leetcode.com/problems/remove-nth-node-from-end-of-list/
Medium

1742

131

Favorite

Share
Given a linked list, remove the n-th node from the end of list and return its head.

Example:

Given linked list: 1->2->3->4->5, and n = 2.

After removing the second node from the end, the linked list becomes 1->2->3->5.
Note:

Given n will always be valid.

Follow up:

Could you do this in one pass?

*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//删除链表倒数第N个节点,定义两个指针_head,_tail,_tail-_head位置差值为n+1，最后将_head的下一节点删除即可，如果n==链表长度，则删除头结点
struct ListNode* removeNthFromEnd(struct ListNode* head, int n){
	if(!head->next){
		free(head);
		return NULL;
	}
	struct ListNode *_head=head;
	struct ListNode *_tail=head;
	int diff=0,size=0;	
	while(_tail){
		if(diff>n){			
			_head=_head->next;
		}else{
			diff++;
		}
		size++;
		_tail=_tail->next;
	}
	//移除头部节点,移除的位置与链表长度一致
	if(n==size){
		_tail=_head;
		head=head->next;
	}else{
		_tail=_head->next;
		_head->next=_tail->next;
	}

//	free(_tail);
	return head;
}



int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	int i=0;
//	l1[i].val = 7;
//	l1[i].next = l1+(++i);
	l1[i].val = 5;
	l1[i++].next = l1+(i+1);
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
	l1[i].val = 2;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 5;
//	l2[i++].next = l2+(i+1);
//	l2[i].val = 6;
//	l2[i++].next = l2+(i+1);
//	l2[i].val = 4;
	l2[i++].next = NULL;
	
	struct ListNode* res = removeNthFromEnd(l1,3);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}


