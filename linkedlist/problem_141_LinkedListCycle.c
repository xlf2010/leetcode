/**
141. Linked List Cycle		https://leetcode.com/problems/linked-list-cycle/
Easy

1510

159

Favorite

Share
Given a linked list, determine if it has a cycle in it.

To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.

 

Example 1:

Input: head = [3,2,0,-4], pos = 1
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the second node.


Example 2:

Input: head = [1,2], pos = 0
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the first node.


Example 3:

Input: head = [1], pos = -1
Output: false
Explanation: There is no cycle in the linked list.


 
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


struct ListNode {
    int val;
    struct ListNode *next;
};

//Runtime: 4 ms, faster than 99.91% of C online submissions for Linked List Cycle.
//Memory Usage: 8.5 MB, less than 49.19% of C online submissions for Linked List Cycle.
// Time: O(n) , Space: (1)
//查看链表是否有环，定义两个指针，第一个后移一步，第二个后移两步，相遇则说明有环，
bool hasCycle(struct ListNode *head) {
	if(!head || !head->next) return false;
    struct ListNode *p2=head->next;
	while(p2){
		if(head==p2) return true;
		if(!p2->next) return false;
		p2=p2->next;	
		if(!p2->next) return false;
		p2=p2->next;
		head=head->next;
	}
	return false;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = -10;
	l1[i++].next = l1+(i+1);
	l1[i].val = -3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 0;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = l1+(i+1);
	l1[i].val = 9;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 0;
	l2[i++].next = l2+(i+1);
	l2[i].val = 1;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
/* 	struct ListNode* res = hasCycle(l2);

 	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p++;
	}  */
	bool cyc = hasCycle(l2);
	printf("%d\n",cyc);
	return 0;
}