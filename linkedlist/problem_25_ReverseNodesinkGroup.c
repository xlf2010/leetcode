/**
25. Reverse Nodes in k-Group https://leetcode.com/problems/reverse-nodes-in-k-group/
Hard

1133

241

Favorite

Share
Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

k is a positive integer and is less than or equal to the length of the linked list. 
If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

Example:

Given this linked list: 1->2->3->4->5

For k = 2, you should return: 2->1->4->3->5

For k = 3, you should return: 3->2->1->4->5

Note:

Only constant extra memory is allowed.
You may not alter the values in the list's nodes, only nodes itself may be changed.

*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//获取链表长度
int get_list_len(struct ListNode* p){
	int len=0;
	while(p){
		len++;
		p=p->next;
	}
	return len;
}

//Runtime: 8 ms, faster than 97.30% of C online submissions for Reverse Nodes in k-Group.
//Memory Usage: 8.3 MB, less than 68.78% of C online submissions for Reverse Nodes in k-Group.
//每达到链表k倍数位置后翻转链表
struct ListNode* reverseKGroup(struct ListNode* head, int k){
	if(k<=1 || !head || !head->next) return head;
	int len = get_list_len(head);
	
	if(k>len) return head;
	
	int i,count=1,reverse_count = len / k;
	
	struct ListNode *curr=head->next;	//当前节点
	struct ListNode *pre=head;			//当前节点的上一节点
	struct ListNode *next=curr->next;	//当前节点的下一节点
	struct ListNode *curr_tail=head;	//当前段的尾节点
	struct ListNode *pre_tail=head;		//前一段的尾节点
	// 分段翻转
	for(i=0;i<reverse_count;i++){
		while(count < k && curr){
			//翻转链表,将curr的下一节点指向pre,curr指向next
			next=curr->next;
			curr->next=pre;
			pre=curr;
			curr=next;
			count++;
		}
		//达到k之后，重置,将尾节点指向下一段的起始节点,tail->next指向下一段起点,i==0时修改头结点即可
		if(i==0) {
			head=pre;
		}
		//上一段的尾节点指向当前头节点
		else {
			pre_tail->next=pre;
		}
		//curr已是下一段的起始点
		pre_tail = curr_tail;
		pre=curr_tail=curr;
		if(curr){
			curr=curr->next;
		}
		count=1;
	}
	//pre是下一段起点的第一个节点
	pre_tail->next=pre;
	return head;
}


int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
//	l1[i].val = 2;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
//	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = reverseKGroup(l2,7);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}


