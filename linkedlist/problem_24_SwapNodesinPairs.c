/**
24. Swap Nodes in Pairs  https://leetcode.com/problems/swap-nodes-in-pairs/
Medium

1086

98

Favorite

Share
Given a linked list, swap every two adjacent nodes and return its head.

You may not modify the values in the list's nodes, only nodes itself may be changed.

 

Example:

Given 1->2->3->4, you should return the list as 2->1->4->3.
*/

 
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

struct ListNode* create_node(int val){
	struct ListNode* n=(struct ListNode *)malloc(sizeof(struct ListNode ));
	n->val=val;
	n->next=NULL;
	return n;
}
/**
 * Time Submitted Status Runtime Memory Language
    a minute ago	Accepted	0 ms	7.2 MB	c
 */
struct ListNode* swapPairs_1(struct ListNode* head){
	if(!head) return NULL;
	if(!head->next) return head;
	//第一二节点
	struct ListNode *res_head=(struct ListNode *)malloc(sizeof(struct ListNode ));
	struct ListNode *res_curr=(struct ListNode *)malloc(sizeof(struct ListNode ));
	res_head->val=head->next->val;
	res_head->next=res_curr;
	res_curr->val=head->val;
	res_curr->next=NULL;
	struct ListNode *p,*res_pre,*curr=head->next->next;
	
	while(curr){
		p=curr;
		if(curr->next){
			//下一节点的值
			res_pre=res_curr;
			res_curr=create_node(p->next->val);
			res_pre->next=res_curr;
			curr=curr->next;
		}
		//当前节点值
		res_pre=res_curr;
		res_curr=create_node(p->val);
		res_pre->next=res_curr;
		curr=curr->next;
	}
	return res_head;
}

//不创建新节点,运行时间更长，内存反而更大了
//Time Submitted Status Runtime Memory Language
//a minute ago	Accepted	4 ms	7.3 MB	c
struct ListNode* swapPairs(struct ListNode* head){
	if(!head || !head->next) return head;
	struct ListNode *p,*pre=head,*curr=head->next->next;
	//交换第一跟第二个节点
	head=head->next;
	head->next=pre;
	pre->next=curr;
	while(curr){
		//pre保存前一个节点，p保存当前节点
		p=curr;
		if(curr->next){
			//交换p，curr两节点
			curr=curr->next;
			pre->next=curr;
			p->next=curr->next;
			curr->next=p;
		}
		pre=p;
		curr=p->next;
	}
	return head;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = swapPairs(l1);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
