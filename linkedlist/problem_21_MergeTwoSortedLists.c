/**
21. Merge Two Sorted Lists	https://leetcode.com/problems/merge-two-sorted-lists/
Easy	

2175

309

Favorite

Share
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: 1->2->4, 1->3->4
Output: 1->1->2->3->4->4
*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 
  
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};


//Runtime: 0 ms, faster than 100.00% of C online submissions for Merge Two Sorted Lists.
//Memory Usage: 7.6 MB, less than 100.00% of C online submissions for Merge Two Sorted Lists.
//合并两个有序链表
struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2){
	//空值判断
	if(!l1 && !l2) return NULL;
	if(!l1) return l2;
	if(!l2) return l1;
	
	struct ListNode *p1=l1,*p2=l2;
	struct ListNode *head=(struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode *pre,*curr=head;
	//合并公共部分
	while(p1 && p2){
		if(p1->val>p2->val){
			curr->val=p2->val;
			p2=p2->next;
		}else{
			curr->val=p1->val;
			p1=p1->next;
		}
		pre=curr;
		curr=(struct ListNode*)malloc(sizeof(struct ListNode));
		curr->next=NULL;
		pre->next=curr;
	}
	//p1剩余部分
	while(p1){
		curr->val=p1->val;
		p1=p1->next;
		pre=curr;
		curr=(struct ListNode*)malloc(sizeof(struct ListNode));
		curr->next=NULL;
		pre->next=curr;
	}
	//p2剩余部分
	while(p2){
		curr->val=p2->val;
		p2=p2->next;
		pre=curr;
		curr=(struct ListNode*)malloc(sizeof(struct ListNode));
		curr->next=NULL;
		pre->next=curr;
	}
	pre->next=NULL;
	free(curr);
	return head;
}


int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 4);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	int i=0;
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = l1+(i+1);
	l1[i].val = 14;
	l1[i++].next = l1+(i+1);
	l1[i].val = 23;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 16;
	l2[i++].next = l2+(i+1);
	l2[i].val = 24;
	l2[i++].next = NULL;
	
	struct ListNode* res = mergeTwoLists(NULL,NULL);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
