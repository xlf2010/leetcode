/**
83. Remove Duplicates from Sorted List https://leetcode.com/problems/remove-duplicates-from-sorted-list/
Easy

736

78

Favorite

Share
Given a sorted linked list, delete all duplicates such that each element appear only once.

Example 1:

Input: 1->1->2
Output: 1->2
Example 2:

Input: 1->1->2->3->3
Output: 1->2->3
*/


#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};
//Runtime: 4 ms, faster than 99.01% of C online submissions for Remove Duplicates from Sorted List.
//Memory Usage: 7.7 MB, less than 35.34% of C online submissions for Remove Duplicates from Sorted List.
//有序链表删除重复元素，只保留一个值
struct ListNode* deleteDuplicates(struct ListNode* head){
	if(!head || !head->next) return head;
	struct ListNode *pre=head,*p=head->next;
	while(p){
		if(pre->val == p->val){
			pre->next=p->next;
			free(p);
			p=pre->next;
		}else{
			pre=p;
			p=p->next;
		}
	}
	return head;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
//	l1[i].val = 2;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
//	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 1;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 8;
	l2[i++].next = l2+(i+1);
	l2[i].val = 9;
	l2[i++].next = NULL;
	
	struct ListNode* ll1 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll1->val=1;
	ll1->next=NULL;
	struct ListNode* ll2 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll2->val=1;
	ll2->next=ll1;
	struct ListNode* ll3 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll3->val=0;
	ll3->next=ll2;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = deleteDuplicates(ll3);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
