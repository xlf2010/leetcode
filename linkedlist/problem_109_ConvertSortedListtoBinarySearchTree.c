/**
109. Convert Sorted List to Binary Search Tree		https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/
Medium

962

67

Favorite

Share
Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

Example:

Given the sorted linked list: [-10,-3,0,5,9],

One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
 
Accepted
174,353
Submissions
428,915
*/


#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//获取链表长度
int get_list_len(struct ListNode* p){
	int len=0;
	while(p){
		len++;
		p=p->next;
	}
	return len;
}

//递归设置每个子树的根节点的左右节点
struct TreeNode* setTreeNode(struct TreeNode* t,int start,int end){
	//开始超过结束，说明是叶子节点，不需设置
	if(start>end) return NULL;
	//相等情况，返回数组的头为节点
	if(start==end ) return t + start;
	int mid = start +((end-start)>>1);
	//递归左子树
	if(!t[mid].left){
		t[mid].left = setTreeNode(t,start,mid-1);
	}
	//递归右子树
	if(!t[mid].right){
		t[mid].right = setTreeNode(t,mid+1,end);
	}
	//返回根节点
	return t+mid;
}

//Runtime: 8 ms, faster than 100.00% of C online submissions for Convert Sorted List to Binary Search Tree.
//Memory Usage: 11 MB, less than 100.00% of C online submissions for Convert Sorted List to Binary Search Tree.
// Time：O(n) n为链表长度，Space O(1)
//将链表转换成tree数组，然后递归分块生成，每个数组中间点为跟节点，小于的为左子树，大于为右子树。然后返回数组中间节点
struct TreeNode* sortedListToBST(struct ListNode* head){
	if(!head) return NULL;
	int len=get_list_len(head);
	struct TreeNode *t = (struct TreeNode *)malloc(sizeof(struct TreeNode) * len);
	struct ListNode *p=head;
	int i=0;
	while(p){
		t[i].left=NULL;
		t[i].right=NULL;
		t[i++].val = p->val;		
		p=p->next;
	}
	setTreeNode(t,0,len-1);
	return t+((len-1)/2);
}

void printTree(struct TreeNode* t){
	if(!t) return;
	printf("%d->",t->val);
	printTree(t->left);
	
	printTree(t->right);
	
	
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = -10;
	l1[i++].next = l1+(i+1);
	l1[i].val = -3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 0;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = l1+(i+1);
	l1[i].val = 9;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 0;
	l2[i++].next = l2+(i+1);
	l2[i].val = 1;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
//	l2[i].val = 4;
//	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct TreeNode* res = sortedListToBST(l2);
	printTree(res);
/* 	struct TreeNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p++;
	} */
	printf("\n");
	return 0;
}