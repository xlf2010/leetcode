/**
143. Reorder List
Medium

842

64

Favorite

Share
Given a singly linked list L: L0→L1→…→Ln-1→Ln,
reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…

You may not modify the values in the list's nodes, only nodes itself may be changed.

Example 1:

Given 1->2->3->4, reorder it to 1->4->2->3.
Example 2:

Given 1->2->3->4->5, reorder it to 1->5->2->4->3.
Accepted
152,889
Submissions
498,034

*/

#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//获取链表长度
int get_list_len(struct ListNode* p){
	int len=0;
	while(p){
		len++;
		p=p->next;
	}
	return len;
}
//Runtime: 12 ms, faster than 100.00% of C online submissions for Reorder List.
//Memory Usage: 10.5 MB, less than 85.80% of C online submissions for Reorder List.
void reorderList(struct ListNode* head){
	if(!head || !head->next) return;
	int len=get_list_len(head),arr_len = len >> 1;
	//长度为奇数，则超过 len/2+1保存到指针数组，偶数就保存 len/2个
	int i = (len & 1) ? 0 : 1,j=0;
	//指针数组，保存链表后半段地址
	struct ListNode *ns[arr_len];
	struct ListNode *nxt,*p=head;
	while(p){
		//超过一半以上的节点地址，即后半段保存到数组
		if(i>arr_len){
			ns[j++]=p;
		}
		i++;
		p=p->next;
	}
	i=arr_len-1;
	//前半段从头遍历，后半段反向遍历
	p=head;
	for(j=arr_len-1;j>=0;j--){
		nxt=p->next;
		p->next=ns[j];
		ns[j]->next=nxt;
		p=nxt;
	}
	//中间节点为链表尾节点
	p->next=NULL;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 22;
	l2[i++].next = l2+(i+1);
	l2[i].val = 12;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
//	struct ListNode* res =
	reorderList(l2);
	struct ListNode* p = l2;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}