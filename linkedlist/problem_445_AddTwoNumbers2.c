/**
445. Add Two Numbers II https://leetcode.com/problems/add-two-numbers-ii/
Medium

705

90

Favorite

Share
You are given two non-empty linked lists representing two non-negative integers. The most significant digit comes first and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Follow up:
What if you cannot modify the input lists? In other words, reversing the lists is not allowed.

Example:

Input: (7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 8 -> 0 -> 7
*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//求链表长度
int linked_len(struct ListNode* p){
	int len=0;
	while(p){
		len++;
		p=p->next;
	}
	return len;
}

int max(int a,int b){
	return a>b?a:b;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2){
	int len1=linked_len(l1);
	int len2=linked_len(l2);

	int i=1,size=max(len1,len2) + 1;
	struct ListNode *curr,*pre,*p1=l1,*p2=l2,*head=(struct ListNode*)malloc(sizeof(struct ListNode) * size);
	//寻找l1起点值
	pre=head;
	head->next=curr;
	while(len1>len2){
		curr=head+i;
		curr->next=NULL;
		curr->val=p1->val;
		pre->next=curr;
		pre=curr;
		p1=p1->next;
		len1--;
		i++;
	}
	
	//寻找l2起点值
	while(len2>len1){
		curr=head+i;
		curr->next=NULL;
		curr->val=p2->val;
		
		pre->next=curr;
		pre=curr;
		p2=p2->next;
		len2--;
		i++;
	}
	//共同的长度
	while(len1){
		curr=head+i;
		curr->val = p1->val+p2->val;
		curr->next=NULL;
		
		pre->next=curr;
		pre=curr;
		p1=p1->next;
		p2=p2->next;
		len1--;
		i++;
	}
	//倒叙判断是否超过10，否则往左进位
	int carry=0;
	for(i=size-1;i>0;i--){
		(head+i)->val += carry;
		if((head+i)->val >= 10){
			carry = 1;
			(head+i)->val -=10;
		}else{
			carry=0;
		}
	}
	if(carry){
		head->val=carry;
	}else{
		head++;
	}
	return head;
}


int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	int i=0;
//	l1[i].val = 7;
//	l1[i].next = l1+(++i);
	l1[i].val = 5;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 4;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 5;
//	l2[i++].next = l2+(i+1);
//	l2[i].val = 6;
//	l2[i++].next = l2+(i+1);
//	l2[i].val = 4;
	l2[i++].next = NULL;
	
	struct ListNode* res = addTwoNumbers(l1,l2);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
