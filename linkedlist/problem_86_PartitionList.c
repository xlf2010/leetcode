/**
86. Partition List	https://leetcode.com/problems/partition-list/
Medium

636

173

Favorite

Share
Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

Example:

Input: head = 1->4->3->2->5->2, x = 3
Output: 1->2->2->4->3->5

*/

#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//Runtime: 0 ms, faster than 100.00% of C online submissions for Partition List.
//Memory Usage: 7.4 MB, less than 5.59% of C online submissions for Partition List.
//链表分块，小于x的在左边，大于等于x的在右边，相对位置不变
struct ListNode* partition(struct ListNode* head, int x){
	if(!head || !head->next) return head;
	
	//分别为p遍历节点，head小于x的头结点，less小于x的尾节点，more_head大于等于x的头结点,more大于等于x的尾节点
	struct ListNode *p=head,*less=NULL,*more_head=NULL,*more=NULL;
	head=NULL;
	while(p){
		if(p->val<x){
			//设置头节点
			if(!head) {
				less=head=p;
			}else{
				less->next=p;
				less=p;
			}
			p=less->next;
		}else{
			if(!more_head) {
				more_head=more=p;
			}else{
				more->next=p;
				more=p;
			}
			p=more->next;
		}
	}
	//没有小于x的，头结点为大于x的头结点
	if(!head) head=more_head;
	//将小于x的尾节点的下一节点指向大于等于x的头结点
	if(less) less->next=more_head;
	//将大于等于x的尾节点置空
	if(more) more->next=NULL;
	return head;
}



int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
//	l1[i].val = 2;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
//	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 1;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
//	l2[i].val = 8;
//	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = NULL;
	
	struct ListNode* ll1 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll1->val=3;
	ll1->next=NULL;
	struct ListNode* ll2 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll2->val=2;
	ll2->next=NULL;
	struct ListNode* ll3 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll3->val=2;
	ll3->next=ll2;
	struct ListNode* ll4 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll4->val=1;
	ll4->next=ll3;
	struct ListNode* ll5 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll5->val=1;
	ll5->next=ll4;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = partition(l2,110);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
