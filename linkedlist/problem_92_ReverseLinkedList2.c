/**

92. Reverse Linked List II  https://leetcode.com/problems/reverse-linked-list-ii/
Medium

1158

86

Favorite

Share
Reverse a linked list from position m to n. Do it in one-pass.

Note: 1 ≤ m ≤ n ≤ length of list.

Example:

Input: 1->2->3->4->5->NULL, m = 2, n = 4
Output: 1->4->3->2->5->NULL
Accepted
192,163
Submissions
552,007

*/

#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//Runtime: 4 ms, faster than 95.01% of C online submissions for Reverse Linked List II.
//Memory Usage: 7.1 MB, less than 61.23% of C online submissions for Reverse Linked List II.
//one pass
struct ListNode* reverseBetween(struct ListNode* head, int m, int n){
	if(!head || m==n) return head;
	int i=0;
	struct ListNode *p=head,*pre=head,*pm,*nxt;
	while(i<=n){
		i++;
		//在(m..n)中间的节点，后节点指向前节点
		if(i>m && i<n){
			nxt=p->next;
			p->next=pre;
			pre=p;
			p=nxt;			
		}else if(i==m){
			//pm保存位置m的节点的前一节点，头节点m==1的话pre=pm
			pm=pre;
			pre=p;			
			p=p->next;
		}else if(i==n){
			//头节点处理m=1,pre=pm=head,
			if(m==1) {
				head=p;
				pm->next=p->next;
			}else{
				pm->next->next=p->next;
				pm->next=p;
			}
			p->next=pre;			
		}else{
			pre=p;			
			p=p->next;
		}
	}
	return head;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 3;
	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 22;
	l2[i++].next = l2+(i+1);
	l2[i].val = 12;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = reverseBetween(l2,1,2);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}