/**
82. Remove Duplicates from Sorted List II
Medium

799

70

Favorite

Share
Given a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.

Example 1:

Input: 1->2->3->3->4->4->5
Output: 1->2->5
Example 2:

Input: 1->1->1->2->3
Output: 2->3
*/


#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};


//有序链表删除重复元素，不保留
struct ListNode* deleteDuplicates(struct ListNode* head){
	//空或只有一个节点
	if(!head || !head->next) return head;

	struct ListNode *pre=head,*p=head,*next=p->next;
	//0标识是否重复，1标识是否有头节点
	int flag = 0;
	//find head
	while(p){
		//查找最近两个不同的节点p,next,如果p与next相等，删除next节点，next后移，直到p与next不相等
		while(next && p->val==next->val){
			p->next=next->next;
			free(next);
			next=p->next;
			flag |= 1;
		}
		//删除相等的头节点
		if((flag & 1) == 1){
			pre->next=p->next;
			free(p);
			p=pre;
			flag &= ~1;
		}else{
			next=p;
		}
		//设置头节点
		if((flag & 2) == 0){
			head=next;
			flag |= 2;
			p=head;
		}
		pre=p;
		if(p) p=p->next;
		if(p) next=p->next;
	}
	return head;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
//	l1[i].val = 2;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
//	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 1;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 8;
	l2[i++].next = l2+(i+1);
	l2[i].val = 9;
	l2[i++].next = NULL;
	
	struct ListNode* ll1 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll1->val=3;
	ll1->next=NULL;
	struct ListNode* ll2 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll2->val=2;
	ll2->next=NULL;
	struct ListNode* ll3 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll3->val=2;
	ll3->next=ll2;
	struct ListNode* ll4 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll4->val=1;
	ll4->next=ll3;
	struct ListNode* ll5 = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll5->val=1;
	ll5->next=ll4;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = deleteDuplicates(ll5);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
