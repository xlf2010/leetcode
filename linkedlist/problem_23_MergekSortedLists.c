/**
23. Merge k Sorted Lists https://leetcode.com/problems/merge-k-sorted-lists/
Hard

2400

152

Favorite

Share
Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.

Example:

Input:
[
  1->4->5,
  1->3->4,
  2->6
]
Output: 1->1->2->3->4->4->5->6

*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 
#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//Runtime: 16 ms, faster than 82.90% of C online submissions for Merge k Sorted Lists.
//Memory Usage: 9.4 MB, less than 68.17% of C online submissions for Merge k Sorted Lists.
//合并两个有序链表 Time：O(Nlogk) N为两个链表总长，K为K个待合并的链表，Space：O(1)
struct ListNode* mergeTwoLists(struct ListNode* l1, struct ListNode* l2){
	//空值判断
	if(!l1 && !l2) return NULL;
	if(!l1) return l2;
	if(!l2) return l1;
	
	struct ListNode *p1=l1,*p2=l2;
	struct ListNode *head;
	struct ListNode *pre,*curr;
	
	if(p1->val > p2->val){
		head=p2;
		p2=p2->next;
	}else{
		head=p1;
		p1=p1->next;
	}
	curr=head;
	//合并公共部分
	while(p1 && p2){
		pre=curr;
		if(p1->val>p2->val){
			curr=p2;
			p2=p2->next;
		}else{
			curr=p1;
			p1=p1->next;
		}
		curr->next=NULL;
		pre->next=curr;
	}
	//p1剩余部分
	while(p1){
		pre=curr;
		curr=p1;
		p1=p1->next;		
		curr->next=NULL;
		pre->next=curr;
	}
	//p2剩余部分
	while(p2){
		pre=curr;
		curr=p2;
		p2=p2->next;
		curr->next=NULL;
		pre->next=curr;
	}
	return head;
}

// 分块 归并排序
struct ListNode* merge(struct ListNode** lists,int start,int end){
	
	if( end<=start ) return lists[start];
	int mid = start + ((end-start) >> 1);
	struct ListNode* left = merge(lists,start,mid);
	struct ListNode* right = merge(lists,mid+1,end);
	struct ListNode* res = mergeTwoLists(left,right);
	return res;
}

struct ListNode* mergeKLists(struct ListNode** lists, int listsSize){
	if(!lists || listsSize <= 0) return NULL;
	return merge(lists,0,listsSize-1);
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 4);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 3);
	int i=0;
	l1[i].val = 2;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = l1+(i+1);
	l1[i].val = 14;
	l1[i++].next = l1+(i+1);
	l1[i].val = 23;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 16;
	l2[i++].next = l2+(i+1);
	l2[i].val = 24;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = mergeKLists(l,2);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
