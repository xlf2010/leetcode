/**
61. Rotate List		https://leetcode.com/problems/rotate-list/
Medium

581

804

Favorite

Share
Given a linked list, rotate the list to the right by k places, where k is non-negative.

Example 1:

Input: 1->2->3->4->5->NULL, k = 2
Output: 4->5->1->2->3->NULL
Explanation:
rotate 1 steps to the right: 5->1->2->3->4->NULL
rotate 2 steps to the right: 4->5->1->2->3->NULL
Example 2:

Input: 0->1->2->NULL, k = 4
Output: 2->0->1->NULL
Explanation:
rotate 1 steps to the right: 2->0->1->NULL
rotate 2 steps to the right: 1->2->0->NULL
rotate 3 steps to the right: 0->1->2->NULL
rotate 4 steps to the right: 2->0->1->NULL

*/


#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int val;
    struct ListNode *next;
};

//获取链表长度
int get_list_len(struct ListNode* p,struct ListNode** tail){
	int len=0;
	while(p){
		len++;
		*tail=p;
		p=p->next;
	}
	return len;
}
//Runtime: 4 ms, faster than 97.07% of C online submissions for Rotate List.
//Memory Usage: 7.3 MB, less than 94.72% of C online submissions for Rotate List.
//将链表尾节点放到头结点，执行k次，执行第i与len+i次效果是一致的，因此将倒数第 k%len 个节点之后的放到头结点即可
struct ListNode* rotateRight(struct ListNode* head, int k){
	if(!head || !head->next) return head;
	struct ListNode* tail;
	int len=get_list_len(head,&tail);
	int i,pos = k % len;
	//刚好整除，不需动链表
	if(!pos) return head;
	struct ListNode* p=head;
	for(i=1;i<len-pos;i++){
		p=p->next;
	}
	tail->next=head;
	head=p->next;
	p->next=NULL;
	return head;
}


int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 5);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 7);
	int i=0;
	l1[i].val = 1;
	l1[i++].next = l1+(i+1);
//	l1[i].val = 2;
//	l1[i++].next = l1+(i+1);
//	l1[i].val = 3;
//	l1[i++].next = l1+(i+1);
	l1[i].val = 4;
	l1[i++].next = l1+(i+1);
	l1[i].val = 5;
	l1[i++].next = NULL;
	
	i=0;
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 5;
	l2[i++].next = l2+(i+1);
	l2[i].val = 3;
	l2[i++].next = l2+(i+1);
	l2[i].val = 4;
	l2[i++].next = l2+(i+1);
	l2[i].val = 6;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = l2+(i+1);
	l2[i].val = 2;
	l2[i++].next = NULL;
	
	struct ListNode* ll = (struct ListNode*)malloc(sizeof(struct ListNode));
	ll->val=1;
	ll->next=NULL;
	struct ListNode** l=(struct ListNode**)malloc(sizeof(struct ListNode *) * 2);
	l[0]=l1;
	l[1]=l2;
	
	struct ListNode* res = rotateRight(l2,6);
	struct ListNode* p = res;
	i=0;
	while(p && (i++)<10){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
