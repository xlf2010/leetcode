package graph;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 310. Minimum Height Trees Medium
 * 
 * For an undirected graph with tree characteristics, we can choose any node as
 * the root. The result graph is then a rooted tree. Among all possible rooted
 * trees, those with minimum height are called minimum height trees (MHTs).
 * Given such a graph, write a function to find all the MHTs and return a list
 * of their root labels.
 * 
 * Format The graph contains n nodes which are labeled from 0 to n - 1. You will
 * be given the number n and a list of undirected edges (each edge is a pair of
 * labels).
 * 
 * You can assume that no duplicate edges will appear in edges. Since all edges
 * are undirected, [0, 1] is the same as [1, 0] and thus will not appear
 * together in edges.
 * 
 * Example 1 :
 * 
 * Input: n = 4, edges = [[1, 0], [1, 2], [1, 3]]
 * 
 * 0 | 1 / \ 2 3
 * 
 * Output: [1]
 * 
 * Example 2 :
 * 
 * Input: n = 6, edges = [[0, 3], [1, 3], [2, 3], [4, 3], [5, 4]]
 * 
 * 0 1 2 \ | / 3 | 4 | 5
 * 
 * Output: [3, 4]
 * 
 * Note:
 * 
 * According to the definition of tree on Wikipedia: “a tree is an undirected
 * graph in which any two vertices are connected by exactly one path. In other
 * words, any connected graph without simple cycles is a tree.” The height of a
 * rooted tree is the number of edges on the longest downward path between the
 * root and a leaf.
 * 
 * 
 * 
 * @author harris
 * 
 * @see <a href=
 *      "https://leetcode.com/problems/minimum-height-trees/">minimum-height-trees</a>
 *
 * 
 */
public class Problem_310_MinimumHeightTrees {

	static class Node {
		public int val;
		public List<Node> outList;

		public Node() {
		}

		public Node(int val, List<Node> outList) {
			super();
			this.val = val;
			this.outList = outList;
		}
	}

	private List<Node> createGraph(int n, int[][] edges) {
		List<Node> nodes = new ArrayList<>(n);
		for (int i = 0; i < n; i++) {
			nodes.add(new Node(i, new ArrayList<>()));
		}
		for (int i = 0; i < edges.length; i++) {
			nodes.get(edges[i][0]).outList.add(nodes.get(edges[i][1]));
			nodes.get(edges[i][1]).outList.add(nodes.get(edges[i][0]));
		}
		return nodes;
	}

	/**
	 * 计算节点的最大深度,由于不存在环,可以深度遍历
	 * @param node
	 * @param currDeep
	 * @return
	 */
	private int findHeight(Node node, int currDeep) {
		int deep = 0;
		for (Node node2 : node.outList) {
			//来源节点不判断
			if (node2 == node) {
				continue;
			}
			currDeep ++;
			deep = findHeight(node2, currDeep);
			if(currDeep < deep) {
				currDeep = deep;
			}
		}
		return currDeep;
	}
	
	/**
	 * 查找树型图(无向无环图)的最小高度树的根节点,广度优先遍历,查找高度最小的树
	 * 
	 * @param n
	 * @param edges
	 * @return
	 */
	public List<Integer> findMinHeightTrees(int n, int[][] edges) {
		LinkedList<Integer> result = new LinkedList<>();
		// 记录当前最小深度的
		int minDeep = Integer.MAX_VALUE,currDeep;
		List<Node> nodes = createGraph(n, edges);
		for (Node node : nodes) {
			currDeep = findHeight(node, 1);
			//发现更小的,清空当前列表,重新构建结果集
			if (currDeep < minDeep) {
				result.clear();
				minDeep = currDeep;
				result.add(node.val);
			} else if (currDeep == minDeep) {
				result.add(node.val);
			}
		}
		return result;
	}

	public static void main(String[] args) {
		int numCourses = 4;
		int[][] prerequisites = new int[3][2];
		prerequisites[0][0] = 1;
		prerequisites[0][1] = 0;
		prerequisites[1][0] = 1;
		prerequisites[1][1] = 2;
		prerequisites[2][0] = 3;
		prerequisites[2][1] = 2;
//		prerequisites[3][0] = 0;
//		prerequisites[3][1] = 2;
//		System.out.println(Arrays.toString(new Problem_210_CourseSchedule3().findOrder(numCourses, prerequisites)));
	}
}
