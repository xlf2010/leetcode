package graph;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 133. Clone Graph Medium
 * 
 * Given a reference of a node in a connected undirected graph, return a deep
 * copy (clone) of the graph. Each node in the graph contains a val (int) and a
 * list (List[Node]) of its neighbors.
 * 
 * <img src="https://assets.leetcode.com/uploads/2019/02/19/113_sample.png"/>
 * 
 * Example:
 * 
 * Input:
 * {"$id":"1","neighbors":[{"$id":"2","neighbors":[{"$ref":"1"},{"$id":"3","neighbors":[{"$ref":"2"},{"$id":"4","neighbors":[{"$ref":"3"},{"$ref":"1"}],"val":4}],"val":3}],"val":2},{"$ref":"4"}],"val":1}
 * 
 * Explanation: Node 1's value is 1, and it has two neighbors: Node 2 and 4.
 * Node 2's value is 2, and it has two neighbors: Node 1 and 3. Node 3's value
 * is 3, and it has two neighbors: Node 2 and 4. Node 4's value is 4, and it has
 * two neighbors: Node 1 and 3.
 * 
 * 
 * 
 * Note:
 * 
 * The number of nodes will be between 1 and 100. The undirected graph is a
 * simple graph, which means no repeated edges and no self-loops in the graph.
 * Since the graph is undirected, if node p has node q as neighbor, then node q
 * must have node p as neighbor too. You must return the copy of the given node
 * as a reference to the cloned graph.
 * 
 * 
 * @author harris
 *
 */
public class Problem_133_CloneGraph {
	static class Node {
		public int val;
		public List<Node> neighbors;

		public Node() {
		}

		public Node(int _val, List<Node> _neighbors) {
			val = _val;
			neighbors = _neighbors;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append(String.format("ref:%d,val:%d,", this.hashCode(), val));
			sb.append("negihbor list:");
			for (Node node : neighbors) {
				sb.append(String.format("ref:%d,val:%d,", node.hashCode(), node.val));
			}
			return sb.toString();
		}
	};

	/**
	 * @param 复制图，递归深度度优先遍历
	 * @return
	 */
	public Node cloneGraph(Node node, HashMap<Node, Node> visited) {
		Node newNode = null, negihbor = null;
		if (visited.containsKey(node)) {
			newNode = visited.get(node);
		} else {
			newNode = new Node(node.val, new ArrayList<Node>(node.neighbors.size()));
			visited.put(node, newNode);
		}
		for (Node node2 : node.neighbors) {
			// 如果没有访问过的节点，new一个节点出来,入队列，访问过的话，新增一个连接
			if (!visited.containsKey(node2)) {
				negihbor = new Node(node2.val, new ArrayList<Node>(node2.neighbors.size()));
				newNode.neighbors.add(negihbor);
				visited.put(node2, negihbor);
				cloneGraph(node2, visited);
			} else {
				newNode.neighbors.add(visited.get(node2));
			}
		}
		return newNode;
	}

	// Runtime: 1 ms, faster than 100.00% of Java online submissions for Clone
	// Graph.
	// Memory Usage: 34.1 MB, less than 96.88% of Java online submissions for Clone
	// Graph.
	public Node cloneGraph(Node node) {
		if (node == null) {
			return node;
		}
		// old node --> new node
		HashMap<Node, Node> visited = new HashMap<>();
		return cloneGraph(node, visited);
	}

	public static void main(String[] args) {
		Node node1 = new Node(1, new ArrayList<>());
		Node node2 = new Node(2, new ArrayList<>());
		Node node3 = new Node(3, new ArrayList<>());
		Node node4 = new Node(4, new ArrayList<>());
		node1.neighbors.add(node2);
		node1.neighbors.add(node4);

		node2.neighbors.add(node1);
		node2.neighbors.add(node3);

		node3.neighbors.add(node2);
		node3.neighbors.add(node4);

		node4.neighbors.add(node3);
		node4.neighbors.add(node1);

		Problem_133_CloneGraph problem_133_CloneGraph = new Problem_133_CloneGraph();
		problem_133_CloneGraph.printGraph(node1);
		System.out.println("-------------------------------------------");
		Node newNode = problem_133_CloneGraph.cloneGraph(node1);
		problem_133_CloneGraph.printGraph(newNode);
	}

	private void printGraph(Node node) {
		LinkedList<Node> queue = new LinkedList<>();
		queue.offer(node);
		HashSet<Node> visited = new HashSet<>();
		while (!queue.isEmpty()) {
			node = queue.poll();
			System.out.println(node + " " + node.val);
			visited.add(node);
			for (Node node2 : node.neighbors) {
				if (!visited.contains(node2)) {
					visited.add(node2);
					queue.offer(node2);
				}
				System.out.println(node2 + " " + node2.val);
			}
		}
	}

}
