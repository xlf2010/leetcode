package graph;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 207. Course Schedule Medium
 * 
 * 
 * 
 * There are a total of n courses you have to take, labeled from 0 to n-1.
 * 
 * Some courses may have prerequisites, for example to take course 0 you have to
 * first take course 1, which is expressed as a pair: [0,1]
 * 
 * Given the total number of courses and a list of prerequisite pairs, is it
 * possible for you to finish all courses?
 * 
 * Example 1:
 * 
 * Input: 2, [[1,0]] Output: true Explanation: There are a total of 2 courses to
 * take. To take course 1 you should have finished course 0. So it is possible.
 * 
 * Example 2:
 * 
 * Input: 2, [[1,0],[0,1]] Output: false Explanation: There are a total of 2
 * courses to take. To take course 1 you should have finished course 0, and to
 * take course 0 you should also have finished course 1. So it is impossible.
 * 
 * Note:
 * 
 * The input prerequisites is a graph represented by a list of edges, not
 * adjacency matrices. Read more about how a graph is represented. You may
 * assume that there are no duplicate edges in the input prerequisites.
 * 
 * Accepted 233,127 Submissions 606,078
 * 
 * @author harris
 * 
 * @see <a href=
 *      "https://leetcode.com/problems/course-schedule/">course-schedule</a>
 *
 * 
 */
public class Problem_207_CourseSchedule {

	static class Node {
		public int val;
		public int inDegree;
		public List<Node> outList;

		public Node() {
		}

		public Node(int val, int inDegree, List<Node> outList) {
			super();
			this.val = val;
			this.inDegree = inDegree;
			this.outList = outList;
		}

	}

	/**
	 * 
	 * 判断有向图是否有环，有环表示不能完成，使用拓扑排序来判断,广度优先遍历BFS
	 * 
	 * 1. 记录入度数组
	 * 
	 * 2. 将入度为0的节点删除，关联的节点入度数减1
	 * 
	 * 3. 如果全部节点都移除了，返回true
	 * 
	 * 4. 如果有节点不能移除，说明有环(相互依赖)，不能完成返回false
	 * 
	 * 
	 * Runtime: 4 ms, faster than 76.74% of Java online submissions for Course
	 * Schedule.
	 * 
	 * Memory Usage: 44.6 MB, less than 90.61% of Java online submissions for Course
	 * Schedule.
	 * 
	 * @param numCourses    课程数
	 * @param prerequisites 课程依赖关系
	 * @return
	 */
	public boolean canFinish(int numCourses, int[][] prerequisites) {
		if (numCourses <= 0) {
			return true;
		}
		// 每个节点入度,按入度升序排列
		List<Node> list = new ArrayList<>(numCourses);
		for (int i = 0; i < numCourses; i++) {
			list.add(new Node(i, 0, new ArrayList<>()));
		}
		Node node = null;
		for (int i = 0; i < prerequisites.length; i++) {
			node = list.get(prerequisites[i][0]);
			for (int j = 1; j < prerequisites[i].length; j++) {
				node.inDegree++;
				list.get(prerequisites[i][j]).outList.add(node);
			}
		}

		// 找出全部入度为0的记录。
		LinkedList<Node> zeroInDegreeNode = new LinkedList<>();
		for (int i = 0; i < numCourses; i++) {
			if (list.get(i).inDegree == 0) {
				zeroInDegreeNode.add(list.get(i));
			}
		}
		// 移除入度为0的节点,相邻节点中全部入度减1，如果为0则加入遍历队列中
		int removeCount = 0;
		while (!zeroInDegreeNode.isEmpty()) {
			node = zeroInDegreeNode.poll();
			node.inDegree = -1;
			removeCount++;
			// 将入度为0的加入到zeroInDegreeNode中
			for (Node node2 : node.outList) {
				if (--node2.inDegree == 0) {
					zeroInDegreeNode.add(node2);
				}
			}
		}
		// 全部节点移除完了，即表示没有环，可以按顺序完成学业。
		return list.size() == removeCount;
	}

	public static void main(String[] args) {
		int numCourses = 4;
		int[][] prerequisites = new int[3][2];
		prerequisites[0][0] = 0;
		prerequisites[0][1] = 1;
		prerequisites[1][0] = 1;
		prerequisites[1][1] = 2;
		prerequisites[2][0] = 2;
		prerequisites[2][1] = 3;
//		prerequisites[3][0] = 0;
//		prerequisites[3][1] = 2;
		System.out.println(new Problem_207_CourseSchedule().canFinish(numCourses, prerequisites));
	}
}
