package graph;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 210. Course Schedule II Medium
 * 
 * There are a total of n courses you have to take, labeled from 0 to n-1.
 * 
 * Some courses may have prerequisites, for example to take course 0 you have to
 * first take course 1, which is expressed as a pair: [0,1]
 * 
 * Given the total number of courses and a list of prerequisite pairs, return
 * the ordering of courses you should take to finish all courses.
 * 
 * There may be multiple correct orders, you just need to return one of them. If
 * it is impossible to finish all courses, return an empty array.
 * 
 * Example 1:
 * 
 * Input: 2, [[1,0]] Output: [0,1] Explanation: There are a total of 2 courses
 * to take. To take course 1 you should have finished course 0. So the correct
 * course order is [0,1] .
 * 
 * Example 2:
 * 
 * Input: 4, [[1,0],[2,0],[3,1],[3,2]] Output: [0,1,2,3] or [0,2,1,3]
 * Explanation: There are a total of 4 courses to take. To take course 3 you
 * should have finished both courses 1 and 2. Both courses 1 and 2 should be
 * taken after you finished course 0. So one correct course order is [0,1,2,3].
 * Another correct ordering is [0,2,1,3] .
 * 
 * Note:
 * 
 * The input prerequisites is a graph represented by a list of edges, not
 * adjacency matrices. Read more about how a graph is represented. You may
 * assume that there are no duplicate edges in the input prerequisites.
 * 
 * Accepted 158,106 Submissions 445,413
 * 
 * @author harris
 * 
 * @see <a href=
 *      "https://leetcode.com/problems/course-schedule-ii/">course-scheduleII</a>
 *
 * 
 */
public class Problem_210_CourseSchedule2 {

	static class Node {
		public int val;
		public int inDegree;
		public List<Node> outList;

		public Node() {
		}

		public Node(int val, int inDegree, List<Node> outList) {
			super();
			this.val = val;
			this.inDegree = inDegree;
			this.outList = outList;
		}

	}

	/**
	 * 
	 * 判断有向图是否有环，有环表示不能完成，使用拓扑排序来判断,广度优先遍历BFS
	 * 
	 * 1. 记录入度数组
	 * 
	 * 2. 将入度为0的节点删除，关联的节点入度数减1，保存结果到int数组
	 * 
	 * 3. 如果全部节点都移除了，返回result
	 * 
	 * 4. 如果有节点不能移除，说明有环(相互依赖)，不能完成返回空数组
	 * 
	 * Runtime: 4 ms, faster than 80.84% of Java online submissions for Course
	 * Schedule II.
	 * 
	 * Memory Usage: 45.8 MB, less than 83.48% of Java online submissions for Course
	 * Schedule II.
	 * 
	 * @param numCourses    课程数
	 * @param prerequisites 课程依赖关系
	 * @return
	 */
	public int[] findOrder(int numCourses, int[][] prerequisites) {

		if (numCourses <= 0) {
			return new int[0];
		}
		// 构建图，临接表存储，记录入度数
		List<Node> list = new ArrayList<>(numCourses);
		for (int i = 0; i < numCourses; i++) {
			list.add(new Node(i, 0, new ArrayList<>()));
		}
		Node node = null;
		for (int i = 0; i < prerequisites.length; i++) {
			node = list.get(prerequisites[i][0]);
			for (int j = 1; j < prerequisites[i].length; j++) {
				node.inDegree++;
				list.get(prerequisites[i][j]).outList.add(node);
			}
		}

		// 找出全部入度为0的记录。
		LinkedList<Node> zeroInDegreeNode = new LinkedList<>();
		for (int i = 0; i < numCourses; i++) {
			if (list.get(i).inDegree == 0) {
				zeroInDegreeNode.add(list.get(i));
			}
		}
		int[] results = new int[numCourses];
		// 移除入度为0的节点,相邻节点中全部入度减1，如果为0则加入遍历队列中
		int removeCount = 0;
		while (!zeroInDegreeNode.isEmpty()) {
			node = zeroInDegreeNode.poll();
			node.inDegree = -1;
			results[removeCount++] = node.val;
			// 将入度为0的加入到zeroInDegreeNode中
			for (Node node2 : node.outList) {
				if (--node2.inDegree == 0) {
					zeroInDegreeNode.add(node2);
				}
			}
		}
		// 全部节点移除完了，即表示没有环，可以按顺序完成学业。
		return list.size() == removeCount ? results : new int[0];
	}

	public static void main(String[] args) {
		int numCourses = 4;
		int[][] prerequisites = new int[3][2];
		prerequisites[0][0] = 1;
		prerequisites[0][1] = 0;
		prerequisites[1][0] = 1;
		prerequisites[1][1] = 2;
		prerequisites[2][0] = 3;
		prerequisites[2][1] = 2;
//		prerequisites[3][0] = 0;
//		prerequisites[3][1] = 2;
		System.out.println(Arrays.toString(new Problem_210_CourseSchedule2().findOrder(numCourses, prerequisites)));
	}
}
