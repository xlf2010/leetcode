# leetcode

leetCode题目
 C语言检查地址是否溢出编译命令，加上gdb调试参数：
 ````shell
 gcc  -fno-omit-frame-pointer -fsanitize=address -o problem_42_TrappingRainWater.e -ggdb problem_42_TrappingRainWater.c
 ````
 运行时可能会报错，但报错没有打印出具体的行号，需根据地址反编译找到对应的代码
 比如一下错误：
 ````
 =================================================================
==8857== ERROR: AddressSanitizer: stack-buffer-underflow on address 0x7fff7f2724ec at pc 0x400c40 bp 0x7fff7f272460 sp 0x7fff7f272458
READ of size 4 at 0x7fff7f2724ec thread T0
    #0 0x400c3f (/home/harris/git/leetcode/array/problem_42_TrappingRainWater.e+0x400c3f)
    #1 0x400dff (/home/harris/git/leetcode/array/problem_42_TrappingRainWater.e+0x400dff)
    #2 0x7fcec9af4f44 (/lib/x86_64-linux-gnu/libc-2.19.so+0x21f44)
    #3 0x4007d8 (/home/harris/git/leetcode/array/problem_42_TrappingRainWater.e+0x4007d8)
Address 0x7fff7f2724ec is located at offset 28 in frame <main> of T0's stack:
  This frame has 1 object(s):
    [32, 44) 'num'
HINT: this may be a false positive if your program uses some custom stack unwind mechanism or swapcontext
      (longjmp and C++ exceptions *are* supported)
Shadow bytes around the buggy address:
  0x10006fe46440: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe46450: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe46460: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe46470: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe46480: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x10006fe46490: 00 00 00 00 00 00 00 00 00 00 f1 f1 f1[f1]00 04
  0x10006fe464a0: f4 f4 f3 f3 f3 f3 00 00 00 00 00 00 00 00 00 00
  0x10006fe464b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe464c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe464d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006fe464e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:     fa
  Heap righ redzone:     fb
  Freed Heap region:     fd
  Stack left redzone:    f1
  Stack mid redzone:     f2
  Stack right redzone:   f3
  Stack partial redzone: f4
  Stack after return:    f5
  Stack use after scope: f8
  Global redzone:        f9
  Global init order:     f6
  Poisoned by user:      f7
  ASan internal:         fe
==8857== ABORTING

 ````
 以上错误只有栈顶的相对地址偏移量0x400c3f
 此时可以使用以下命令：
````shell
objdump  -S problem_42_TrappingRainWater.e
#找到0x400c3f附近代码
		if(height[before_idx] <= height[after_idx] || i==heightSize-1){
  400bf4:	8b 45 ec             	mov    -0x14(%rbp),%eax
  400bf7:	48 98                	cltq   
  400bf9:	48 8d 14 85 00 00 00 	lea    0x0(,%rax,4),%rdx
  400c00:	00 
  400c01:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
  400c05:	48 8d 0c 02          	lea    (%rdx,%rax,1),%rcx
  400c09:	48 89 c8             	mov    %rcx,%rax
  400c0c:	48 89 c2             	mov    %rax,%rdx
  400c0f:	48 c1 ea 03          	shr    $0x3,%rdx
  400c13:	48 81 c2 00 80 ff 7f 	add    $0x7fff8000,%rdx
  400c1a:	0f b6 12             	movzbl (%rdx),%edx
  400c1d:	84 d2                	test   %dl,%dl
  400c1f:	40 0f 95 c7          	setne  %dil
  400c23:	48 89 c6             	mov    %rax,%rsi
  400c26:	83 e6 07             	and    $0x7,%esi
  400c29:	83 c6 03             	add    $0x3,%esi
  400c2c:	40 38 d6             	cmp    %dl,%sil
  400c2f:	0f 9d c2             	setge  %dl
  400c32:	21 fa                	and    %edi,%edx
  400c34:	84 d2                	test   %dl,%dl
  400c36:	74 08                	je     400c40 <trap+0x210>
  400c38:	48 89 c7             	mov    %rax,%rdi
  400c3b:	e8 f0 fa ff ff       	callq  400730 <__asan_report_load4@plt>
  400c40:	8b 31                	mov    (%rcx),%esi

#说明是在执行 callq  400730 <__asan_report_load4@plt> 报错，同时能能看出是在执行这个条件判断出现数组越界，最终定位到问题是before_idx初始值是-1导致数组访问越界报错， ‘if(height[before_idx] <= height[after_idx] || i==heightSize-1){’
````
 
 