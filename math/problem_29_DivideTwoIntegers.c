/*
29. Divide Two Integers		https://leetcode.com/problems/divide-two-integers/
Medium

Given two integers dividend and divisor, divide two integers 
without using multiplication, division and mod operator.

Return the quotient after dividing dividend by divisor.

The integer division should truncate toward zero.

Example 1:

Input: dividend = 10, divisor = 3
Output: 3

Example 2:

Input: dividend = 7, divisor = -3
Output: -2

Note:

    Both dividend and divisor will be 32-bit signed integers.
    The divisor will never be 0.
    Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 231 − 1 when the division result overflows.

Accepted
204,067
Submissions
1,263,064

*/
#include <stdio.h>
#include <stdlib.h>

//计算两个数除法，不能用乘除运算符来计算，用除数累加方式计算,累加的值超过被除数时，递归计算剩余部分
long int divi(long int dividend,long int divisor){
	if(dividend < divisor) return 0;
	long int sum=divisor,bef=divisor,result=1;
	while(1){
		bef=sum;
		sum+=sum;
		if(sum<dividend)
			result+=result;
		else
			break;
	}
	//超过被除数时，剩余部分当做被除数计算，
	result+=divi(dividend-(sum-bef),divisor);
	return result;
}

long int absx(int x){
	if(x==-2147483648) return 2147483648;
	return x<0?-x:x;
}

//Runtime: 4 ms, faster than 75.00% of C online submissions for Divide Two Integers.
//Memory Usage: 6.7 MB, less than 59.18% of C online submissions for Divide Two Integers.
int divide(int dividend, int divisor){
	//被除数是0或除数是1，返回被除数
	if(dividend == 0 || divisor == 1) return dividend;
	//除数是1，如果溢出的话返回int最大值，否则返回相反数
	if(divisor == -1){
		if(dividend <= -2147483648) return 0x7fffffff;
		else return -dividend;
	}
	
	int diff_sign = 1;
	long int div_1;
	long int div_2;
	
	if(dividend < 0){
		div_1 = absx(dividend);
		diff_sign = 0;
	}else{
		div_1 = dividend;
	}
	
	if(divisor < 0){
		div_2 = absx(divisor);
		diff_sign ^= 0;
	}else{
		div_2 = divisor;
		diff_sign ^= 1;
	}
	
	long int result = divi(div_1,div_2);
	if(result > 0x7fffffff){
		return 0x7fffffff;
	}
	return diff_sign ? -result : result;
}

int main(int argc,char** argv){
	int res = divide(-2147483648,-2147483648);
	printf("res=%d\n",res);
	return 0;
}
