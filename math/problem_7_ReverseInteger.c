/**
7. Reverse Integer  https://leetcode.com/problems/reverse-integer/
Easy

2118

3216

Favorite

Share
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. 
For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.

*/
#include <stdio.h>

//将一个整数翻转
//Runtime: 4 ms, faster than 98.95% of C online submissions for Reverse Integer.
//Memory Usage: 7 MB, less than 100.00% of C online submissions for Reverse Integer.
int reverse(int x){
	long res=0,i=x;
	int neg=0,max=0x7FFFFFFF;
	
	if(i<0){
		neg=1;
		i=-i;
	}
	
	while(i){
		res=res*10+(i%10);
		if(res>max) return 0;
		i/=10;
	}
	return neg?-res:res;
}

int main(int argc,char** argv){
	int i=1534236469;
	i=reverse(i);
	printf("i=%d\n",i);
	return 0;
}
