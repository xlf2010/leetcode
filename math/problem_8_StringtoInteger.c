/**

8. String to Integer (atoi)	https://leetcode.com/problems/string-to-integer-atoi/
Medium

918

5835

Favorite

Share
Implement atoi which converts a string to an integer.

The function first discards as many whitespace characters as necessary until the first non-whitespace character is found. 
Then, starting from this character, takes an optional initial plus or minus sign followed by as many numerical digits as possible, 
and interprets them as a numerical value.

The string can contain additional characters after those that form the integral number, 
which are ignored and have no effect on the behavior of this function.

If the first sequence of non-whitespace characters in str is not a valid integral number, 
or if no such sequence exists because either str is empty or it contains only whitespace characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned.

Note:

Only the space character ' ' is considered as whitespace character.
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−2^31,  2^31 − 1]. 
If the numerical value is out of the range of representable values, INT_MAX (2^31 − 1) or INT_MIN (−2^31) is returned.
Example 1:

Input: "42"
Output: 42
Example 2:

Input: "   -42"
Output: -42
Explanation: The first non-whitespace character is '-', which is the minus sign.
             Then take as many numerical digits as possible, which gets 42.
Example 3:

Input: "4193 with words"
Output: 4193
Explanation: Conversion stops at digit '3' as the next character is not a numerical digit.
Example 4:

Input: "words and 987"
Output: 0
Explanation: The first non-whitespace character is 'w', which is not a numerical 
             digit or a +/- sign. Therefore no valid conversion could be performed.
Example 5:

Input: "-91283472332"
Output: -2147483648
Explanation: The number "-91283472332" is out of the range of a 32-bit signed integer.
             Thefore INT_MIN (−2^31) is returned.


*/

#include <stdio.h>

int isNumber(char c){
	return c>='0' && c<='9';
}

/**
  Runtime: 0 ms, faster than 100.00% of C online submissions for String to Integer (atoi).
  Memory Usage: 7 MB, less than 100.00% of C online submissions for String to Integer (atoi).
*/
int myAtoi(char * str){
	char* s=str;
	//查找第一个不是空的字符
	while(s){
		if(*s==' ') {
			s++;
			continue;
		}
		if(*s!='+' && *s!='-' && !isNumber(*s)) return 0;
		break;
	}
	long res=0;
	unsigned int neg=0,max=0x80000000;
	
	//判断符号
	if(*s=='-') {
		neg=1;
		s++;
	}else if(*s=='+'){
		s++;
	}
	//将字符串转换成数字，不是数字或超出最大值，跳出循环
	while(s){
		if(!isNumber(*s)) break;
		res = res*10 + (*s-'0');
		if(res > max) {
			res=max;
			break;
		}
		s++;
	}
	//超出最大值范围,正数是0x7FFFFFFF
	if(!neg && res==max){
		res=max-1;
	}
	return neg ? -res : res;
}


int main(int argc,char** argv){
	char* str="91283472332234235";
	int i=myAtoi(str);
	printf("i=%d\n",i);
	return 0;
}
