/*
12. Integer to Roman	https://leetcode.com/problems/integer-to-roman/
Medium

Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000

For example, two is written as II in Roman numeral, just two one's added together. Twelve is written as, XII, which is simply X + II. The number twenty seven is written as XXVII, which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

    I can be placed before V (5) and X (10) to make 4 and 9. 
    X can be placed before L (50) and C (100) to make 40 and 90. 
    C can be placed before D (500) and M (1000) to make 400 and 900.

Given an integer, convert it to a roman numeral. Input is guaranteed to be within the range from 1 to 3999.

Example 1:

Input: 3
Output: "III"

Example 2:

Input: 4
Output: "IV"

Example 3:

Input: 9
Output: "IX"

Example 4:

Input: 58
Output: "LVIII"
Explanation: L = 50, V = 5, III = 3.

Example 5:

Input: 1994
Output: "MCMXCIV"
Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int add(char *ret,int c_start,int x,char five_c,char one_c,char next_c){
	int i;
	// 1<=x<=3
	if(x <= 3){
		for(i=0;i<x;i++){
			ret[c_start + i] = one_c;
		}
		return x;
	}
	
	//x==4
	if(x == 4){
		ret[c_start++] = one_c;
		ret[c_start] = five_c;
		return 2;
	}
	
	//x==5
	if(x == 5){
		ret[c_start] = five_c;
		return 1;
	}
	
	//6<=x<=8
	if(x >= 6 && x <= 8){
		ret[c_start++] = five_c;
		for(i=6;i<=x;i++){
			ret[c_start++] = one_c;
		}
		return x-4;
	}
	
	//x==9
	if(x==9){
		ret[c_start++] = one_c;
		ret[c_start] = next_c;
		return 2;
	}
	return 0;
}

//将数字或转换成罗马数字
//Runtime: 4 ms, faster than 89.00% of C online submissions for Integer to Roman.
//Memory Usage: 7.2 MB, less than 55.77% of C online submissions for Integer to Roman.
char * intToRoman(int num){
	int c_start=0;
	char *ret = (char *)malloc(sizeof(char) * 30);
	memset(ret,'\0',30);
	if(num>=1000){
		c_start += add(ret,c_start,num/1000,'\0','M','\0');
		num%=1000;
	}
	
	if(num>=100){
		c_start += add(ret,c_start,num/100,'D','C','M');
		num%=100;
	}
	
	if(num>=10){
		c_start += add(ret,c_start,num/10,'L','X','C');
		num%=10;
	}
	
	if(num){
		c_start += add(ret,c_start,num,'V','I','X');
	}

	return ret;
}

int main(int argc,char** argv){
	char *c = intToRoman(6);
	char *p=c;
	int count=0;
	while(*p){
		printf("%c",*p);
		p++;
		count++;
	}
	printf("\ncount=%d\n",count);
	free(c);
	return 0;
}
