/**
2. Add Two Numbers  https://leetcode.com/problems/add-two-numbers/
Medium

You are given two non-empty linked lists representing two non-negative integers. 
The digits are stored in reverse order and each of their nodes contain a single digit. 
Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.

*/

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
 
#include<stdio.h>
#include<stdlib.h>
 
struct ListNode {
    int val;
    struct ListNode *next;
};

int addTow(struct ListNode* p1, struct ListNode* p2,int* carry){
	int sum=p1->val+p2->val + *carry;
	*carry = sum >= 10 ? 1 : 0;
	if(*carry) {
		sum -= 10;
	}else{
		*carry = 0;
	}
	return sum;
}

int addOne(struct ListNode* p,int* carry){
	int sum=p->val + *carry;
	*carry = sum >= 10 ? 1 : 0;
	if(*carry) {
		sum -= 10;
	}else{
		*carry = 0;
	}
	return sum;
}
 
struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
	struct ListNode* p1=l1,*p2=l2,*curr,*next,*res=(struct ListNode*)malloc(sizeof(struct ListNode));
	res->val=0;
	res->next=NULL;
	if(!p1 && !p2) return res;
	if(!p1) return p2;
	if(!p2) return p1;
	curr = next = res;
	//进位标志
	int carry=0;
	//两边有值
    while(p1 && p2){
		next->val=addTow(p1,p2,&carry);
		p1=p1->next;
		p2=p2->next;
		curr=next;
		next=(struct ListNode*)malloc(sizeof(struct ListNode));
		next->next=NULL;
		curr->next=next;
	}
	//左边有值
	while(p1){
		next->val=addOne(p1,&carry);
		p1=p1->next;
		curr=next;
		next=(struct ListNode*)malloc(sizeof(struct ListNode));
		next->next=NULL;
		curr->next=next;
	}
	//右边有值
	while(p2){
		next->val=addOne(p2,&carry);
		p2=p2->next;
		curr=next;
		next=(struct ListNode*)malloc(sizeof(struct ListNode));
		next->next=NULL;
		curr->next=next;
	}
	// 有进位
	if(carry){
		next->val=carry;
	}else{
		curr->next=NULL;
		free(next);	
	}
	return res;
}

int main(int argc,char** argv){
	struct ListNode* l1 = (struct ListNode*)malloc(sizeof(struct ListNode) * 1);
	struct ListNode* l2 = (struct ListNode*)malloc(sizeof(struct ListNode) * 1);
	l1[0].val = 5;
	l1[0].next = NULL;
//	l1[1].val = 4;
//	l1[1].next = NULL;
//	l1[2].val = 3;
//	l1[2].next = NULL;
	
	l2[0].val = 5;
	l2[0].next = NULL;
//	l2[1].val = 6;
//	l2[1].next = l2+2;
//	l2[2].val = 4;
//	l2[2].next = NULL;
	
	struct ListNode* res = addTwoNumbers(l1,l2);
	struct ListNode* p = res;
	while(p){
		printf("%d->",p->val);
		p=p->next;
	}
	printf("\n");
	return 0;
}
