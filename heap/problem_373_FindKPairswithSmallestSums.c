/*

373. Find K Pairs with Smallest Sums	https://leetcode.com/problems/find-k-pairs-with-smallest-sums/
Medium

You are given two integer arrays nums1 and nums2 sorted in ascending order and an integer k.

Define a pair (u,v) which consists of one element from the first array and one element from the second array.

Find the k pairs (u1,v1),(u2,v2) ...(uk,vk) with the smallest sums.

Example 1:

Input: nums1 = [1,7,11], nums2 = [2,4,6], k = 3
Output: [[1,2],[1,4],[1,6]] 
Explanation: The first 3 pairs are returned from the sequence: 
             [1,2],[1,4],[1,6],[7,2],[7,4],[11,2],[7,6],[11,4],[11,6]

Example 2:

Input: nums1 = [1,1,2], nums2 = [1,2,3], k = 2
Output: [1,1],[1,1]
Explanation: The first 2 pairs are returned from the sequence: 
             [1,1],[1,1],[1,2],[2,1],[1,2],[2,2],[1,3],[1,3],[2,3]

Example 3:

Input: nums1 = [1,2], nums2 = [3], k = 3
Output: [1,3],[2,3]
Explanation: All possible pairs are returned from the sequence: [1,3],[2,3]


*/

#include <stdio.h>
#include <stdlib.h>

struct node_t{
	int num1,num2;
};


void swap(struct node_t* a,struct node_t* b){
	struct node_t tmp=*a;
	*a=*b;
	*b=tmp;
}

//堆已满，替换数组头元素，向下调整
void full_adjust(struct node_t *heap,int heapSize,struct node_t val){
	heap[0]=val;
	int curr=0,child;
	int half = heapSize >> 1;	//循环至少有一个左节点,非叶子节点
	while(curr < half){
		child=(curr << 1) + 1;
		//如果存在右节点，且左节点大于右节点，向右下调整
		if(child + 1 < heapSize && heap[child].num1+heap[child].num2 < heap[child+1].num1+heap[child+1].num2){
			child+=1;
		}
		//如果都比子节点小，则不需调整
		if(heap[curr].num1+heap[curr].num2 > heap[child].num1+heap[child].num2) break;
		swap(heap+curr,heap+child);
		curr=child;
	}
}

//堆数组没有满，插入数组尾，向上调整
void non_full_adjust(struct node_t *heap,int heapSize,struct node_t val){
	heap[heapSize] = val;
	int parent = (heapSize-1)>>1,curr=heapSize;
	//当前节点小于父节点，上升
	while(parent >= 0 && heap[curr].num1 + heap[curr].num2 > heap[parent].num1 + heap[parent].num2){
		swap(heap+curr,heap+parent);
		curr=parent;
		parent=(curr-1)>>1;
	}
}

//插入堆
void insert(struct node_t *heap,int *heapSize,int k,struct node_t val){
	if((*heapSize) < k){
		non_full_adjust(heap,*heapSize,val);
		(*heapSize)++;
	}else if(heap->num1+heap->num2 > val.num1+val.num2){
		full_adjust(heap,*heapSize,val);
	}
}

/**
 * Return an array of arrays of size *returnSize.
 * The sizes of the arrays are returned as *returnColumnSizes array.
 * Note: Both returned array and *columnSizes array must be malloced, assume caller calls free().
 */
 //两个递增有序数组，两个数组每个元素之和，查找前k个最小之和,建立大顶堆
 //Runtime: 24 ms, faster than 100.00% of C online submissions for Find K Pairs with Smallest Sums.
//Memory Usage: 9.3 MB, less than 100.00% of C online submissions for Find K Pairs with Smallest Sums.
int** kSmallestPairs(int* nums1, int nums1Size, int* nums2, int nums2Size, int k, int* returnSize, int** returnColumnSizes){
	int i,j,heapSize=0;
	struct node_t n,heap[k];
	for(i=0;i<nums1Size;i++){
		for(j=0;j<nums2Size;j++){
			n.num1=nums1[i];
			n.num2=nums2[j];
			insert(heap,&heapSize,k,n);
		}
	}
	if(k>heapSize) k = heapSize;
	*returnSize = k;
	int **ret;
	if(k<=0){
		ret = (int **)malloc(sizeof(int *));
		return ret;
	}
	*returnColumnSizes = (int *)malloc(sizeof(int) * k);
	ret = (int **)malloc(sizeof(int *) * k);
	int *res;
	for(i=0;i<k;i++){
		res = (int *)malloc(sizeof(int) * 2);
		res[0]=heap[i].num1;
		res[1]=heap[i].num2;
		ret[i]=res;
		(*returnColumnSizes)[i]=2;
	}
	return ret;
}

int main(int argc,char** argv){
	int nums1[]={1,1,2,3,4,5};
	int nums1Size=sizeof(nums1)/sizeof(*nums1);
	int nums2[]={1,5,6,7,8};
	int nums2Size=sizeof(nums2)/sizeof(*nums2);
	int i,j,k=2,returnSize;
	int *returnColumnSizes;
	int **ret=kSmallestPairs(nums1,nums1Size,nums2,nums2Size,k,&returnSize,&returnColumnSizes);
	for(i=0;i<returnSize;i++){
		for(j=0;j<returnColumnSizes[i];j++){
			printf("%d,",ret[i][j]);
		}
		printf("\n");
	}
	
	//free allocation
	free(returnColumnSizes);
	for(i=0;i<returnSize;i++){
		free(ret[i]);
	}
	free(ret);
	return 0;
}
