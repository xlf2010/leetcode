/*
347. Top K Frequent Elements	https://leetcode.com/problems/top-k-frequent-elements/
Medium

Given a non-empty array of integers, return the k most frequent elements.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]

Example 2:

Input: nums = [1], k = 1
Output: [1]

Note:

    You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
    Your algorithm's time complexity must be better than O(n log n), where n is the array's size.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct node_t{
	int val,count;
};

void swap(struct node_t* a,struct node_t* b){
	struct node_t tmp=*a;
	*a=*b;
	*b=tmp;
}

//堆已满，替换数组头元素，向下调整
void full_adjust(struct node_t *heap,int heapSize,struct node_t val){
	heap[0]=val;
	int curr=0,child;
	int half = heapSize >> 1;	//循环至少有一个左节点,非叶子节点
	while(curr < half){
		child=(curr << 1) + 1;
		//如果存在右节点，且左节点大于右节点，向右下调整
		if(child + 1 < heapSize && heap[child].count > heap[child+1].count){
			child+=1;
		}
		//如果都比子节点小，则不需调整
		if(heap[curr].count < heap[child].count) break;
		swap(heap+curr,heap+child);
		curr=child;
	}
}

//堆数组没有满，插入数组尾，向上调整
void non_full_adjust(struct node_t *heap,int heapSize,struct node_t val){
	heap[heapSize] = val;
	int parent = (heapSize-1)>>1,curr=heapSize;
	//当前节点小于父节点，上升
	while(parent >= 0 && heap[curr].count < heap[parent].count){
		swap(heap+curr,heap+parent);
		curr=parent;
		parent=(curr-1)>>1;
	}
}

int comp(const void* a,const void* b){
	return *(int *)a - *(int *)b;
}

//插入堆
void insert(struct node_t *heap,int *heapSize,int k,struct node_t val){
	if((*heapSize) < k){
		non_full_adjust(heap,*heapSize,val);
		(*heapSize)++;
	}else if(heap->count < val.count){
		full_adjust(heap,*heapSize,val);
	}
}


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
//Runtime: 16 ms, faster than 96.83% of C online submissions for Top K Frequent Elements.
//Memory Usage: 9.7 MB, less than 46.67% of C online submissions for Top K Frequent Elements.
//查找前K个出现最频繁的数字
int* topKFrequent(int* nums, int numsSize, int k, int* returnSize){
	if(numsSize<=1) {
		*returnSize=1;
		return nums;
	}
	*returnSize=0;
	struct node_t heap[numsSize],n;
	int i,heapSize=0,count=1;
	memset(heap,0,sizeof(heap));
	qsort(nums,numsSize,sizeof(int),comp);
	//统计相同的个数
	for(i=0;i<numsSize;i++){
		if(i+1<numsSize && nums[i] == nums[i+1]){
			count++;
		}else{
			n.val=nums[i];
			n.count=count;
			insert(heap,&heapSize,k,n);
			count=1;
		}
	}
	//构建结果
	if(k>heapSize) k=heapSize;
	int *res = (int *)malloc(sizeof(int) * k);
	for(i=0;i<k;i++){
		if(heap[i].count > 0){
			res[i]=heap[i].val;
			(*returnSize)++;
		}
	}
	return res;
}


int main(int argc,char** argv){
	int num[]={1,2};
	int len=sizeof(num)/sizeof(*num),k=2,i;
	int *res=topKFrequent(num,len,k,&len);
	for(i=0;i<len;i++){
		printf("%d,",res[i]);
	}
	printf("\n");
	free(res);
	return 0;
}

