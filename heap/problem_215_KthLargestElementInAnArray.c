/*
215. Kth Largest Element in an Array	https://leetcode.com/problems/kth-largest-element-in-an-array/

215. Kth Largest Element in an Array
Medium

Find the kth largest element in an unsorted array. Note that it is the kth largest element in the sorted order, not the kth distinct element.

Example 1:

Input: [3,2,1,5,6,4] and k = 2
Output: 5

Example 2:

Input: [3,2,3,1,2,4,5,5,6] and k = 4
Output: 4

Note:
You may assume k is always valid, 1 ≤ k ≤ array's length.


*/

#include <stdio.h>
#include <stdlib.h>

void swap(int* a,int* b){
	int tmp=*a;
	*a=*b;
	*b=tmp;
}

//堆已满，替换数组头元素，向下调整
void full_adjust(int *heap,int heapSize,int val){
	heap[0]=val;
	int curr=0,child;
	int half = heapSize >> 1;	//循环至少有一个左节点,非叶子节点
	while(curr < half){
		child=(curr << 1) + 1;
		//如果存在右节点，且左节点大于右节点，向右下调整
		if(child + 1 < heapSize && heap[child] > heap[child+1]){
			child+=1;
		}
		//如果都比子节点小，则不需调整
		if(heap[curr] < heap[child]) break;
		swap(heap+curr,heap+child);
		curr=child;
	}
}

//堆数组没有满，插入数组尾，向上调整
void non_full_adjust(int *heap,int heapSize,int val){
	heap[heapSize] = val;
	int parent = (heapSize-1)>>1,curr=heapSize;
	//当前节点小于父节点，上升
	while(parent >= 0 && heap[curr] < heap[parent]){
		swap(heap+curr,heap+parent);
		curr=parent;
		parent=(curr-1)>>1;
	}
}

void insertHeap(int* heap,int* heapSize,int k,int val){
	if((*heapSize) < k){
		non_full_adjust(heap,*heapSize,val);
		(*heapSize)++;
	}else if(*heap < val){
		full_adjust(heap,*heapSize,val);
	}
}

//查找第k大的数，建立一个k大小的最小堆，然后取最小元素即可
//Runtime: 8 ms, faster than 79.28% of C online submissions for Kth Largest Element in an Array.
//Memory Usage: 7.5 MB, less than 21.19% of C online submissions for Kth Largest Element in an Array.
int findKthLargest(int* nums, int numsSize, int k){
	int heap[k],i,heapSize=0;
	for(i=0;i<numsSize;i++){
		insertHeap(heap,&heapSize,k,nums[i]);
	}	
	//for(i=0;i<heapSize;i++){
		//printf("%d,",heap[i]);
	//}
	return heap[0];
}

int main(int argc,char** argv){
	int num[]={11,2,1,6,3,5,4,10,9};
	int len=sizeof(num)/sizeof(*num),k=3;
	printf("\n%d\n",findKthLargest(num,len,k));
	return 0;
}
