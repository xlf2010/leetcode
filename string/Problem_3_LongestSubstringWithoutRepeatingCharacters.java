package string;

/**
 3. Longest Substring Without Repeating Characters
 Medium

 6209

 354

 Favorite

 Share
 Given a string, find the length of the longest substring without repeating characters.

 Example 1:

 Input: "abcabcbb"
 Output: 3 
 Explanation: The answer is "abc", with the length of 3. 
 Example 2:

 Input: "bbbbb"
 Output: 1
 Explanation: The answer is "b", with the length of 1.
 Example 3:

 Input: "pwwkew"
 Output: 3
 Explanation: The answer is "wke", with the length of 3. 
 Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */
import java.util.*;

//获取字符串的最大字串,字串不包括重复的字符
//Runtime: 6 ms, faster than 84.05% of Java online submissions for Longest Substring Without Repeating Characters.
//Memory Usage: 41.3 MB, less than 5.20% of Java online submissions for Longest Substring Without Repeating Characters.
public class Problem_3_LongestSubstringWithoutRepeatingCharacters {
	public int lengthOfLongestSubstring(String s) {
		if (s == null || s.length() <= 0) {
			return 0;
		}
		// 保存字符对应的位置最大索引
		HashMap<Character, Integer> charMap = new HashMap<>();
		char[] cs = s.toCharArray();
		charMap.put(cs[0], 0);
		// 保存结果result
		// sstart_idx表示每次出现重复之后起始位置,用于计算出现重复字符时的长度,起始值为-1
		int result = 1, start_idx = 0;
		for (int i = 1; i < cs.length; i++) {
			// 字符曾经出现过,计算最大,起始位置为重复字符串的下一位
			if (charMap.containsKey(cs[i]) && charMap.get(cs[i]) >= start_idx) {
				if (result < i - start_idx)
					result = i - start_idx;
				start_idx = charMap.get(cs[i]) + 1;
			}
			// 最后一位且之前没有出现过的情况,需最后一位索引-起始位置+1
			if (i == cs.length - 1 && result < i - start_idx + 1) {
				result = i - start_idx + 1;
			}
			charMap.put(cs[i], i);
		}
		return result;
	}

	public static void main(String[] args) {
		String str = "pwwkew";
		int res = new Problem_3_LongestSubstringWithoutRepeatingCharacters()
				.lengthOfLongestSubstring(str);
		System.out.println(res);
	}
}