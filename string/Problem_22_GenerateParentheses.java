package string;

import java.util.ArrayList;
import java.util.List;

/**
 * 22. Generate Parentheses
 * Medium
 * 
 * 4115
 * 
 * 230
 * 
 * Add to List
 * 
 * Share
 * Given n pairs of parentheses, write a function to generate all combinations
 * of well-formed parentheses.
 * 
 * For example, given n = 3, a solution set is:
 * 
 * [
 * "((()))",
 * "(()())",
 * "(())()",
 * "()(())",
 * "()()()"
 * ]
 * Accepted
 * 470,082
 * Submissions
 * 786,607
 */
//Runtime: 0 ms, faster than 100.00% of Java online submissions for Generate Parentheses.
//Memory Usage: 38.6 MB, less than 31.61% of Java online submissions for Generate Parentheses.
public class Problem_22_GenerateParentheses {

	// 括号匹配,给定n个括号,返回合法的括号组合
	public List<String> generateParenthesis(int n) {
		List<String> retList = new ArrayList<>();
		if (n <= 0) {
			return retList;
		}
		generalRecurse(n, 0, 0, new StringBuilder(n << 1), retList);
		return retList;
	}

	private void generalRecurse(int n, int open, int close, StringBuilder str,
			List<String> retList) {
		// 括号匹配完,保存结果
		if (open == n && close == n) {
			retList.add(str.toString());
			return;
		}
		// 生成左括号,至多有n个左括号
		if (open < n) {
			str.append('(');
			generalRecurse(n, open + 1, close, str, retList);
			// 递归结束,在52行代码保存结果return后,回溯到上一个括号
			str.setLength(str.length() - 1);
		}
		// 基于左括号来生成右括号,右括号不能超过出现过的左括号才算合法
		if (close < open) {
			str.append(')');
			generalRecurse(n, open, close + 1, str, retList);
			// 递归结束,在52行代码保存结果return后,回溯到上一个括号
			str.setLength(str.length() - 1);
		}
	}

	public static void main(String[] args) {
		List<String> list = new Problem_22_GenerateParentheses()
				.generateParenthesis(3);
		for (String string : list) {
			System.out.println(string);
		}
	}
}