package string;

import java.util.ArrayList;
import java.util.List;

/**
 * 17. Letter Combinations of a Phone Number
 * Medium
 * 
 * 3161
 * 
 * 366
 * 
 * Add to List
 * 
 * Share
 * Given a string containing digits from 2-9 inclusive, return all possible
 * letter combinations that the number could represent.
 * 
 * A mapping of digit to letters (just like on the telephone buttons) is given
 * below. Note that 1 does not map to any letters.
 * 
 * <img src=
 * "http://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Telephone-keypad2.svg/200px-Telephone-keypad2.svg.png"
 * >
 * 
 * Example:
 * 
 * Input: "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
 * Note:
 * 
 * Although the above answer is in lexicographical order, your answer could be
 * in any order you want.
 * 
 * Accepted
 * 526,708
 * Submissions
 * 1,175,215
 */
// Runtime: 0 ms, faster than 100.00% of Java online submissions for Letter
//Combinations of a Phone Number.
// Memory Usage: 38.4 MB, less than 6.16% of Java online submissions for
//Letter Combinations of a Phone Number.
public class Problem_17_LetterCombinationsofaPhoneNumber {
	// 给一个数字字符串2~9,传统手机键盘字母(2:abc,3:def,4:ghi,5:jkl,6:mno,7:pqrs,8:tuv,9wxyz)
	// 返回所有字母的组合结果
	// 思路 :递归遍历,从最后一个数字开始,每个数字处理完后回退上一个数字
	public List<String> letterCombinations(String digits) {
		if (digits == null || digits.length() <= 0) {
			return new ArrayList<>();
		}
		String[] numbers = { "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv",
				"wxyz" };
		// 集合大小=n*n-1,按最大组合数计算
		int charLen = digits.length();
		List<String> result = new ArrayList<>(charLen * (charLen - 1));

		// 递归获取结果
		getResultRecursion(digits.toCharArray(), 0, new StringBuilder(),
				result, numbers);
		return result;
	}

	public void getResultRecursion(char[] cs, int currentIndex,
			StringBuilder preStr, List<String> result, String[] numbers) {
		if (currentIndex >= cs.length) {
			// 最后一个数字时候,保存结果
			result.add(preStr.toString());
			return;
		}
		// 遍历所有的数字
		String numDigit = numbers[cs[currentIndex] - '0' - 2];
		for (int i = 0; i < numDigit.length(); i++) {
			// 选定一个字母后,获取下一个数字
			// 保存当前选定的字母
			StringBuilder sb = new StringBuilder(preStr).append(numDigit
					.charAt(i));
			// 将digit删除第一个数字后递归下一个
			getResultRecursion(cs, currentIndex + 1, sb, result, numbers);
		}
	}

	public static void main(String[] args) {
		String digits = "23";
		List<String> list = new Problem_17_LetterCombinationsofaPhoneNumber()
				.letterCombinations(digits);
		for (String string : list) {
			System.out.println(string);
		}
	}
}