/*

63. Unique Paths II					https://leetcode.com/problems/unique-paths-ii/
Medium

A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

Now consider if some obstacles are added to the grids. How many unique paths would there be?

An obstacle and empty space is marked as 1 and 0 respectively in the grid.

Note: m and n will be at most 100.

Example 1:

Input:
[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
Output: 2
Explanation:
There is one obstacle in the middle of the 3x3 grid above.
There are two ways to reach the bottom-right corner:
1. Right -> Right -> Down -> Down
2. Down -> Down -> Right -> Right


一个m*n的矩阵块，从左上点出发，每次向右或向下走一步，如果是1,即障碍物,则置0,算出有多少种走法
动态规划法: 定义一个m*n矩阵,第一行于第一列为1,也就是第一行的每格只能往右走达到,第一列往下走得到
中间的每格都由上一行的值与左边的值相加得到
s[m][n]
// 初始化第一行
for i=1 to n; do 
	if obstacleGrid[0][i] == 0 then
		s[0][i] = 1;
	else s[0][i] = 0;
done
//初始化第一列
for i=1 to m; do 
	if obstacleGrid[m][0] == 0 then
		s[m][0] = 1;
	else s[m][0] = 0;
done
//从第二行第二列开始,如果是障碍物则置0,否则有s[i][j] = s[i-1][j] + s[i][j-1];
for i=2 to n; do
	for j=2 to m; do
		if obstacleGrid[0][i] == 0 then
			s[i][j] = s[i-1][j] + s[i][j-1];
		else s[i][j]=0
	done
done

return s[m][n];

Accepted
211,955
Submissions
631,029
*/

#include<stdio.h>
#include<stdlib.h>

//Runtime: 4 ms, faster than 83.93% of C online submissions for Unique Paths II.
//Memory Usage: 7.4 MB, less than 7.14% of C online submissions for Unique Paths II.
int uniquePathsWithObstacles_1(int** obstacleGrid, int obstacleGridSize, int* obstacleGridColSize){
	//第一格是障碍物,直接返回
	if(obstacleGridSize<=0 || obstacleGrid[0][0])return 0;
	
	int i,j;
	unsigned int s[obstacleGridSize][obstacleGridColSize[0]];
	s[0][0] = 1;
	// 初始化第一行,如果前一个格可达(值是1)且当前值是0,表示可达
	for(i=1;i<obstacleGridColSize[0];i++){
		s[0][i] = (s[0][i-1] && obstacleGrid[0][i] == 0) ? 1 : 0;
	}
	//初始化第一列
	for(i=1;i<obstacleGridSize;i++){
		s[i][0] = (s[i-1][0] && obstacleGrid[i][0] == 0) ? 1 : 0;
	}
	//从第二行第二列开始有s[i][j] = s[i-1][j] + s[i][j-1]
	for(i=1;i<obstacleGridSize;i++){
		for(j=1;j<obstacleGridColSize[0];j++){
			s[i][j] = obstacleGrid[i][j] ? 0 : s[i-1][j] + s[i][j-1];
		}
	}
	return s[obstacleGridSize-1][obstacleGridColSize[0]-1];
}

//空间优化,由于每个格子依赖上一个于左边,用一维数组来保存结果
//Runtime: 0 ms, faster than 100.00% of C online submissions for Unique Paths II.
//Memory Usage: 7.5 MB, less than 7.14% of C online submissions for Unique Paths II.
int uniquePathsWithObstacles(int** obstacleGrid, int obstacleGridSize, int* obstacleGridColSize){
	//第一格是障碍物,直接返回
	if(obstacleGridSize<=0 || obstacleGrid[0][0])return 0;
	unsigned int s[obstacleGridColSize[0]],i,j;
	s[0] = 1;
	// 初始化第一行,如果前一个格可达(值是1)且当前值是0,表示可达
	for(i=1;i<obstacleGridColSize[0];i++){
		s[i] = (s[i-1] && obstacleGrid[0][i] == 0) ? 1 : 0;
	}
	
	//从第二行开始有s[j] = s[j-1] + s[j]
	for(i=1;i<obstacleGridSize;i++){
		for(j=0;j<obstacleGridColSize[0];j++){
			//障碍物更新成0
			if(obstacleGrid[i][j]){
				s[j] = 0;
			//第一列不用动,保持初始化的值即可,要么1,要么0				
			}else if (j>0){
				s[j] += s[j-1];
			}
		}
	}
	return s[obstacleGridColSize[0] - 1];
}


int **trans(int arr[][3],int obstacleGridSize){
	int s = 3;
	int **res = (int **)malloc(sizeof(int *) * obstacleGridSize);
	int i,j,*tmp;
	for(i=0;i<obstacleGridSize;i++){
		tmp=(int *)malloc(sizeof(int) * s);
		for(j=0;j<s;j++){
			tmp[j] = arr[i][j];
		}
		res[i] = tmp;
	}
	return res;
}

int main(int argc,char** argv){
	int i,obstacleGridSize=3;
	int obstacleGrid[3][3] = {{0,0,0},{0,1,0},{0,0,0}};
	int obstacleGridColSize[3] = {3,3,3};
	int **tra = trans(obstacleGrid,obstacleGridSize);
	int res=uniquePathsWithObstacles(tra,obstacleGridSize,obstacleGridColSize);
	for(i=0;i<obstacleGridSize;i++){
		free(tra[i]);
	}
	free(tra);
	printf("res=%d\n",res);
	return 0;
}
