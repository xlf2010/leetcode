/*
198. House Robber https://leetcode.com/problems/house-robber/
Easy

2385

72

Favorite

Share
You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.

Example 1:

Input: [1,2,3,1]
Output: 4
Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
             Total amount you can rob = 1 + 3 = 4.
Example 2:

Input: [2,7,9,3,1]
Output: 12
Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
             Total amount you can rob = 2 + 9 + 1 = 12.
*/
#include<stdio.h>

//求数组最大和，其中计算和的元素不能相邻
int rob(int* nums, int numsSize){
	if(numsSize<=0) return 0;
	if(numsSize<=1) return nums[0];
	
	int i,i_2_max=nums[0],i_1_max=nums[1];
	int curr_max=i_2_max>i_1_max?i_2_max:i_1_max;
	int max=curr_max;
	for(i=2;i<numsSize;i++){
		//当前抢劫的值+前一个屋子的最大值，取当前值与上一个屋子的最大值
		curr_max=nums[i]+i_2_max;
		if(max<curr_max) max=curr_max;
		//截止到前一个屋子能取到的最大值
		i_2_max=i_2_max>i_1_max?i_2_max:i_1_max;
		//上一个屋子的最大值
		i_1_max=curr_max;
	}
	return max;
}


int main(int argc,char** argv){
	int num[]={1,2,3,1};
//	int num[]={2,7,9,3,1};
	int len=sizeof(num)/sizeof(*num);
	int res = rob(num,len);
	printf("res=%d",res);
	printf("\n");
	return 0;
}

