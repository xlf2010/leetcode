/*
 53. Maximum Subarray
 https://leetcode.com/problems/maximum-subarray/
 Easy
 Given an integer array nums, find the contiguous subarray (containing at least one number) 
 * which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.
Follow up:

If you have figured out the O(n) solution, 
* try coding another solution using the divide and conquer approach,
*  which is more subtle.
   
 */
 
#include<stdio.h>
 
 //参考Follow up，使用分块归并
int maxArray(int* nums,int start,int end){
	int min=-2147483647;
	 if(start>end) return min;
	 int mid=((end-start)>>1)+start;
	 //分块处理左右最大子序列
	 int lmax=maxArray(nums,start,mid-1);
	 int rmax=maxArray(nums,mid+1,end);
	 
	 //计算当前整个序列和
	 int i,sum=0,lf=0,rf=0;
	 for(i=mid-1;i>=start;i--){
		sum+=nums[i];
		if(sum>lf) lf=sum;
	 }
	 sum=0;
	 for(i=mid+1;i<=end;i++){
		sum+=nums[i];
		if(sum>rf) rf=sum;
	 }
	 
	 int res=lmax>rmax?lmax:rmax;
	 
	 if(res>lf+rf+nums[mid]) 
		return res;
	 return lf+rf+nums[mid];
}

 //参考Follow up，使用分块归并
int maxSubArray_1(int* nums, int numsSize) {
    if(numsSize<=0) return 0;
    int[3] res;
	return maxArray(nums,0,numsSize-1);
}

//动态规划
int maxSubArray(int* nums, int numsSize) {
    if(numsSize<=0) return 0;
    int i,m,l=0,r=0;
    //保存每个位置最大值,s[i]=max(s[i-1]+nums[i], nums[i]);
    int s[numsSize];
    m=s[0]=nums[0];
    for(i=1;i<numsSize;i++){
		//如果前面的值之和大于当前的值，取之前的值与当前值之和，位置右移
		if(s[i-1]+nums[i]>nums[i]){
			s[i] = s[i-1]+nums[i];
			if(nums[i]>0)r++;
		}else{
			s[i]=nums[i];
			//从当前位置开始计算
			l=r=i;
		}
		if(m<s[i]) m = s[i];
	}
	//输出子串在数组位置
//	printf("l=%d,r=%d\n",l,r);
	return m;
}

int main(int args,char** argv){
	int num[]={1};
	int i,returnSize,len=sizeof(num)/sizeof(*num),target=10;
	int res = maxSubArray_1(num,len);
	printf("res=%d",res);
	printf("\n");
}
