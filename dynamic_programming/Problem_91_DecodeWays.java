package dynamic_programming;


/**
 * 91. Decode Ways Medium
 * 
 * https://leetcode.com/problems/decode-ways/
 * 
 * A message containing letters from A-Z is being encoded to numbers using the
 * following mapping:
 * 
 * 'A' -> 1 'B' -> 2 ... 'Z' -> 26
 * 
 * Given a non-empty string containing only digits, determine the total number
 * of ways to decode it.
 * 
 * Example 1:
 * 
 * Input: "12" Output: 2 Explanation: It could be decoded as "AB" (1 2) or "L"
 * (12).
 * 
 * Example 2:
 * 
 * Input: "226" Output: 3 Explanation: It could be decoded as "BZ" (2 26), "VF"
 * (22 6), or "BBF" (2 2 6).
 * 
 * Accepted 275,211 Submissions 1,218,136
 *
 */
public class Problem_91_DecodeWays {

	/**
	 * 将数字与字母建立映射关系
	 * 
	 * 'A' -> 1 'B' -> 2 ... 'Z' -> 26
	 * 
	 * 给定一串数字,问有多少种组合方式,如12 可以是A(1)B(2)或L(12)
	 * 
	 * Runtime: 1 ms, faster than 98.68% of Java online submissions for Decode Ways.
	 * Memory Usage: 34.3 MB, less than 100.00% of Java online submissions for
	 * Decode Ways.
	 * 
	 * @param s
	 * @return
	 */
	public int numDecodings(String s) {
		if (s == null || s.length() <= 0) {
			return 0;
		}
		char[] cs = s.toCharArray();

		// 默认一种情况，即每个数字都是单独的组合
		int res = 1;
		// 1或2会存在多种情况,记录1或2开始的位置
		int oneTwoStart = -1;
		for (int i = 0; i < cs.length; i++) {
			// 每一段如果是0开始的话，则不能解析
			if (oneTwoStart == -1 && cs[i] == '0') {
				return 0;
			}

			// 1或2开始位置
			if ((cs[i] == '1' || cs[i] == '2') && oneTwoStart < 0) {
				oneTwoStart = i;
			}
			// 1或2 结束位置，有起始位置，或当前值不是1或2，或已达到终点
			if (oneTwoStart >= 0 && (cs[i] == '0' || cs[i] > '2' || i == cs.length - 1)) {

				// 当前的位置的值是0，只能与前一位解析成10或20，不算在组合里面
				if (cs[i] == '0') {
					res *= count(cs, oneTwoStart, i - 2);
				} else {
					res *= count(cs, oneTwoStart, i);
				}
				oneTwoStart = -1;
			}
		}

		// 没有出现1或2，组合数是1
		return res == 0 ? 1 : res;
	}

	// 两两组合
	private int count(char[] cs, int start, int end) {

		// 倒数第二个是2,且最后一个数字是0或大于7，只有一种组合，end往前移
		if (end > 0 && cs[end - 1] == '2' && cs[end] > '6') {
			end--;
		}

		return fibonacci(cs, start, end);
	}

	/**
	 * 本质上是一个fibonacci数列,
	 * 只有1个字符是,组合数是1
	 * 2个字符时,组合数是2
	 * 
	 * 从第三个字符开始,都可以表示成 f(n) = f(n-1)+f(n-2)
	 * 即将字符拆成两部分, 一部分是1个数字与剩下的n-1,或前两个组成十位数与n-2
	 * 
	 * @param cs    字符串
	 * @param start 1或2开始位置
	 * @param end   非1或2结束位置
	 * @return 组合数
	 */
	private int fibonacci(char[] cs, int start, int end) {
		if (start >= end) {
			return 1;
		}
		if (end - start == 1) {
			return 2;
		}
		int n1 = 1, n2 = 2, n3 = 3;
		for (int i = start + 2; i <= end; i++) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
		}
		return n3;
	}

	public static void main(String[] args) {
		Problem_91_DecodeWays decodeWays = new Problem_91_DecodeWays();

		String txt = "11123";
		System.out.println("src: " + txt + " dec :" + decodeWays.numDecodings(txt));
//		txt = "100";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
//		txt = "1";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
//		txt = "01";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
//		txt = "10";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
//		txt = "101";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
//		txt = "999999444";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
		// 589824
		txt = "4757562545844617494555774581341211511296816786586787755257741178599337186486723247528324612117156948";
		System.out.println("src: " + txt + " dec :" + decodeWays.numDecodings(txt));
//		txt = "25";
//		System.out.println("src: " + txt + " dec :"
//				+ decodeWays.numDecodings(txt));
	}
}
