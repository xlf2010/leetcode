package dynamic_programming;

/**
 * 64. Minimum Path Sum Medium
 * 
 * https://leetcode.com/problems/minimum-path-sum/
 * 
 * Given a m x n grid filled with non-negative numbers, find a path from top
 * left to bottom right which minimizes the sum of all numbers along its path.
 * 
 * Note: You can only move either down or right at any point in time.
 * 
 * Example:
 * 
 * Input: [ [1,3,1], [1,5,1], [4,2,1] ] Output: 7 Explanation: Because the path
 * 1→3→1→1→1 minimizes the sum.
 * 
 * Accepted 245,096 Submissions 513,111
 *
 */
public class Problem_64_MinimumPathSum {

	/**
	 * 给定一个m*n非负矩阵
	 * 
	 * 从左上角移动到右下角,每次移动一个位置,只能是向右或者向下
	 * 
	 * 求所有路径中和最小值
	 * 
	 * 
	 * Runtime: 2 ms, faster than 89.96% of Java online submissions for Minimum
	 * Path
	 * Sum.
	 * 
	 * Memory Usage: 42.2 MB, less than 82.36% of Java online submissions for
	 * Minimum Path Sum.
	 * 
	 * @param grid
	 * @return
	 */
	public int minPathSum(int[][] grid) {
		if (grid == null) {
			return 0;
		}
		// 1. 初始化第一行,只能向右移动得到,因此值是唯一的
		for (int i = 1; i < grid[0].length; i++) {
			grid[0][i] += grid[0][i - 1];
		}
		// 2. 初始化第一列,只能向下移动,值是唯一的
		for (int i = 1; i < grid.length; i++) {
			grid[i][0] += grid[i - 1][0];
		}
		// 3.第二行第二列开始,保存两个方向来源(上往下,左往右)中较小的值
		// 由于是非负的,当前较小值得到的结果也是最小的(贪心)
		for (int i = 1; i < grid.length; i++) {
			for (int j = 1; j < grid[i].length; j++) {
				if (grid[i - 1][j] < grid[i][j - 1]) {
					grid[i][j] += grid[i - 1][j];
				} else {
					grid[i][j] += grid[i][j - 1];
				}
			}
		}
		return grid[grid.length - 1][grid[0].length - 1];
	}

	public static void main(String[] args) {
		int[][] grid = { { 1, 2, 5 }, { 3, 2, 1 } };
		System.out.println(new Problem_64_MinimumPathSum().minPathSum(grid));
	}
}
