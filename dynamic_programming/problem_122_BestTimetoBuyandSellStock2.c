/*
122. Best Time to Buy and Sell Stock II https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
Easy

931

1250

Favorite

Share
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like (i.e., buy one and sell one share of the stock multiple times).

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Example 1:

Input: [7,1,5,3,6,4]
Output: 7
Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
             Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
Example 2:

Input: [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
             Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
             engaging multiple transactions at the same time. You must sell before buying again.
Example 3:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
*/
#include <stdio.h>

int maxProfit(int* prices, int pricesSize){
	if(pricesSize<2) return 0;
	unsigned int min=prices[0];
	int i,max=0,fin_max=0;
	//线性扫描，如果下一天小于前一天的金额
	for (i=1;i<pricesSize;i++){
		if(prices[i]<min){
			min=prices[i];
		}else if(prices[i]-min>max){
			max=prices[i]-min;
		}
		//判断是否下一买入点，记录当前利润，并重置max，min值寻找下一买入卖出点
		if(prices[i]<prices[i-1] || i==pricesSize-1){
			fin_max+=max;
			max=0;
			min=prices[i];
		}
	}
	return fin_max;
}

int main(int argc,char** argv){
//	int num[]={1,2,3,4,5};
	int num[]={7,1,5,3,6,4};
	int len=sizeof(num)/sizeof(*num);
	int res = maxProfit(num,len);
	printf("res=%d",res);
	printf("\n");
	return 0;
}