/*
121. Best Time to Buy and Sell Stock
https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
Easy

2455

117

Favorite

Share
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.

Note that you cannot sell a stock before you buy one.

Example 1:

Input: [7,1,5,3,6,4]
Output: 5
Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
             Not 7-1 = 6, as selling price needs to be larger than buying price.
Example 2:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.

*/

#include<stdio.h>


int maxProfit(int* prices, int pricesSize){
	unsigned int min=1<<31-1;
	int i,max=0;
	for (i=0;i<pricesSize;i++){
		if(prices[i]<min){
			min=prices[i];
		}else if(prices[i]-min>max){
			max=prices[i]-min;
		}
	}
	return max;
}

int maxProfit_2(int* prices, int pricesSize){
	if(pricesSize <=0) return 0;
	int i,start_pos=-1;
	//后一天价值要大于前一天的，才能卖出获利
	for(i=0;i<pricesSize-1;i++){
		if(prices[i+1] > prices[i]){
			start_pos=i;
			break;
		}
	}
	//找不到买入位置，即数组为降序排列，卖出都是亏的，返回0
	if(start_pos<0) return 0;
	
	int max=0,curr_max,before_max=0;	//保存最大值，当前最大值，上一个最大值
	i=start_pos+1;
	for(i;i<pricesSize;i++){
		curr_max = prices[i]-prices[i-1]+before_max;
		if(max<curr_max) {
			max=curr_max;
		}
		//curr_max小于0，说明是个更低点买入
		before_max=curr_max>0?curr_max:0;
	}
	return max;
}



int maxProfit_1(int* prices, int pricesSize){
	if(pricesSize <=0) return 0;
	int i,start_pos=-1;
	//后一天价值要大于前一天的，才能卖出获利s
	for(i=0;i<pricesSize-1;i++){
		if(prices[i+1] > prices[i]){
			start_pos=i;
			break;
		}
	}
	//找不到买入位置，即数组为降序排列，卖出都是亏的，返回0
	if(start_pos<0) return 0;
	
	int res[pricesSize];
	int max=0,curr_max=0,before_max;	//保存最大值，当前最大值，上一个最大值
	int curr_start_pos=start_pos;		//当前开始位置
	int end_pos;			//结束位置
	res[start_pos]=0;
	i=start_pos+1;
	for(i;i<pricesSize;i++){
		res[i] = prices[i]-prices[i-1]+res[i-1];
		if(res[i]<0){
			curr_start_pos=i;
			res[i]=0;
		}
		if(max<res[i]) {
			max=res[i];
			end_pos = i;
		}
	}
//	printf("start=%d,end=%d\n",curr_start_pos,end_pos);
	return max;
}

int main(int argc,char** argv){
//	int num[]={1,2};
	int num[]={7,1,5,3,6,4};
	int len=sizeof(num)/sizeof(*num);
	int res = maxProfit(num,len);
	printf("res=%d",res);
	printf("\n");
	return 0;
}
