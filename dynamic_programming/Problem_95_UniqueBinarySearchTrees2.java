package dynamic_programming;


import java.util.ArrayList;
import java.util.List;

//95. Unique Binary Search Trees II
//Medium

//https://leetcode.com/problems/unique-binary-search-trees-ii/
//
//Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1 ... n.
//
//Example:
//
//Input: 3
//Output:
//[
//  [1,null,3,2],
//  [3,2,null,1],
//  [3,1,null,null,2],
//  [2,1,3],
//  [1,null,2,null,3]
//]
//Explanation:
//The above output corresponds to the 5 unique BST's shown below:
//
//   1         3     3      2      1
//    \       /     /      / \      \
//     3     2     1      1   3      2
//    /     /       \                 \
//   2     1         2                 3
//
//Accepted
//146,044
//Submissions
//401,334

public class Problem_95_UniqueBinarySearchTrees2 {
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	private TreeNode copyTreeNode(TreeNode treeNode) {

		return treeNode;
	}

	public List<TreeNode> general(int start, int end) {
		ArrayList<TreeNode> retList = new ArrayList<>();
		if (start >= end) {
			return retList;
		}
		retList.addAll(general(start + 1, end));
		return retList;
	}

	public List<TreeNode> generateTrees(int n) {
		if (n <= 0) {
			return new ArrayList<>();
		}
		ArrayList<TreeNode> retList = new ArrayList<>();
		for (int i = 0; i < n; i++) {

		}
		return retList;
	}

	public static void main(String[] args) {

	}
}
