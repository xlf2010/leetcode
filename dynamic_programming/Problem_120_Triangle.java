package dynamic_programming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 120. Triangle Medium
 * 
 * https://leetcode.com/problems/triangle/
 * 
 * 1213
 * 
 * 134
 * 
 * Favorite
 * 
 * Share Given a triangle, find the minimum path sum from top to bottom. Each
 * step you may move to adjacent numbers on the row below.
 * 
 * For example, given the following triangle
 * 
 * [ [2], [3,4], [6,5,7], [4,1,8,3] ] The minimum path sum from top to bottom is
 * 11 (i.e., 2 + 3 + 5 + 1 = 11).
 * 
 * Note:
 * 
 * Bonus point if you are able to do this using only O(n) extra space, where n
 * is the total number of rows in the triangle.
 * 
 * Accepted 190,290 Submissions 475,119
 */
public class Problem_120_Triangle {

	/**
	 * 递归计算路径之和
	 * 
	 * @param triangle
	 * @param sum
	 * @param currPos
	 * @param currRow
	 * @param res
	 */
	private void recursion(List<List<Integer>> triangle, int sum, int currPos,
			int currRow, int[] res) {
		// 超过行数
		if (currRow >= triangle.size()) {
			// 取最小的和
			if (res[currPos] > sum) {
				res[currPos] = sum;
			}
			return;
		}
		// 当前节点的和
		sum += triangle.get(currRow).get(currPos);
		// 计算当前节点的左节点
		recursion(triangle, sum, currPos, currRow + 1, res);
		// 计算当前节点的右节点
		recursion(triangle, sum, currPos + 1, currRow + 1, res);
	}

	/**
	 * 给定一个三角形数集，求从顶点到底边路径中和最小，空间复杂度不会超过三角形的行数
	 * 
	 * 思路：这三角形长得像递归树，考虑用递归实现，自顶向下
	 * 
	 * 时间复杂度为2^n，每条路径都算了一遍,毫无意外TLE了。
	 * 
	 * Time Limit Exceeded
	 * 
	 * @param triangle
	 * @return
	 */
	public int minimumTotal_1(List<List<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}
		// 保存结果集
		int[] res = new int[triangle.get(triangle.size() - 1).size() + 1];
		for (int i = 0; i < res.length; i++) {
			res[i] = Integer.MAX_VALUE;
		}
		recursion(triangle, 0, 0, 0, res);
		int min = res[0];
		for (int i = 1; i < res.length; i++) {
			if (min > res[i]) {
				min = res[i];
			}
		}
		return min;
	}

	/**
	 * 也是自顶向下，从第3行开始
	 * 
	 * 除了两边的元素来源只有上一行的第一个或最后一个，中间的元素来源可能有两个
	 * 
	 * 只需取两个来源中较小的一个即可，建立二维数组保存其中较小的结果
	 * 
	 * 路径中每个节点都取最小的结果，得到的和一定是最小的，可以用反证法证明
	 * 
	 * 每一行中，对应行位置j
	 * 头元素直接与上一行的第一个元素相加即可
	 * if j==0
	 * triangle[i][0] += triangle[i-1][0];
	 * 最后一个元素直接与上一行最后一个元素相加即可
	 * if j==columnSize
	 * triangle[i][columnSize] += triangle[i-1][columnSize-1];
	 * 中间的元素，保存两个来源(左上与右上)中较小的值
	 * else
	 * triangle[i][j] += min(triangle[i-1][j-1],triangle[i-1][j]);
	 * 
	 * 时间复杂度为O(n)，n为三角数的个数，没有而外空间
	 * 
	 * Runtime: 2 ms, faster than 84.94% of Java online submissions for
	 * Triangle.
	 * 
	 * 
	 * Memory Usage: 37 MB, less than 99.13% of Java online submissions for
	 * Triangle.
	 * 
	 * @param triangle
	 * @return
	 */
	public int minimumTotal_2(List<List<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}

		int columnSize = 0, sum;
		for (int i = 1; i < triangle.size(); i++) {
			columnSize = triangle.get(i).size();
			for (int j = 0; j < columnSize; j++) {
				// 中间的元素，保存两个来源(左上与右上)中较小的值
				if (j > 0 && j < columnSize - 1) {
					setValBetween(triangle, i, j);
				} else if (j == 0) {
					// 头元素直接与上一行的第一个元素相加即可
					sum = triangle.get(i - 1).get(j) + triangle.get(i).get(j);
					triangle.get(i).set(j, sum);
				} else {
					// 最后一个元素直接与上一行最后一个元素相加即可
					sum = triangle.get(i - 1).get(j - 1)
							+ triangle.get(i).get(j);
					triangle.get(i).set(j, sum);
				}
			}
		}
		int min = Integer.MAX_VALUE;
		for (Integer val : triangle.get(triangle.size() - 1)) {
			if (min > val) {
				min = val;
			}
		}
		return min;
	}

	/**
	 * 设置每行除去头尾的值
	 * 
	 * @param triangle
	 * @param i
	 * @param j
	 */
	private void setValBetween(List<List<Integer>> triangle, int i, int j) {
		// 左上角
		int upLeft = triangle.get(i - 1).get(j - 1);
		// 右上角
		int upRight = triangle.get(i - 1).get(j);
		if (upLeft < upRight) {
			triangle.get(i).set(j, upLeft + triangle.get(i).get(j));
		} else {
			triangle.get(i).set(j, upRight + triangle.get(i).get(j));
		}
	}

	/**
	 * 自底向上动态规划,从最后一行开始向上计算
	 * 
	 * 从倒数第二行开始有，每一行之和为下一行和较小的值
	 * 
	 * triangle[i][j] += min(triangle[i+1][j],triangle[i+1][j+1])
	 * 
	 * 
	 * Runtime: 4 ms, faster than 28.00% of Java online submissions for
	 * Triangle.
	 * 
	 * 
	 * Memory Usage: 37.1 MB, less than 98.98% of Java online submissions for
	 * Triangle.
	 * Next challenges:
	 * 
	 * @param triangle
	 * @return
	 */
	public int minimumTotal(List<List<Integer>> triangle) {

		if (triangle == null || triangle.size() <= 0) {
			return 0;
		}

		int columnSize = 0, min;
		for (int i = triangle.size() - 2; i >= 0; i--) {
			columnSize = triangle.get(i).size();
			for (int j = 0; j < columnSize; j++) {
				min = Math.min(triangle.get(i + 1).get(j), triangle.get(i + 1)
						.get(j + 1));
				triangle.get(i).set(j, min + triangle.get(i).get(j));
			}
		}
		return triangle.get(0).get(0);
	}

	public static void main(String[] args) {
		ArrayList<List<Integer>> list = new ArrayList<>();
		list.add(Arrays.asList(2));
		list.add(Arrays.asList(3, 4));
		list.add(Arrays.asList(6, 5, 7));
		list.add(Arrays.asList(4, 1, 8, 3));
		System.out.println(new Problem_120_Triangle().minimumTotal(list));
	}
}
