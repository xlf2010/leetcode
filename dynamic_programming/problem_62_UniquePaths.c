/*

62. Unique Paths		https://leetcode.com/problems/unique-paths/
Medium

A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

How many possible unique paths are there?


Above is a 7 x 3 grid. How many possible unique paths are there?

Note: m and n will be at most 100.

Example 1:

Input: m = 3, n = 2
Output: 3
Explanation:
From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Right -> Down
2. Right -> Down -> Right
3. Down -> Right -> Right
Example 2:

Input: m = 7, n = 3
Output: 28

一个m*n的矩阵块，从左上点出发，每次向右或向下走一步，算出有多少种走法
动态规划法: 定义一个m*n矩阵,第一行于第一列为1,也就是第一行的每格只能往右走达到,第一列往下走得到
中间的每格都由上一行的值与左边的值相加得到
s[m][n]
// 初始化第一行
for i=1 to n; do 
	s[0][i] = 1;
done
//初始化第一列
for i=1 to m; do 
	s[m][0] = 1;
done
//从第二行第二列开始
for i=2 to n; do
	for j=2 to m; do
		s[i][j] = s[i-1][j] + s[i][j-1];
	done
done

return s[m][n];
*/

#include<stdio.h>
#include<stdlib.h>

//Runtime: 4 ms, faster than 57.35% of C online submissions for Unique Paths.
//Memory Usage: 6.6 MB, less than 80.43% of C online submissions for Unique Paths.
int uniquePaths_1(int m, int n){
	if(m<=0||n<=0)return 0;
	//只有一行或一列,就一种走法
	if(m==1||n==1) return 1;
	int i,j,s[m][n];
	// 初始化第一行
	for(i=0;i<n;i++){
		s[0][i] = 1;
	}
	//初始化第一列
	for(i=0;i<m;i++){
		s[i][0] = 1;
	}
	//从第二行第二列开始有s[i][j] = s[i-1][j] + s[i][j-1]
	for(i=1;i<m;i++){
		for(j=1;j<n;j++){
			s[i][j] = s[i-1][j] + s[i][j-1];
		}
	}
	return s[m-1][n-1];
}

//空间优化,由于每个格子依赖上一个于左边,用一维数组来保存结果
//Runtime: 0 ms, faster than 100.00% of C online submissions for Unique Paths.
//Memory Usage: 6.6 MB, less than 73.17% of C online submissions for Unique Paths.
int uniquePaths(int m, int n){
	if(m<=0||n<=0)return 0;
	//只有一行或一列,就一种走法
	if(m==1||n==1) return 1;
	int i,j,s[n];
	// 初始化第一行
	for(i=0;i<n;i++){
		s[i] = 1;
	}

	//从第二行开始有s[i] = s[i] + s[i-1]
	for(i=1;i<m;i++){
		for(j=1;j<n;j++){
			s[j] += s[j-1];
		}
	}
	return s[n-1];
}

int main(int argc,char** argv){
	int m=3,n=2;
	int res=uniquePaths(m,n);
	printf("res=%d\n",res);
	return 0;
}
