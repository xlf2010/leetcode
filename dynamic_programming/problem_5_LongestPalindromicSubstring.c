/*
5. Longest Palindromic Substring  https://leetcode.com/problems/longest-palindromic-substring/

Medium

3443

335

Favorite

Share
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>


/*
 * 分两种情况讨论，
 * 1.中间没有重复,如 aabaa
 * 2.中间有除重复,如 aabbaa
 */
char* longestPalindrome(char * s){
	int before_max=0,i,l=0,r=1,idx_l=0,idx_r=0,len=strlen(s);
	for(i=0;i<len;i++){
		//处理情况1：以i为中心往两边查找，看是否相同
		l=i-1;
		r=i+1;
		while(l>=0 && r<len && s[l]==s[r]){
			l--;
			r++;
		}
		if(before_max<r-l-2) {
			before_max=r-l-2;
			idx_l=l+1;
			idx_r=r-1;
		}
		//处理情况2：以i与i+1为中心往两边查找，看是否相同
		l=i;
		r=i+1;
		while(l>=0 && r<len && s[l]==s[r]){
			l--;
			r++;
		}
		if(before_max<r-l-2) {
			before_max=r-l-2;
			idx_l=l+1;
			idx_r=r-1;
		}
	}
	
	char* c = (char *) malloc (sizeof(char) * (idx_r-idx_l+2));
	for(i=idx_l;i<=idx_r;i++){
		c[i-idx_l]=s[i];
	}
	c[i-idx_l]='\0';
	return c;
}

int main(int argc,char** argv){
	char* s="ccc";
	char* res=longestPalindrome(s);
	printf("%s\n",res);
	return 0;
}


