/*
123. Best Time to Buy and Sell Stock III	https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/
Hard

988

53

Favorite

Share
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.

Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock before you buy again).

Example 1:

Input: [3,3,5,0,0,3,1,4]
Output: 6
Explanation: Buy on day 4 (price = 0) and sell on day 6 (price = 3), profit = 3-0 = 3.
             Then buy on day 7 (price = 1) and sell on day 8 (price = 4), profit = 4-1 = 3.
Example 2:

Input: [1,2,3,4,5]
Output: 4
Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
             Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
             engaging multiple transactions at the same time. You must sell before buying again.
Example 3:

Input: [7,6,4,3,1]
Output: 0
Explanation: In this case, no transaction is done, i.e. max profit = 0.
*/
#include <stdio.h>

//与前一个问题122不同的是，这里最多只能卖两次
int maxProfit_1(int* prices, int pricesSize){
	if(pricesSize<2) return 0;
	unsigned int min=prices[0];
	int i,max=0,fin_max_1=0,fin_max_2=0;	//记录最大两次卖出的价格

	//线性扫描，如果下一天小于前一天的金额
	for (i=1;i<pricesSize;i++){
		if(prices[i]<min){
			min=prices[i];
		}else if(prices[i]-min>max){
			max=prices[i]-min;
		}
		//判断是否下一买入点，更新，并重置max，min值寻找下一买入卖出点
		if(prices[i]<prices[i-1] || i==pricesSize-1){
			if(fin_max_1<fin_max_2){
				fin_max_1=max>fin_max_1?max:fin_max_1;
			}else{
				fin_max_2=max>fin_max_2?max:fin_max_2;			
			}			
			max=0;
			min=prices[i];
		}
	}
	return fin_max_1+fin_max_2;
}


int maxProfit(int* prices, int pricesSize){
	if(pricesSize <=0) return 0;
	int i,start_pos=-1;
	//后一天价值要大于前一天的，才能卖出获利
	for(i=0;i<pricesSize-1;i++){
		if(prices[i+1] > prices[i]){
			start_pos=i;
			break;
		}
	}
	//找不到买入位置，即数组为降序排列，卖出都是亏的，返回0
	if(start_pos<0) return 0;
	
	int curr_max,before_max=0,fin_max_1=0,fin_max_2=0;	//保存最大值，当前最大值，上一个最大值,最终两个大小的值
	i=start_pos+1;
	for(i;i<pricesSize;i++){
		curr_max = prices[i]-prices[i-1]+before_max;
		//curr_max小于0，说明是个更低点买入,记录当前最大值
		if(curr_max<0||i==pricesSize-1){
			//覆盖fin_max_1，fin_max_2中比较小的那个值
			if(fin_max_1<fin_max_2){
				fin_max_1=curr_max>fin_max_1?curr_max:fin_max_1;
			}else{
				fin_max_2=curr_max>fin_max_2?curr_max:fin_max_2;			
			}
			//重新统计最大值
			before_max=0;
		}else{
			before_max=curr_max;
		}
	}
	return fin_max_1+fin_max_2;
}


int main(int argc,char** argv){
	int num[]={1,2,4,2,5,7,2,4,9,0};
//	int num[]={3,3,7,0,0,3,1,3};
	int len=sizeof(num)/sizeof(*num);
	int res = maxProfit(num,len);
	printf("res=%d",res);
	printf("\n");
	return 0;
}