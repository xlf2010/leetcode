/*
70. Climbing Stairs
https://leetcode.com/problems/climbing-stairs/
Easy

2034

76

Favorite

Share
You are climbing a stair case. It takes n steps to reach to the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

Note: Given n will be a positive integer.

Example 1:

Input: 2
Output: 2
Explanation: There are two ways to climb to the top.
1. 1 step + 1 step
2. 2 steps
Example 2:

Input: 3
Output: 3
Explanation: There are three ways to climb to the top.
1. 1 step + 1 step + 1 step
2. 1 step + 2 steps
3. 2 steps + 1 step

*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

// 1. 递归求解，保存中间结果res,如果已有结果，则不需站考, p[i]=p[i-1]+p[i-2]
// Time:O(n),Space:O(n)
int climb(int* res,int n){
	if(n<=2) return n;
	if(res[n]>0) return res[n];
	int r=climb(res,n-1);
	if(res[n-1]<0) res[n-1]=r;
	int l=climb(res,n-2);
	if(res[n-2]<0) res[n-2]=l;
	return l+r;
}

int climbStairs_1(int n) {
	int res[n+1];
	//memset按字节初始化的，即0或-1时才是预期结果，0二进制是全0,-1二进制全是1
	memset(res,-1,sizeof(res));
	return climb(res,n);
}

//2. 到达当前位置，要么从n-1位置走一步到，要么从n-2位置走两步到，因此有每个位置p[i]=p[i-1]+p[i-2],
//由于不需记录每个位置的值回溯，只需一个变量保存最终结果即可，本质上是Fibonacci公式
//Time:O(n),Space:O(1)
int climbStairs_2(int n) {
	if(n<=2) return n;
	int i,n_0,n_1=2,n_2=1; // n,n-1,n-2
	for(i=3;i<=n;i++){
		n_0=n_1+n_2;
		n_2=n_1;
		n_1=n_0;
	}
	return n_0;
}

/* 3.Fibonacci矩阵计算 
 * 			Fn+1 Fn				Fn   Fn-1
 * 令 Q(n) = Fn	 Fn-1 则Q(n-1) = Fn-1 Fn-2
 * 则矩阵递推关系为
 * 
 * 				1 1
 * Q(n)=Q(n-1)* 1 0
 * 
 * 	Fn+1 Fn		  Fn   Fn-1 	1 1
 * 	Fn	 Fn-1	= Fn-1 Fn-2  *  1 0
 * 
 * 可用数学归纳法证明上述等式
 * 
 * 		    	1 1
 * 初始值 Q(1) = 1 0 
 * 
 * 即Q(n) = Q(1)^n 
 * 
 * 快速幂：
 * 令结果为 result
 * 如果为偶数则有 Q(n) = Q(n/2) * Q(n/2) = Q(1)^(n/2)*Q(1)^(n/2)
 * 如果为奇数则有res*=Q(n) Q(n) = Q(n/2) * Q(n/2) = Q(1)^(n/2)*Q(1)^(n/2)
 * Time:O(log2n),Space:O(1)
 */ 
void matrix_mul(unsigned int (*a)[2],unsigned int (*b)[2],unsigned int (*c)[2]){
	int size=2;
	memset(c,0,sizeof(unsigned int)*size*size);
	int i,j,k;
	for(i=0;i<size;i++){
		for(j=0;j<size;j++){
			c[i][j]=a[i][0]*b[0][j] + a[i][1]*b[1][j] ;
		}
	}
}

int climbStairs(int n) {
	if(n==0) return 1;
	if(n<=2) return n;
	//矩阵运算结果，开始是单位阵，相当于数数字1
	int size=2;
	unsigned int  res[2][2]={{1,0},{0,1}};
	unsigned int base[2][2]={{1,1},{1,0}};
	unsigned int tmp[2][2];
	
	while(n){
		//奇数情况,由于除以2会舍去一个base，先将base乘到res中
		if((n&1)==1){
			matrix_mul(res,base,tmp);
			memcpy(res,tmp,sizeof(unsigned int )*size*size);
		}
		//n除以2，把底数变大，指数变小
		n>>=1;
		matrix_mul(base,base,tmp);
		memcpy(base,tmp,sizeof(unsigned int)*size*size);
	}
	return res[0][0];
}


int main(int argc,char** argv){
	int i,j,n=3524578 ;
	int res=climbStairs(n);
	//int a[2][2]= {{2,3},{3,4}};
	//int b[2][2]= {{4,5},{1,2}};
	//matrix_mul(a,b,n);
	//for(i=0;i<n;i++){
		//for(j=0;j<n;j++){
			//printf("%d ",a[i][j]);
		//}
		//printf("\n");
	//}
	printf("res=%d\n",res);
}
