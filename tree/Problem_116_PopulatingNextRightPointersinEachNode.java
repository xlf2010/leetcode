package tree;


/**
 * 116. Populating Next Right Pointers in Each Node https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
 * Medium
 * You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following
 * definition:
 * struct Node {
	 * int val;
	 * Node *left;
	 * Node *right;
	 * Node *next;
 * }
 * Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.
 * Initially, all next pointers are set to NULL.
 * Example:
 * Input:
 * {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":null,"next":null,"right":null,"val":4},"next":null,"right":{"$id":"4","left":null,"next"
 * :null,
 * "right":null,"val":5},"val":2},"next":null,"right":{"$id":"5","left":{"$id":"6","left":null,"next":null,"right":null,"val":6},"next":null,"right"
 * :{"$id":"7","left":null,"next":null,"right":null,"val":7},"val":3},"val":1}
 * Output:
 * {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":null,"next":{"$id":"4","left":null,"next":{"$id":"5","left":null,"next":{"$id":"6","left"
 * :null
 * ,"next":null,"right":null,"val":7},"right":null,"val":6},"right":null,"val":5},"right":null,"val":4},"next":{"$id":"7","left":{"$ref":"5"},"next"
 * :null,"right":{"$ref":"6"},"val":3},"right":{"$ref":"4"},"val":2},"next":null,"right":{"$ref":"7"},"val":1}
 * Explanation: Given the above perfect binary tree (Figure A), your function should populate each next pointer to point to its next right node, just
 * like in Figure B.
 * Note:
 * You may only use constant extra space.
 * Recursive approach is fine, implicit stack space does not count as extra space for this problem. 
 */
public class Problem_116_PopulatingNextRightPointersinEachNode {
	static class Node {
		public int val;
		public Node left;
		public Node right;
		public Node next;

		public Node() {
		}

		public Node(int _val, Node _left, Node _right, Node _next) {
			val = _val;
			left = _left;
			right = _right;
			next = _next;
		}

		@Override
		public String toString() {
			return Integer.toString(val);
		}
	}

	//递归处理，如果是左节点，则next指向右节点，如果是右节点，则指向parent的兄弟节点的左孩子
	public void conn(Node node, Node parent, boolean isLeft) {
		if (node == null) {
			return;
		}
		if (isLeft) {
			node.next = parent.right;
		} else if (parent.next != null) {
			node.next = parent.next.left;
		} else {
			node.next = null;
		}
		conn(node.left, node, true);
		conn(node.right, node, false);
	}

	//Runtime: 0 ms, faster than 100.00% of Java online submissions for Populating Next Right Pointers in Each Node.
	//Memory Usage: 34.2 MB, less than 98.48% of Java online submissions for Populating Next Right Pointers in Each Node.
	// 一颗完美二叉树(所有叶子节点在同一层次,所有节点(除了叶节点)都有2个孩子，节点个数为2^n - 1),将所有节点Node.next指向兄弟节点
	public Node connect(Node root) {
		if (root == null) {
			return root;
		}
		conn(root.left, root, true);
		conn(root.right, root, false);
		return root;
	}

	public static void main(String[] args) {
		Node[] nodes = new Node[15];
		Node _left = null, _right = null;
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = new Node(i, _left, _right, null);
		}
		for (int i = 0; i < nodes.length; i++) {
			if (i <= nodes.length / 2 - 1) {
				_left = nodes[i * 2 + 1];
				_right = nodes[i * 2 + 2];
			} else {
				_left = _right = null;
			}
			nodes[i].left = _left;
			nodes[i].right = _right;
		}

		Node node = new Problem_116_PopulatingNextRightPointersinEachNode().connect(nodes[0]);

		Node next_left = node.left;
		while (node != null) {
			while (node != null) {
				System.out.print(node.val + ",");
				node = node.next;
			}
			System.out.println();
			node = next_left;
			if (node != null)
				next_left = node.left;
		}

	}

}
