/**
100. Same Tree	https://leetcode.com/problems/same-tree/
Easy

1119

34

Favorite

Share
Given two binary trees, write a function to check if they are the same or not.

Two binary trees are considered the same if they are structurally identical and the nodes have the same value.

Example 1:

Input:     1         1
          / \       / \
         2   3     2   3

        [1,2,3],   [1,2,3]

Output: true
Example 2:

Input:     1         1
          /           \
         2             2

        [1,2],     [1,null,2]

Output: false
Example 3:

Input:     1         1
          / \       / \
         2   1     1   2

        [1,2,1],   [1,1,2]

Output: false
Accepted
376,382
Submissions
752,835

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//递归判断
int sameTree(struct TreeNode* p, struct TreeNode* q){
	if(!p || !q) return 1;
	//不相等，返回0
	if(p->val != q->val) {
		return 0;
	}
	//一边有子树，另一边没有，返回0
	if((p->left && !q->left) || (!p->left && q->left)){
		return 0;
	}
	
	if((p->right && !q->right) || (!p->right && q->right)){
		return 0;
	}
	
	return sameTree(p->left,q->left) && sameTree(p->right,q->right);
}
//Runtime: 0 ms, faster than 100.00% of C online submissions for Same Tree.
//Memory Usage: 7.1 MB, less than 96.01% of C online submissions for Same Tree.
//判断两个树是否一样
bool isSameTree(struct TreeNode* p, struct TreeNode* q){
	if(!p && !q) return 1;
	if(!p || !q) return 0;
	
	return sameTree(p,q);
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=10;
	t[i].left = t+1;
	t[i].right = t+2;
	i++;
	t[i].val=5;
	t[i].right=NULL;
	t[i].left = NULL;
	i++;
	t[i].val=15;
	t[i].left=t+3;
	t[i].right =t+4;
	i++;
	t[i].val=6;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=20;
	t[i].left=NULL;
	t[i].right =NULL;
	
	struct TreeNode* t2 = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	i=0;
	t2[i].val=10;
	t2[i].left = t2+1;
	t2[i].right = t2+2;
	i++;
	t2[i].val=5;
	t2[i].right=NULL;
	t2[i].left = NULL;
	i++;
	t2[i].val=15;
	t2[i].left=t2+3;
	t2[i].right =t2+4;
	i++;
	t2[i].val=6;
	t2[i].left=NULL;
	t2[i].right =NULL;
	i++;
	t2[i].val=20;
	t2[i].left=NULL;
	t2[i].right =NULL;
//	int returnSize;
	int res = isSameTree(t,t2);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}

