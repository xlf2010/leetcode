/**
114. Flatten Binary Tree to Linked List 	https://leetcode.com/problems/flatten-binary-tree-to-linked-list/
Medium

Given a binary tree, flatten it to a linked list in-place.

For example, given the following tree:

    1
   / \
  2   5
 / \   \
3   4   6

The flattened tree should look like:

1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6



*/

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};


//定义链表栈
struct Stack {
    struct TreeNode *t;
    struct Stack *pre;
};

//入栈
void push(struct Stack** stack,struct TreeNode* t){
	if(!t) return;
	struct Stack* l = (struct Stack *) malloc(sizeof(struct Stack));
	l->t=t;
	l->pre=*stack;
	*stack=l;
}
//出栈
struct TreeNode* pop(struct Stack** stack){
	if(!(*stack)) return NULL;
	struct Stack* l = *stack;
	*stack=(*stack)->pre;
	struct TreeNode* t=l->t;
	free(l);
	return t;
}

//Runtime: 4 ms, faster than 95.91% of C online submissions for Flatten Binary Tree to Linked List.
//Memory Usage: 8.2 MB, less than 47.01% of C online submissions for Flatten Binary Tree to Linked List.
//将树转换成链表
void flatten(struct TreeNode* root){
	if(!root) return;
	if(!root->left && !root->right) return;
	
	struct Stack *stack = NULL;
	struct TreeNode *t=root;
	while(t || stack){
		//右节点入栈
		push(&stack,t->right);
		//存在左节点拼接左节点，左节点的右节点入栈
		while(t->left){
			t->right=t->left;
			t->left=NULL;
			t=t->right;
			push(&stack,t->right);
		}
		//弹出右节点，拼接右节点
		t->right=pop(&stack);
		t=t->right;
	}
}

void printTree(struct TreeNode* t){
	if(!t) return;
	printf("%d,",t->val);
	printTree(t->left);
	printTree(t->right);
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = NULL;
	i++;
	t[i].val=3;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=6;
	t[i].left=NULL;
	t[i].right =t+3;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;


	flatten(t);
	printTree(t);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	//printf("%d\n",res);
	return 0;
}
