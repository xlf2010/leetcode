/**
104. Maximum Depth of Binary Tree
Easy

1293

48

Favorite

Share
Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its depth = 3.

Accepted
506,140
Submissions
836,980
 */

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void findMax(struct TreeNode *t,int deep,int *max){
	if(!t) return;
	findMax(t->left,deep+1,max);
	if(*max < deep){
		*max=deep;
	}
	findMax(t->right,deep+1,max);
}
//Runtime: 4 ms, faster than 99.63% of C online submissions for Maximum Depth of Binary Tree.
//Memory Usage: 9.4 MB, less than 92.34% of C online submissions for Maximum Depth of Binary Tree.
//求树的最大深度
int maxDepth(struct TreeNode* root){
	if(!root)return 0;
	int max=-1,deep=1;
	findMax(root,deep,&max);
	return max;
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+2;
	i++;
	t[i].val=2;
	t[i].left=t+3;
	t[i].right=NULL;
	i++;
	t[i].val=3;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;

	int res = maxDepth(t);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}

