/**
103. Binary Tree Zigzag Level Order Traversal
Medium

934

59

Favorite

Share
Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
Accepted
218,082
Submissions
523,185

*/


#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};


struct Queue{
	struct TreeNode *t;
	struct Queue *next;
	int level;
};

void offer(struct Queue **head,struct Queue **tail,struct TreeNode *t,int level){
	if(!t) return;
	struct Queue *q = (struct Queue *)malloc(sizeof(struct Queue));
	q->t=t;
	q->level=level;
	q->next=NULL;
	if(!(*head)) {
		*head=q;
	}else{
		(*tail)->next=q;
	}
	*tail=q;
}

struct TreeNode* poll(struct Queue **head,int *level){
	if(!(*head)) return NULL;
	struct Queue *q = *head;
	*head=(*head)->next;
	*level = q->level;
	struct TreeNode *t = q->t;
	free(q);
	return t;
}
//Runtime: 0 ms, faster than 100.00% of C online submissions for Binary Tree Zigzag Level Order Traversal.
//Memory Usage: 8.3 MB, less than 100.00% of C online submissions for Binary Tree Zigzag Level Order Traversal.
//蛇形层次遍历二叉树
int** zigzagLevelOrder(struct TreeNode* root, int* returnSize, int** returnColumnSizes){
	*returnSize = 0;
	*returnColumnSizes = (int *)malloc(sizeof(int));
	(*returnColumnSizes)[*returnSize]=0;
	if(!root) return NULL;
	
	struct Queue *head=NULL,*tail=NULL;
	
	int level=1,curr_level=0,column_size=0;
	int *re,**ret = (int **)malloc(sizeof(int *));
	struct TreeNode *t=root;
	while(t){
		//同一层次，加入ret数组,不同层次另起数组
		if(level == curr_level){			
			(*returnColumnSizes)[(*returnSize) - 1] ++;
			column_size = (*returnColumnSizes)[(*returnSize) - 1];
			
			re = (int *)realloc(re,sizeof(int) * column_size);
			re[column_size - 1] = t->val;
			ret[(*returnSize) - 1] = re;
		}else{
			//返回行大小
			(*returnSize)++;
			
			//设置返回值
			ret = (int **)realloc(ret,sizeof(int *) * (*returnSize));
			re = (int *)malloc(sizeof(int));
			*re=t->val;
			ret[(*returnSize) - 1] = re;
			
			//设置返回列大小
			*returnColumnSizes = (int *)realloc(*returnColumnSizes,sizeof(int) * (*returnSize));
			(*returnColumnSizes)[(*returnSize) - 1] = 1;
			
			curr_level=level;
		}
		
		//子树入队列
		offer(&head,&tail,t->left,level+1);
		offer(&head,&tail,t->right,level+1);
		
		t=poll(&head,&level);
	}
	int i,j;
	//元素位置为奇数的翻转
	for(i=0;i<*returnSize;i++){
		if(!(i & 1)) continue;
		column_size = (*returnColumnSizes)[i] - 1;
		j=0;
		re = ret[i];
		while(j < column_size){
			level = re[j];
			re[j] = re[column_size];
			re[column_size] = level;
			j++;
			column_size--;
		}
	}
	return ret;
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+3;
	i++;
	t[i].val=2;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=3;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;

	int *returnColumnSizes,returnSize,j;
	int **res = zigzagLevelOrder(t,&returnSize,&returnColumnSizes);
	for(i=0;i<returnSize;i++){
		for(j=0;j<returnColumnSizes[i];j++){
			printf("%d,",res[i][j]);
		}
		printf("\n");
	}
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("\n");
	return 0;
}
