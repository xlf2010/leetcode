/**
129. Sum Root to Leaf Numbers		https://leetcode.com/problems/sum-root-to-leaf-numbers/
Medium

Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.

An example is the root-to-leaf path 1->2->3 which represents the number 123.

Find the total sum of all root-to-leaf numbers.

Note: A leaf is a node with no children.

Example:

Input: [1,2,3]
    1
   / \
  2   3
Output: 25
Explanation:
The root-to-leaf path 1->2 represents the number 12.
The root-to-leaf path 1->3 represents the number 13.
Therefore, sum = 12 + 13 = 25.

Example 2:

Input: [4,9,0,5,1]
    4
   / \
  9   0
 / \
5   1
Output: 1026
Explanation:
The root-to-leaf path 4->9->5 represents the number 495.
The root-to-leaf path 4->9->1 represents the number 491.
The root-to-leaf path 4->0 represents the number 40.
Therefore, sum = 495 + 491 + 40 = 1026.

Accepted
184,870
Submissions
435,234
*/
#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//递归，如果是叶子节点的话，往sum加，否则转换成十进制存放在lastVal
void calSum(struct TreeNode *t,int lastVal,int *sum){
	if(!t) return;
	
	lastVal = lastVal * 10 + t->val;
	if(!t->left && !t->right){		
		*sum+=lastVal;
	}
	
	calSum(t->left,lastVal,sum);
	calSum(t->right,lastVal,sum);
}

//Runtime: 0 ms, faster than 100.00% of C online submissions for Sum Root to Leaf Numbers.
//Memory Usage: 7.4 MB, less than 99.10% of C online submissions for Sum Root to Leaf Numbers.
//求从根节点往叶子节点所有路径组成的数字之和
int sumNumbers(struct TreeNode* root){
	if(!root) return 0;
	int sum=0;
	calSum(root,0,&sum);
	return sum;
}

int main(int argc,char** argv){
	int i=0,size=6;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+3;
	i++;
	t[i].val=3;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=6;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=t+5;
	t[i].right =NULL ;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;


	int res=sumNumbers(t);
//	printTree(t);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}
