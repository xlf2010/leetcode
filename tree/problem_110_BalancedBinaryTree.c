/**

110. Balanced Binary Tree		https://leetcode.com/problems/balanced-binary-tree/
Easy

1307

118

Favorite

Share
Given a binary tree, determine if it is height-balanced.

For this problem, a height-balanced binary tree is defined as:

a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

Example 1:

Given the following tree [3,9,20,null,null,15,7]:

    3
   / \
  9  20
    /  \
   15   7
Return true.

Example 2:

Given the following tree [1,2,2,3,3,null,null,4,4]:

       1
      / \
     2   2
    / \
   3   3
  / \
 4   4
Return false.

Accepted
333,455
Submissions
807,709

*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

bool isBalanced(struct TreeNode* root){
	
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+3;
	i++;
	t[i].val=2;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=3;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;

	int res = hasPathSum(t,5);

	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}
