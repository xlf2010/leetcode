/**
94. Binary Tree Inorder Traversal
Medium

1563

68

Favorite

Share
Given a binary tree, return the inorder traversal of its nodes' values.

Example:

Input: [1,null,2,3]
   1
    \
     2
    /
   3

Output: [1,3,2]
Follow up: Recursive solution is trivial, could you do it iteratively?

*/
#include <stdio.h>
#include <stdlib.h>


struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

void traversal(struct TreeNode* t,int** res,int *returnSize){
	if(!t) return;
	traversal(t->left,res,returnSize);
	(*returnSize)++;
	int* tmp = (int *)realloc(*res,sizeof(int)*(*returnSize));
	tmp[(*returnSize)-1] = t->val;
	*res=tmp;
	traversal(t->right,res,returnSize);
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
 //Runtime: 0 ms, faster than 100.00% of C online submissions for Binary Tree Inorder Traversal.
//Memory Usage: 7.7 MB, less than 61.97% of C online submissions for Binary Tree Inorder Traversal.
//递归中序遍历树
int* inorderTraversal_1(struct TreeNode* root, int* returnSize){
	*returnSize=0;
	if(!root) return NULL;
	int* res=(int *)malloc(sizeof(int));
	traversal(root,&res,returnSize);
	return res;
}

//定义链表栈
struct Stack {
    struct TreeNode *t;
    struct Stack *pre;
};

//入栈
void push(struct Stack** stack,struct TreeNode* t){
	struct Stack* l = (struct Stack *) malloc(sizeof(struct Stack));
	l->t=t;
	l->pre=*stack;
	*stack=l;
}
//出栈
struct Stack* pop(struct Stack** stack){
	if(!(*stack)) return NULL;
	struct Stack* l = *stack;
	*stack=(*stack)->pre;
	return l;
}
//Runtime: 4 ms, faster than 94.38% of C online submissions for Binary Tree Inorder Traversal.
//Memory Usage: 7.4 MB, less than 95.42% of C online submissions for Binary Tree Inorder Traversal.
//循环遍历
int* inorderTraversal(struct TreeNode* root, int* returnSize){
	*returnSize=0;
	if(!root) return NULL;
	struct Stack *stack =NULL;
	struct TreeNode* t=root;
	int* res = (int *)malloc(sizeof(int));
	struct Stack* ln;
	while(t || stack){
		while(t){
			push(&stack,t);
			t=t->left;
		}
		
		ln=pop(&stack);
		t=ln->t;
		(*returnSize)++;
		res=(int *)realloc(res,sizeof(int)*(*returnSize));
		res[(*returnSize)-1]=t->val;
		
		t=t->right;
		free(ln);
	}
	return res;
}

int main(int argc,char** argv){
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * 3);
	int i=0;
	t[i].val=1;
	t[i].left=NULL;
	t[i].right = t+(i+1);
	i++;
	t[i].val=2;
	t[i].left=t+(i+1);
	t[i].right = NULL;
	i++;
	t[i].val=3;
	t[i].left=NULL;
	t[i++].right =NULL;
	
	int returnSize;
	int *res = inorderTraversal(t,&returnSize);
	if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	}
	printf("\n");
	return 0;
}

