package tree;


import java.util.LinkedList;

/**
 * 117. Populating Next Right Pointers in Each Node II
 * Medium
 * Given a binary tree
 * struct Node {
 * int val;
 * Node *left;
 * Node *right;
 * Node *next;
 * }
 * Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.
 * Initially, all next pointers are set to NULL.
 * Example:
 * Input:
 * {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":null,"next":null,"right":null,"val":4},"next":null,"right":{"$id":"4","left":null,"next"
 * :null,
 * "right":null,"val":5},"val":2},"next":null,"right":{"$id":"5","left":null,"next":null,"right":{"$id":"6","left":null,"next":null,"right":null,
 * "val":7},"val":3},"val":1}
 * Output:
 * {"$id":"1","left":{"$id":"2","left":{"$id":"3","left":null,"next":{"$id":"4","left":null,"next":{"$id":"5","left":null,"next":null,"right":null
 * ,"val"
 * :7},"right":null,"val":5},"right":null,"val":4},"next":{"$id":"6","left":null,"next":null,"right":{"$ref":"5"},"val":3},"right":{"$ref":"4"},"val"
 * :2},"next":null,"right":{"$ref":"6"},"val":1}
 * Explanation: Given the above binary tree (Figure A), your function should populate each next pointer to point to its next right node, just like in
 * Figure B.
 * Note:
 * You may only use constant extra space.
 * Recursive approach is fine, implicit stack space does not count as extra space for this problem.
 */
public class Problem_117_PopulatingNextRightPointersinEachNode2 {
	static class Node {
		public int val;
		public Node left;
		public Node right;
		public Node next;

		public Node() {
		}

		public Node(int _val, Node _left, Node _right, Node _next) {
			val = _val;
			left = _left;
			right = _right;
			next = _next;
		}

		@Override
		public String toString() {
			return Integer.toString(val);
		}
	}

	public void offerChild(LinkedList<Node> linkedList, Node node) {
		if (node.left != null) {
			linkedList.offer(node.left);
		}
		if (node.right != null) {
			linkedList.offer(node.right);
		}
	}

	// 一颗完美普通,将所有节点Node.next指向同一层级的右节点
	public Node connect(Node root) {
		if (root == null || (root.left == null && root.right == null)) {
			return root;
		}
		LinkedList<Node> linkedList = new LinkedList<>();
		Node first = root, pre = null, curr = null;
		linkedList.offer(root);

		while (!linkedList.isEmpty()) {
			curr = linkedList.poll();

			offerChild(linkedList, curr);

			if (curr == first) {
				first = null;
			} else if (pre != null) {
				pre.next = curr;
			}
			pre = curr;
			if (first == null) {
				first = curr.left == null ? curr.right : curr.left;
			}
		}
		return root;
	}

	public static void main(String[] args) {
		Node[] nodes = new Node[15];
		Node _left = null, _right = null;
		for (int i = 0; i < nodes.length; i++) {
			nodes[i] = new Node(i, _left, _right, null);
		}
		for (int i = 0; i < nodes.length; i++) {
			if (i <= nodes.length / 2 - 1) {
				_left = nodes[i * 2 + 1];
				_right = nodes[i * 2 + 2];
			} else {
				_left = _right = null;
			}
			nodes[i].left = _left;
			nodes[i].right = _right;
		}
		nodes[5].left = null;
		nodes[4].left = nodes[4].right = null;
		Node node = new Problem_117_PopulatingNextRightPointersinEachNode2().connect(nodes[0]);

		Node next_left = node.left;
		while (node != null) {
			while (node != null) {
				System.out.print(node.val + ",");
				node = node.next;
			}
			System.out.println();
			node = next_left;
			if (node != null)
				next_left = node.left;
		}

	}

}
