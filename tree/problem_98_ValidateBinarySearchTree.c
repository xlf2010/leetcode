/**
98. Validate Binary Search Tree  https://leetcode.com/problems/validate-binary-search-tree/
Medium

1880

282

Favorite

Share
Given a binary tree, determine if it is a valid binary search tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.
 

Example 1:

    2
   / \
  1   3

Input: [2,1,3]
Output: true
Example 2:

    5
   / \
  1   4
     / \
    3   6

Input: [5,1,4,null,null,3,6]
Output: false
Explanation: The root node's value is 5 but its right child's value is 4.
Accepted
399,950
Submissions
1,557,140
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//定义链表栈
struct Stack {
    struct TreeNode *t;
    struct Stack *pre;
    //检查标志位，从右数起，1为左节点，2为右节点
    int check;
};

//入栈
void push(struct Stack** stack,struct TreeNode* t){
	struct Stack* l = (struct Stack *) malloc(sizeof(struct Stack));
	l->t=t;
	l->pre=*stack;
	l->check=0;
	*stack=l;
}
//出栈
struct Stack* pop(struct Stack** stack){
	if(!(*stack)) return NULL;
	struct Stack* l = *stack;
	*stack=(*stack)->pre;
	return l;
}


bool isValid(struct Stack* stack,struct TreeNode* t){
	if(!t) return true;
	struct TreeNode* curr = t;
	while(stack){
		//左子树必须小于父节点
		if(curr == stack->t->left && t->val >= stack->t->val){
			return false;
		}
		//右子树必须大于父节点
		if(curr == stack->t->right && t->val <= stack->t->val){
			return false;
		}
		curr=stack->t;
		stack=stack->pre;
	}
	return true;
}

void freeStack(struct Stack* stack){
	struct Stack* s;
	while(stack){
		s=stack;
		stack=stack->pre;
		free(s);
	}
}

//Runtime: 356 ms, faster than 10.63% of C online submissions for Validate Binary Search Tree.
//Memory Usage: 84 MB, less than 5.13% of C online submissions for Validate Binary Search Tree.
//循环效率不咋滴
bool isValidBST_1(struct TreeNode* root){
	if(!root) return true;
	struct Stack *stack = NULL;
	struct TreeNode* t=root->left;
	struct Stack* ln;
	push(&stack,root);
	while(stack){
		//校验左节点,满足小于根节点则入栈，标记已遍历
		while(t && (stack->check & 1) == 0){
			if(!isValid(stack,t)){
				freeStack(stack);
				return false;
			}
			stack->check |= 1;
			push(&stack,t);
			t=t->left;
		}
		
		t=stack->t;
		//有右节点时，将当前节点为子树根节点向右遍历,标记已遍历
		if(t && t->right && (stack->check & 2) == 0){
			//校验右节点
			t=t->right;
			if(!isValid(stack,t)){
				freeStack(stack);
				return false;
			}
			stack->check |= 2;
			push(&stack,t);
		}
		//没有右节点，则表示当前节点已检查完，可以释放栈元素
		else{
			ln=pop(&stack);
			free(ln);
		}
	}
	return true;
}

//合法的BST是有序的，如果遍历出现前一节点的值大于当前值，直接返回false
void isVal(struct TreeNode* t,int* res,int *is_first,int* before){
	if(!(*res) || !t) return;
	isVal(t->left,res,is_first,before);
	if(*is_first){
		*is_first=0;
	}else if(*before >= t->val){
		*res=0;
		return;
	}
	*before=t->val;
	isVal(t->right,res,is_first,before);
}

//Runtime: 4 ms, faster than 100.00% of C online submissions for Validate Binary Search Tree.
//Memory Usage: 10.1 MB, less than 90.95% of C online submissions for Validate Binary Search Tree.
//还是用递归中序写一遍吧
bool isValidBST(struct TreeNode* root){
	int res=1,is_first=1,before;
	isVal(root,&res,&is_first,&before);
	return res;
}

int main(int argc,char** argv){
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * 5);
	int i=0;
	t[i].val=10;
	t[i].left = t+1;
	t[i].right = t+2;
	i++;
	t[i].val=5;
	t[i].right=NULL;
	t[i].left = NULL;
	i++;
	t[i].val=15;
	t[i].left=t+3;
	t[i].right =t+4;
	i++;
	t[i].val=6;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=20;
	t[i].left=NULL;
	t[i].right =NULL;
	
	int returnSize;
	int res = isValidBST(t);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}
