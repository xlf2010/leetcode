/**

112. Path Sum		https://leetcode.com/problems/path-sum/
Easy

922

296

Favorite

Share
Given a binary tree and a sum, 
determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

Note: A leaf is a node with no children.

Example:

Given the below binary tree and sum = 22,

      5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1
return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.

Accepted
312,569
Submissions
828,004

*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//递归计算分支和
void calc(struct TreeNode *t,int *flag,int cal_val,int sum){
	if(*flag || !t) return;
	cal_val+=t->val;
	
	//已经相等且没有左右子树，返回1
	if(cal_val == sum && !t->left && !t->right){
		*flag=1;
		return;
	}
	//遍历左子树
	calc(t->left,flag,cal_val,sum);
	calc(t->right,flag,cal_val,sum);
}
//Runtime: 0 ms, faster than 100.00% of C online submissions for Path Sum.
//Memory Usage: 9.4 MB, less than 97.55% of C online submissions for Path Sum.
//计算从根节点到叶子节点是否有路径之和等于sum
bool hasPathSum(struct TreeNode* root, int sum){
	if(!root) return 0;
	int flag=0;
	calc(root,&flag,0,sum);
	return flag;
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+3;
	i++;
	t[i].val=2;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=3;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;

	int res = hasPathSum(t,5);

	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}
