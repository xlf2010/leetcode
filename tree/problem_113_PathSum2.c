/**
113. Path Sum II
Medium

871

32

Favorite

Share
Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.

Note: A leaf is a node with no children.

Example:

Given the below binary tree and sum = 22,

      5
     / \
    4   8
   /   / \
  11  13  4
 /  \    / \
7    2  5   1
Return:

[
   [5,4,11,2],
   [5,8,4,5]
]
Accepted
230,644
Submissions
568,209

*/


#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

int qsize=0;

struct Queue{
	struct TreeNode *t;
	struct Queue *pre;
	struct Queue *next;
};

void offer(struct Queue **head,struct Queue **tail,struct TreeNode *t){
	if(!t) return;
	struct Queue *q = (struct Queue *)malloc(sizeof(struct Queue));
	q->t=t;
	q->pre=*tail;
	q->next=NULL;
	if(!(*head)) {
		*head=q;
	}else{
		(*tail)->next=q;
	}
	qsize++;
	*tail=q;
}

struct TreeNode* poll_tail(struct Queue **tail){
	if(!(*tail)) return NULL;
	struct Queue *q = *tail;
	*tail=(*tail)->pre;
	struct TreeNode *t = q->t;
	free(q);
	qsize--;
	return t;
}

struct Queue *head=NULL,*tail=NULL;

//构建结果
void generalPath(int*** ret,int** returnColumnSizes,int returnSize){
	*ret = (int **)realloc(*ret,sizeof(int *) * returnSize);
	*returnColumnSizes = (int *)realloc(*returnColumnSizes,sizeof(int) * returnSize);
	
	int i=0,*re=(int *)malloc(sizeof(int) * qsize);
	struct Queue *q=head;
	while(q){
		re[i++] = q->t->val;
		q=q->next;
	}
	(*ret)[returnSize-1] = re;
	(*returnColumnSizes)[returnSize-1] = i;
}


//递归计算分支和
void calc(struct TreeNode *t,int cal_val,int sum, int* returnSize, int** returnColumnSizes,int*** ret){
	if(!t) return;
	offer(&head,&tail,t);
	cal_val+=t->val;
	//已经相等且没有左右子树，构建结果
	if(cal_val == sum && !t->left && !t->right){
		(*returnSize)++;
		generalPath(ret,returnColumnSizes,*returnSize);
	}
	//遍历左子树
	calc(t->left,cal_val,sum,returnSize,returnColumnSizes,ret);
	calc(t->right,cal_val,sum,returnSize,returnColumnSizes,ret);
	poll_tail(&tail);
}

//Runtime: 8 ms, faster than 100.00% of C online submissions for Path Sum II.
//Memory Usage: 20.7 MB, less than 33.90% of C online submissions for Path Sum II.
int** pathSum(struct TreeNode* root, int sum, int* returnSize, int** returnColumnSizes){
	//空值判断
	*returnSize = 0;
	*returnColumnSizes = (int *)malloc(sizeof(int));
	(*returnColumnSizes)[*returnSize]=0;
	if(!root) return NULL;
	
	int **ret = (int **)malloc(sizeof(int *));
	head=tail=NULL;
	qsize=0;
	calc(root,0,sum,returnSize,returnColumnSizes,&ret);
	return ret;
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = NULL;
	t[i].right = NULL;
	i++;
	t[i].val=3;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=6;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=4;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=5;
	t[i].left=NULL;
	t[i].right = NULL;

	int *returnColumnSizes,returnSize,j;
	int **res = pathSum(t,1,&returnSize,&returnColumnSizes);
	for(i=0;i<returnSize;i++){
		for(j=0;j<returnColumnSizes[i];j++){
			printf("%d,",res[i][j]);
		}
		printf("\n");
	}

	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	//printf("%d\n",res);
	return 0;
}
