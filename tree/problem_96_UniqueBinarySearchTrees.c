/**
96. Unique Binary Search Trees	https://leetcode.com/problems/unique-binary-search-trees/
Medium

1667

65

Favorite

Share
Given n, how many structurally unique BST's (binary search trees) that store values 1 ... n?

Example:

Input: 3
Output: 5
Explanation:
Given n = 3, there are a total of 5 unique BST's:

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
Accepted
198,222
Submissions
429,716

*/

#include <stdio.h>
#include <string.h>

/**给定n，求[1..n]能组成多少个二叉搜索树
n=1:f(1)=1;
n=2:f(2)=2=1+1(以1为根结果为1，以2为根结果也是1)
n=3:f(3)=5=2+1+2(以1为根结果为2，以2为根结果是1,以3为根结为2)
n=4:f(4)=14=5+2+2+5(以1为根结果为5，以2为根结果是2,以3为根结为2,以4为根结果为5)
可以这么看问题：
以 i in [1..n],为根节点, [0..i-1]为左子树(0不存在)，[i+1..n]为右子树
则以i为根节点的二叉搜索树个数为左子树个数乘以右子树个数f(i-1)*f(n-1-i)

n
sum(f(i-1) * f(n-1-i))
i=1

令f(0)=1，即为树空情况下是一种情况
*/
//Runtime: 0 ms, faster than 100.00% of C online submissions for Unique Binary Search Trees.
//Memory Usage: 6.7 MB, less than 38.21% of C online submissions for Unique Binary Search Trees.
int numTrees_1(int n){
	if(n<2) return 1;
	int sum[n+1];
	sum[0]=sum[1]=1;
	int i,j;
	for(i=2;i<=n;i++){
		sum[i]=0;
		for(j=0;j<i;j++){
			sum[i]+=sum[j] * sum[i-1-j];
		}
	}
/* 	for(i=0;i<=n;i++){
		printf("%d,",sum[i]);
	} */
	return sum[n];
}
//Runtime: 4 ms, faster than 51.35% of C online submissions for Unique Binary Search Trees.
//Memory Usage: 6.8 MB, less than 12.20% of C online submissions for Unique Binary Search Trees.
//求n为根节点的子树数量
int countNumTree(int n,int* sum){
	if(sum[n]>0) return sum[n];
	int i;
	for(i=0;i<n;i++){
		sum[n] += countNumTree(i,sum) * countNumTree(n-i-1,sum);
	}
	return sum[n];
}

//递归写一遍
int numTrees(int n){
	if(n<2) return 1;
	int sum[n+1];
	memset(sum,0,sizeof(sum));
	sum[0]=sum[1]=1;
	
	return countNumTree(n,sum);
}

int main(int argc,char** argv){
	int res=numTrees(4);
	printf("%d\n",res);
	res=numTrees_1(4);
	printf("%d\n",res);
	return 0;
}
