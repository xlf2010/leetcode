/**
95. Unique Binary Search Trees II	https://leetcode.com/problems/unique-binary-search-trees-ii/
Medium

1194

111

Favorite

Share
Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1 ... n.

Example:

Input: 3
Output:
[
  [1,null,3,2],
  [3,2,null,1],
  [3,1,null,null,2],
  [2,1,3],
  [1,null,2,null,3]
]
Explanation:
The above output corresponds to the 5 unique BST's shown below:

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3
Accepted
137,756
Submissions
386,547
*/

#include <stdio.h>
#include <stdlib.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//递归生成子树
struct TreeNode* generalSubTree(struct TreeNode*** ret_node,int n,int* size){
	if(n<=0) return NULL;
	
}

/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
struct TreeNode** generateTrees(int n, int* returnSize){
	
	if(n<1) {
		*returnSize=0;
		return NULL;
	}
	struct TreeNode** ret_node = (struct TreeNode**)malloc(sizeof(struct TreeNode*));
	if(n==1){
		struct TreeNode* t = (struct TreeNode*)malloc(sizeof(struct TreeNode));
		t->val=n;
		t->left=NULL;
		t->right=NULL;
		ret_node[0] = t;
		*returnSize=1;
		return ret_node;
	}
	
	int sum[n+1];
	sum[0]=sum[1]=1;
	int i,j;
	for(i=2;i<=n;i++){
		sum[i]=0;
		for(j=0;j<i;j++){
			sum[i]+=sum[j] * sum[i-1-j];
		}
	}
	for(i=0;i<=n;i++){
		printf("%d,",sum[i]);
	}
	return sum[n];
}


int main(int argc,char** argv){
	int res=numTrees(4);
	return 0;
}
