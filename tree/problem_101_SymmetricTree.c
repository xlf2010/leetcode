/**
101. Symmetric Tree	https://leetcode.com/problems/symmetric-tree/
Easy

2186

46

Favorite

Share
Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3
 

But the following [1,2,2,null,3,null,3] is not:

    1
   / \
  2   2
   \   \
   3    3
 

Note:
Bonus points if you could solve it both recursively and iteratively.

Accepted
399,336
Submissions
919,821


*/


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct TreeNode {
    int val;
    struct TreeNode *left;
    struct TreeNode *right;
};

//定义链表栈
struct Stack {
    struct TreeNode *t;
    struct Stack *pre;
    int leftFlag;
};

//入栈
void push(struct Stack** stack,struct TreeNode* t, int leftFlag){
	struct Stack* l = (struct Stack *) malloc(sizeof(struct Stack));
	l->t=t;
	l->pre=*stack;
	l->leftFlag=leftFlag;
	*stack=l;
}
//出栈
struct Stack* pop(struct Stack** stack){
	if(!(*stack)) return NULL;
	struct Stack* l = *stack;
	*stack=(*stack)->pre;
	return l;
}

//先序递归判断是否对称树
//flag 标志位0表示是否对称，如果是0则不用比较后面的了,第1位表示栈行为 flag & 2==2表示出栈,flag & 2 ==0 表示入栈
void symmetric(struct TreeNode *t,struct TreeNode *root,struct Stack **stack,int *flag,int leftFlag){
	//为空或者已经不是对称树，直接返回
	if(!t || !(*flag & 1)) return;
	//左子树
	symmetric(t->left,root,stack,flag,1);
	
	//根节点为对称中心，表示开始访问以根节点的右子树，要出栈比较了
	if(t==root){
		*flag |= 2;
	}
	//出栈与当前节点比较，不相等则对称标志位置0
	else if(*flag & 2){
		struct Stack *s = pop(stack);
		if(!s || s->leftFlag == leftFlag || s->t->val != t->val)
			*flag &= ~1;
	}else if(!(*flag & 2)){
		push(stack,t,leftFlag);
	}
	//右子树
	symmetric(t->right,root,stack,flag,0);
}

//Runtime: 0 ms, faster than 100.00% of C online submissions for Symmetric Tree.
//Memory Usage: 11.4 MB, less than 94.17% of C online submissions for Symmetric Tree.
//先序遍历递归，用栈保存访问过的值
bool isSymmetric_1(struct TreeNode* root){
	if(!root) return 1;
	struct Stack* stack=NULL;
	int flag=1;
	symmetric(root,root,&stack,&flag,1);
	//栈非空，表示左子树还有剩余节点，非对称
	if(stack) return 0;
	return flag & 1;
}

//看了solution，发现递归更优雅的实现方式
//定义两个指针，分别为左右子树跟节点
bool sym(struct TreeNode *t1,struct TreeNode* t2){
	if(!t1 && !t2) return 1;
	if(!t1 || !t2) return 0;
	//根节点值相同，左子树的左节点与右子树右节点相同，左子树右节点与右子树左节点相同
	return t1->val==t2->val && sym(t1->left,t2->right) && sym(t1->right,t2->left);
}

bool isSymmetric_2(struct TreeNode* root){
	if(!root) return 1;	
	return sym(root,root);
}

//Runtime: 4 ms, faster than 97.90% of C online submissions for Symmetric Tree.
//Memory Usage: 8.4 MB, less than 94.17% of C online submissions for Symmetric Tree.
//循环实现,也是分别使用两个指针,根节点值相同，左子树的左节点与右子树右节点相同，左子树右节点与右子树左节点相同
bool isSymmetric(struct TreeNode* root){
	if(!root) return 1;	
	struct Stack *stack=NULL;
	push(&stack,root,0);
	push(&stack,root,0);
	struct TreeNode *t1,*t2;
	while(stack){
		t1=pop(&stack)->t;
		t2=pop(&stack)->t;
		if(!t1 && !t2) continue;
		if(!t1 || !t2 || t1->val != t2->val) return 0;
		push(&stack,t1->left,0);
		push(&stack,t2->right,0);
		push(&stack,t1->right,0);
		push(&stack,t2->left,0);
	}
	return 1;
}

int main(int argc,char** argv){
	int i=0,size=5;
	struct TreeNode* t = (struct TreeNode *) malloc(sizeof(struct TreeNode ) * size);
	
	t[i].val=1;
	t[i].left = t+1;
	t[i].right = t+3;
	i++;
	t[i].val=2;
	t[i].left=t+2;
	t[i].right=NULL;
	i++;
	t[i].val=2;
	t[i].left=NULL;
	t[i].right =NULL;
	i++;
	t[i].val=2;
	t[i].left=t+4;
	t[i].right =NULL;
	i++;
	t[i].val=2;
	t[i].left=NULL;
	t[i].right = NULL;

	
	int res = isSymmetric(t);
	
	/* if(res){
		for(i=0;i<returnSize;i++){
			printf("%d,",*(res+i));
		}
	} */
	printf("%d\n",res);
	return 0;
}

